package utils
{
	import feathers.controls.Button;

	public class Quest
	{
		public var _qName:String;
		public var _qDescription:String;
		
		public var _qComplete:Boolean;
		public var _qAvaliable:Boolean;
		
		public var _qXp:int;
		public var _qSupply:int;
		public var _nextQuest:int;
		
		public function Quest(name:String, params:Object=null)
		{
			super(name, params);
			/*data from db*/
		}	
		
		
	}
}