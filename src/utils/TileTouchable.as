package utils
{
	import functional.Assets;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.events.Event;
	
	public class TileTouchable extends Button
	{
		private var tileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tile"));
		
		
		public function TileTouchable()
		{
			super(Assets.getHudAtlas().getTexture("tile"));
			addEventListener(Event.ADDED, onAdded);
		}
		
		private function onAdded():void
		{
			this.upState = tileSkin;
		}
	}
}