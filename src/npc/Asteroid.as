package npc
{
	import functional.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	import starling.utils.deg2rad;
	
	public class Asteroid extends Sprite
	{
		public var isMineable:Boolean = false;
		
		public var icon:Image = new Image(Assets.getSSAtlas().getTexture("Asteroids"));
		
		public function Asteroid(ax:int, ay:int)
		{
			super();
			
			this.x = ax*100+50;
			this.y = ay*100+50;
			
			icon.scaleX = 0.9;
			icon.scaleY = 0.9;
			
			icon.pivotX = 50;
			icon.pivotY = 50;
			
			icon.rotation = deg2rad(Math.random()*360);
			
			addChild(icon);
			flatten();
		}
	}
}