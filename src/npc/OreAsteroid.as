package npc
{
	import functional.Assets;
	
	import starling.display.Image;
	
	public class OreAsteroid extends Asteroid
	{
		public function OreAsteroid(ax:int, ay:int)
		{
			super(ax, ay);
			isMineable = true;
			
			icon.texture = Assets.getSSAtlas().getTexture("Asteroids");
		}
	}
}