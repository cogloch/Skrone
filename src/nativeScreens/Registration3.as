package nativeScreens
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import ui.MenuAssets;
	import ui.Screen;
	
	public class Registration3 extends Screen
	{
		//private var _header:MenuAssets.bg;
		
		public function Registration3()
		{
			super();
			//_header = new MenuAssets.bg;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		private function onAddedToStage():void
		{
			//addChild(_header);
		}
		
		private function onRemovedFromStage():void
		{
			this.removeChildren();
		}
	}
}