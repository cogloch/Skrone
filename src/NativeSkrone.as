package
{
	import flash.display.Sprite;
	
	import functional.GameMenu;
	import functional.GameState;
	import functional.nativeFlash.NativeMenu;
	
	import playerio.*;
	
	[SWF(width="800", height="600", frameRate="30", backgroundColor="#000000")]
	
	public class NativeSkrone extends Sprite
	{
		public var client:Client;
		public var _gameState:Skrone;
		public var _gameMenu:NativeMenu;
		
		public function NativeSkrone()
		{
			trace("Skrone started");
			super();
			_gameMenu = new NativeMenu();
			_gameMenu.addEventListener("startGame", onStartGame);
			addChild(_gameMenu);
		}
		
		private function onStartGame():void
		{
			trace("start game");
			removeChild(_gameMenu);
			_gameState = new Skrone();
			_gameState.addEventListener("gameEnded", onGameEnded);
			addChild(_gameState);
		}
		
		private function onGameEnded():void
		{
			trace("game ended");
			removeChildren();
			addChild(_gameMenu);
		}
	}
}