package units.utility
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;

	public class Engineer extends Ship
	{
		private var icon:Image;
		
		public var buildRange:int = 2;
		public var repairRange:int = 2;
		public var recycleRange:int = 2;
		public var repairPower:int;
		
		public function Engineer()
		{
			type = "Engineer";
			sType = 16;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 75;
			maxHp = 75;
			
			shield = 30;
			maxShield = 30;
			shieldRegen = 15;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 2;
			moves = 4;
			los = 3;
			
			repairPower = 100;
		}
		
		private function _createShip(name:String, posX:uint, posY:uint):void
		{
			
		}
		
		private function _createStructue(name:String, posX:uint, posY:uint):void
		{
			
		}
		
		private function _pingStructure(name:String, posX:uint, posY:uint):void
		{
		
		}
	}
}