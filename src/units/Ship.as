package units
{
	import functional.Assets;
	import functional.Data;
	import functional.customEvents.CombatEvent;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Ship extends Sprite
	{
		public var hp:int;
		public var maxHp:int;
		public var dmg:int;
		public var shield:int;
		public var maxShield:int;
		public var shieldRegen:int;
		public var morale:int;
		public var maxMorale:int;
		public var speed:int;
		public var hasMorale:Boolean = true;
		public var hasShield:Boolean = true;
		
		public var los:int;
		
		public var tX:int;
		public var tY:int;
		
		//public var range:int;
		public var moveRange:int = 3;
		
		public var type:String;
		public var sType:int;
		public var moves:int = 5;
		public var cMoves:int;
		//public var oldMoves:int;
		
		public var faction:uint; // 1 = Korbis       2 = Tosse
		
		public var isBurning:Boolean = false;
		public var shieldsDown:Boolean = false;
		
		public var cRotation:int = 1;
		
		public var dead:Boolean = false;
		
		public var movesIcon:Image = new Image(Assets.getHudAtlas().getTexture("statIconsMoves"+moves));
		public var movesIconBg:Image = new Image(Assets.getHudAtlas().getTexture("statIconsTurns"));
		public var noMovesIcon:Image = new Image(Assets.getHudAtlas().getTexture("noMovesIcon"));
		
		public function Ship()
		{
			super();
		}
		
		private function die():void//sNo:String):void
		{
			//explosion
			//sound
			//creates junk
		}
		
		private function spawnAt(_x:int, _y:int):void
		{
				
		}
		
		public function getDmg(dmgAmount:int, eMorale:int, eSpeed:int):void
		{
			if(shield>0)
			{
				//get dmg to shield
				//low sound 
				//sparkles	
				if(shield - dmgAmount < 1)
				{
					hp = hp - (dmgAmount - shield);
					Data.hpDmg = dmgAmount - shield;
					Data.shieldDmg = shield;
					Data.died = false;
					//trace(sNo + " receives " + dmgAmount.toString() + " damage.");
					//trace(sNo + "'s shields are down. Taking " + (dmgAmount - shield).toString() + " damage to hull.");
					shield = 0;
				}
				else
				{
					shield -= dmgAmount;
					Data.hpDmg = 0;
					Data.shieldDmg = dmgAmount;
					//trace(sNo + " receives " + dmgAmount.toString() + " damage.");
				}
			}
			else
			{
				//get dmg to hp
				//high sound
				//poof/explosions/particles
				hp -= dmgAmount;
				Data.shieldDmg = 0;
				Data.hpDmg = dmgAmount;
				if(hp<1)
					Data.died = true;
				else
					Data.died = false;
			}
			
		}
		
		public function regenShield():void
		{
			//if out of combat
			if(shield+shieldRegen>=maxShield)
				shield = maxShield;
			else
				shield += shieldRegen;
		}
		
		private function moveTo(_x:int, _y:int):void
		{
			//engine sound
			//reactor particles
			//move to point b 
			//if enemy target in sight
			//attack while it stays in sight
			if(1==1)
			{
				//attack(target);
			}
		}
	}
}