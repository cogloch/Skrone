package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;

	public class Frigate extends Ship
	{
		private var icon:Image;
		
		public var attackRange:int = 4;
		
		public function Frigate()
		{
			type = "Frigate";
			sType = 15;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();	
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			
			shield = 60;
			maxShield = 60;
			shieldRegen = 20;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 3;
			moves = 4;
			los = 5;
			
			dmg = 40;
		}
	}
}