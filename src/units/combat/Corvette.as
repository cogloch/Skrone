package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;
	
	public class Corvette extends Ship
	{
		private var icon:Image;
		
		public var healRange:int = 2;
		public var healPower:int;
		
		public function Corvette()
		{
			super();
			type = "Corvette";
			sType = 19;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 100;
			maxHp = 100;
			
			shield = 20;
			maxShield = 20;
			shieldRegen = 10;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 2;
			moves = 4;
			los = 3;
		}
	}
}