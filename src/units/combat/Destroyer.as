package units.combat
{
	import units.Ship;
	
	import starling.display.Image;
	
	import functional.Assets;

	public class Destroyer extends Ship
	{
		private var icon:Image;
		
		public var attackRange:int = 4;
		
		public function Destroyer()
		{
			type = "Destroyer";
			sType = 17;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			
			shield = 50;
			maxShield = 50;
			shieldRegen = 25;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 3;
			moves = 4;
			los = 5;
			
			dmg = 65;
		}
	}
}