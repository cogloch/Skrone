package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;
	
	public class Striker extends Ship
	{
		private var icon:Image;
		
		public var attackRange:int = 2;
		//public var cloakRange:int = 2;
		
		public function Striker()
		{
			super();
			type = "Striker";
			sType = 24;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 130;
			maxHp = 130;
			
			shield = 80;
			maxShield = 80;
			shieldRegen = 35;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 8;
			moves = 5;
			los = 5;
			
			dmg = 40;
		}
	}
}