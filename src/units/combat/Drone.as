package units.combat
{
	import units.Ship;
	
	import starling.display.Image;
	import functional.Assets;
	
	public class Drone extends Ship
	{
		private var icon:Image;
		
		public var sabotageRange:int = 2;
		public var attackRange:int = 2;
		
		public function Drone()
		{
			super();
			type = "Drone";
			sType = 23;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hasMorale = false;
			hp = 50;
			maxHp = 50;
			
			shield = 20;
			maxShield = 10;
			shieldRegen = 10;
			
			speed = 4;
			moves = 7;
			los = 3;
			
			dmg = 25;
		}
	}
}