package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;

	public class Infiltrator extends Ship
	{
		public var cloakLength:int;
		public var cloakPower:int;
		
		private var icon:Image;
		
		public var attackRange:int = 2;
		public var zombifyRange:int = 2;
		
		public var isCloaked:Boolean;
		
		public function Infiltrator()
		{
			type = "Infiltrator";
			sType = 21;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			isCloaked = false;
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 90;
			maxHp = 90;
			
			shield = 90;
			maxShield = 90;
			shieldRegen = 25;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 2;
			moves = 4;
			los = 5;
			
			dmg = 40;
		}
		
		private function go_invisible(cloakLength:int, cloakPower:int):void
		{
			
		}
	}
}






