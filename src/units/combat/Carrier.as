package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;

	public class Carrier extends Ship
	{
		private var icon:Image;
		
		public var attackRange:int = 5;
		
		public function Carrier()
		{
			type = "Carrier";
			sType = 20;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 200;
			maxHp = 200;
			
			shield = 50;
			maxShield = 50;
			shieldRegen = 25;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 3;
			moves = 4;
			los = 5;
			
			dmg = 80;
		}
	}
}