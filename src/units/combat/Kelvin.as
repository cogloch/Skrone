package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;

	public class Kelvin extends Ship
	{
		private var icon:Image;
		
		public var attackRange:int = 2;
		//public var releaseRange:int = 2;
		
		public function Kelvin()
		{
			type = "Kelvin";
			sType = 18;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
	
			setStats();
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			
			shield = 50;
			maxShield = 50;
			shieldRegen = 20;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 3;
			moves = 4;
			los = 5;
			
			dmg = 40;
		}
	}
}