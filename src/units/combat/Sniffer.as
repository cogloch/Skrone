package units.combat
{
	import functional.Assets;
	
	import starling.display.Image;
	
	import units.Ship;
	
	public class Sniffer extends Ship
	{
		private var icon:Image;
		
		public var sniffRange:int = 2;
		
		public function Sniffer()
		{
			super();
			type = "Scout";
			sType = 22;
			icon = new Image(Assets.getSSAtlas().getTexture(type));
			addChild(icon);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 130;
			maxHp = 130;
			
			shield = 50;
			maxShield = 50;
			shieldRegen = 20;
			
			morale = 50;
			maxMorale = 100;
			
			speed = 3;
			moves = 7;
			los = 5;
			
			dmg = 20;
		}
	}
}