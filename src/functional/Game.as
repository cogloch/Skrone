package functional
{
	import feathers.controls.Button;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Game extends Sprite
	{
		private var screenMenu:GameMenu;
		private var screenInGame:GameState;
		private var soundButton:Button;
		
		public function Game()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initScreens();
		}
		
		private function initScreens():void
		{
			//this.addEventListener(NavigationEvent.CHANGE_SCREEN, onChangeScreen);
			
			//In-game screen
			screenInGame = new GameState();
			//screenInGame.addEventListener(NavigationEvent.CHANGE_SCREEN, onInGameNavigation);
			this.addChild(screenInGame);
			
			//Menu screen
			screenMenu = new GameMenu();
			this.addChild(screenMenu);
			
			//Sound Button
			soundButton = new Button();
			
		}
		
	}
}