package functional
{
	import feathers.controls.Button;
	
	import functional.Assets;
	import functional.customEvents.ActionSelectedEvent;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.utils.deg2rad;
	
	public class ActionsWheel extends Sprite
	{
		/**
		 * Ship Actions
		 */
		public var move:Button;
		public var attack:Button;
		public var build:Button;
		public var sniff:Button;
		public var cloak:Button;
		public var repair:Button;
		public var recycle:Button;
		public var zombify:Button;
		public var heal:Button;
		public var sabotage:Button;
		public var release:Button;
		
		/**
		 * Build List
		 */
		public var reactor:Button;
		public var oreMine:Button;
		public var factory:Button;
		public var monitor:Button;
		public var fBase:Button;
		public var cannon:Button;
		public var turret:Button; //damn... almost
		public var wall:Button;
		public var fortress:Button;
		public var dome:Button;
		
		/**
		 * Spawn List
		 */
		public var frigate:Button;
		public var engineer:Button;
		public var destroyer:Button;
		public var kelvin:Button;
		public var corvette:Button;
		public var carrier:Button;
		public var infiltrator:Button;
		public var scout:Button;
		public var drone:Button;
		public var striker:Button;
		
		/**
		 * Building Actions
		 */
		public var spawnEngi:Button;
		
		private var bg:Image;
		private var info:Image;
		public var infoSec:Image;
		public var insufficientF:Alert;
		public var pointers:Array = new Array;
		
		public var isOn:Boolean;
		public var isBuild:Boolean;
		public var isSpawn:Boolean;
		
		/**
		 * Ship Actions
		 */
		private var moveUp:Texture;
		private var moveHover:Texture;
		
		private var attackUp:Texture;
		private var attackHover:Texture;
		
		private var buildUp:Texture;
		private var buildHover:Texture;
		
		private var sniffUp:Texture;
		private var sniffHover:Texture;
		
		private var cloakUp:Texture;
		private var cloakHover:Texture;
		
		private var unCloakUp:Texture;
		private var unCloakHover:Texture;
		
		private var repairUp:Texture;
		private var repairHover:Texture;
		
		private var recycleUp:Texture;
		private var recycleHover:Texture;
		
		private var zombifyUp:Texture;
		private var zombifyHover:Texture;
		
		private var healUp:Texture;
		private var healHover:Texture;
		
		private var sabotageUp:Texture;
		private var sabotageHover:Texture;
		
		private var releaseUp:Texture;
		private var releaseHover:Texture;
		
		/**
		 * Build List 
		 */
		private var reactorUp:Texture;
		private var reactorHover:Texture;
		
		private var oreMineUp:Texture;
		private var oreMineHover:Texture;
		
		private var factoryUp:Texture;
		private var factoryHover:Texture;
		
		private var monitorUp:Texture;
		private var monitorHover:Texture;
		
		private var fBaseUp:Texture;
		private var fBaseHover:Texture;
		
		private var cannonUp:Texture;
		private var cannonHover:Texture;
		
		private var turretUp:Texture;
		private var turretHover:Texture;
		
		private var wallUp:Texture;
		private var wallHover:Texture;
		
		private var fortressUp:Texture;
		private var fortressHover:Texture;
		
		private var domeUp:Texture;
		private var domeHover:Texture;
		
		/**
		 * Spawn List
		 */
		private var frigateUp:Texture;
		private var frigateHover:Texture;
		
		private var engineerUp:Texture;
		private var engineerHover:Texture;
		
		private var destroyerUp:Texture;
		private var destroyerHover:Texture;
		
		private var kelvinUp:Texture;
		private var kelvinHover:Texture;
		
		private var corvetteUp:Texture;
		private var corvetteHover:Texture;
		
		private var carrierUp:Texture;
		private var carrierHover:Texture;
		
		private var infiltratorUp:Texture;
		private var infiltratorHover:Texture;
		
		private var scoutUp:Texture;
		private var scoutHover:Texture;
		
		private var droneUp:Texture;
		private var droneHover:Texture;
		
		private var strikerUp:Texture;
		private var strikerHover:Texture;
		
		/**
		 * Building Actions
		 */
		private var spawnEngiUp:Texture;
		private var spawnEngiHover:Texture;
		
		private var hovered:int;
		
		public function ActionsWheel()
		{
			super();
			hovered = 0;
			/**
			 * Ship actions
			 */
			moveUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("moveUp"));
			moveHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("moveHover"));
			
			attackUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("attackUp"));
			attackHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("attackHover"));
			
			buildUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("buildUp"));
			buildHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("buildHover"));
			
			sniffUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("sniffUp"));
			sniffHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("sniffHover"));
			
			cloakUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("cloakUp"));
			cloakHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("cloakHover"));
			
			unCloakUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("unCloakUp"));
			unCloakHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("unCloakHover"));
			
			repairUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("repairUp"));
			repairHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("repairHover"));
			
			recycleUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("recycleUp"));
			recycleHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("recycleHover"));
			
			zombifyUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("zombifyUp"));
			zombifyHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("zombifyHover"));

			healUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("healUp"));
			healHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("healHover"));
			
			sabotageUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("sabotageUp"));
			sabotageHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("sabotageHover"));
			
			releaseUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("releaseUp"));
			releaseHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("releaseHover"));
			
			/**
			 * Build list
			 */
			reactorUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("reactorUp"));
			reactorHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("reactorHover"));
			
			oreMineUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("oreMineUp"));
			oreMineHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("oreMineHover"));
			
			factoryUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("factoryUp"));
			factoryHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("factoryHover"));
			
			monitorUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("monitorUp"));
			monitorHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("monitorHover"));
			
			fBaseUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("baseUp"));
			fBaseHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("baseHover"));
			
			cannonUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("cannonUp"));
			cannonHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("cannonHover"));
			
			turretUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("turretUp"));
			turretHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("turretHover"));
			
			wallUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("wallUp"));
			wallHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("wallHover"));
			
			fortressUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("fortressUp"));
			fortressHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("fortressHover"));
			
			domeUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("domeUp"));
			domeHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("domeHover"));
			
			/**
			 * Spawn list
			 */
			frigateUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("frigateUp"));
			frigateHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("frigateHover"));
			
			engineerUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("engineerUp"));
			engineerHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("engineerHover"));
			
			destroyerUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("destroyerUp"));
			destroyerHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("destroyerHover"));
			
			kelvinUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("kelvinUp"));
			kelvinHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("kelvinHover"));
			
			corvetteUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("corvetteUp"));
			corvetteHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("corvetteHover"));
			
			carrierUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("carrierUp"));
			carrierHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("carrierHover"));
			
			infiltratorUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("infiltratorUp"));
			infiltratorHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("infiltratorHover"));
			
			scoutUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("scoutUp"));
			scoutHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("scoutHover"));
			
			droneUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("droneUp"));
			droneHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("droneHover"));
			
			strikerUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("strikerUp"));
			strikerHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("strikerHover"));
			
			/**
			 * Building Actions
			 */
			spawnEngiUp = Texture.fromTexture(Assets.getHudAtlas().getTexture("spawnEngiUp"));
			spawnEngiHover = Texture.fromTexture(Assets.getHudAtlas().getTexture("spawnEngiHover"));
			
			bg = new Image(Assets.getHudAtlas().getTexture("inactive"));
			addChild(bg);
			//bg.addEventListener(TouchEvent.TOUCH, onBgTouch);
			info = new Image(Assets.getHudAtlas().getTexture("actionWheelInfo"));
			infoSec = new Image(Assets.getHudAtlas().getTexture("actionWheelInfo2"));
			
			insufficientF = new Alert(Assets.getHudAtlas().getTexture("insufficientFunds"));
			addChild(info);
			//addChild(infoSec);
			isOn = false;	
			isBuild = false;
			isSpawn = false;
			//addEventListener(Event.ADDED_TO_STAGE, onAdded);
			initPointers();
			initButtons();
		}
		
		/*private function onBgTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			trace("touching bg");
			if(touch.phase == TouchPhase.HOVER)
			{
				if(hovered)
				{
					trace("dishovered an action");
					switch(hovered)
					{
						case 1:{move.texture = moveUp; break;}
					}
					hovered = 0;
				}
			}
		}*/
		
		private function onMove():void
		{
			Data.action = 1;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "move");
		}
		
		private function onAttack():void
		{
			Data.action = 2;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "attack");
		}
		
		private function onBuild():void
		{
			Data.action = 3;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "build");
		}
		
		private function onSniff():void
		{
			Data.action = 4;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "sniff");
		}
		
		private function onCloak():void
		{
			Data.action = 5 ;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "cloak");
		}
		
		private function onRepair():void
		{
			Data.action = 6;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "repair");
		}
		
		private function onRecycle():void
		{
			Data.action = 7;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "recycle");
		}
		
		private function onZombify():void
		{
			Data.action = 8;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "zombify");
		}
		
		private function onHeal():void
		{
			Data.action = 9;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "heal");
		}
		
		private function onSabotage():void
		{
			Data.action = 10;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "sabotage");
		}
		
		private function onRelease():void
		{
			Data.action = 11;
			dispatchEventWith(ActionSelectedEvent.SELECTED_ACTION, true, "release");
		}
		
		/**
		 * Build List
		 */
		private function onReactor():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "reactor");
		}
		
		private function onOreMine():void
		{
			trace("mine build touched");
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "oreMine");
		}
		
		private function onFactory():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "factory");
		}
		
		private function onMonitor():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "monitor");
		}
		
		private function onBase():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "base");
		}
		
		private function onCannon():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "cannon");
		}
		
		private function onTurret():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "turret");
		}
		
		private function onWall():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "wall");
		}
		
		private function onFortress():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "fortress");
		}
		
		private function onDome():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_BUILD, true, "dome");
		}
		
		/**
		 * Spawn List
		 */
		private function onFrigate():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "frigate");
		}
		
		private function onEngineer():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "engineer");
		}
		
		private function onDestroyer():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "destroyer");
		}
		
		private function onKelvin():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "kelvin");
		}
		
		private function onCorvette():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "corvette");
		}
		
		private function onCarrier():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "carrier");
		}
		
		private function onInfiltrator():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "infiltrator");
		}
		
		private function onScout():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "scout");
		}
		
		private function onDrone():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "drone");
		}
		
		private function onStriker():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "striker");
		}
		
		/**
		 * Building Actions
		 */
		private function onSpawnEngi():void
		{
			dispatchEventWith(ActionSelectedEvent.SELECTED_SPAWN, true, "engineer");	
		}
		
		private function initButtons():void
		{
			/**
			 * Ship Actions
			 */
			move = new Button();
			move.defaultSkin = new Image(moveUp);
			move.hoverSkin = new Image(moveHover);
			move.x = 300;
			move.y = 72;
			move.addEventListener(Event.TRIGGERED, onMove);
			
			attack = new Button();
			attack.defaultSkin = new Image(attackUp);
			attack.hoverSkin = new Image(attackHover);
			attack.x = 300;
			attack.y = 360;
			attack.addEventListener(Event.TRIGGERED, onAttack);
			
			build = new Button();
			build.defaultSkin = new Image(buildUp);
			build.hoverSkin = new Image(buildHover);
			build.x = 300;
			build.y = 360;
			build.addEventListener(Event.TRIGGERED, onBuild);
			
			sniff = new Button();
			sniff.defaultSkin = new Image(sniffUp);
			sniff.hoverSkin = new Image(sniffHover);
			sniff.x = 310;
			sniff.y = 300;
			sniff.addEventListener(Event.TRIGGERED, onSniff);
			
			cloak = new Button();
			cloak.defaultSkin = new Image(cloakUp);
			cloak.hoverSkin = new Image(cloakHover);
			cloak.x = 550;
			cloak.y = 230;
			cloak.addEventListener(Event.TRIGGERED, onCloak);
			
			repair = new Button();
			repair.defaultSkin = new Image(repairUp);
			repair.hoverSkin = new Image(repairHover);
			repair.x = 566;
			repair.y = 222;
			repair.addEventListener(Event.TRIGGERED, onRepair);
			
			recycle = new Button();
			recycle.defaultSkin = new Image(recycleUp);
			recycle.hoverSkin = new Image(recycleHover);
			recycle.x = 16;
			recycle.y = 222;
			recycle.addEventListener(Event.TRIGGERED, onRecycle);
			
			zombify = new Button();
			zombify.defaultSkin = new Image(zombifyUp);
			zombify.hoverSkin = new Image(zombifyHover);
			zombify.x = 310;
			zombify.y = 0;
			zombify.addEventListener(Event.TRIGGERED, onZombify);
			
			heal = new Button();
			heal.defaultSkin = new Image(healUp);
			heal.hoverSkin = new Image(healHover);
			heal.x = 310;
			heal.y = 0;
			heal.addEventListener(Event.TRIGGERED, onHeal);
			
			sabotage = new Button();
			sabotage.defaultSkin = new Image(sabotageUp);
			sabotage.hoverSkin = new Image(sabotageHover);
			sabotage.x = 310;
			sabotage.y = 0;
			sabotage.addEventListener(Event.TRIGGERED, onSabotage);
			
			release = new Button();
			release.defaultSkin = new Image(releaseUp);
			release.hoverSkin = new Image(releaseHover);
			release.x = 310;
			release.y = 0;
			release.addEventListener(Event.TRIGGERED, onRelease);
			
			/**
			 * Build list
			 */
			reactor = new Button();
			reactor.defaultSkin = new Image(reactorUp);
			reactor.hoverSkin = new Image(reactorHover);
			reactor.x = 31;
			reactor.y = 86;
			reactor.addEventListener(Event.TRIGGERED, onReactor);
			
			oreMine = new Button();
			oreMine.defaultSkin = new Image(oreMineUp);
			oreMine.hoverSkin = new Image(oreMineHover);
			oreMine.x = 185;
			oreMine.y = 86;
			oreMine.addEventListener(Event.TRIGGERED, onOreMine);
			
			factory = new Button();
			factory.defaultSkin = new Image(factoryUp);
			factory.hoverSkin = new Image(factoryHover);
			factory.x = 339;
			factory.y = 86;
			factory.addEventListener(Event.TRIGGERED, onFactory);
			
			monitor = new Button();
			monitor.defaultSkin = new Image(monitorUp);
			monitor.hoverSkin = new Image(monitorHover);
			monitor.x = 494;
			monitor.y = 86;
			monitor.addEventListener(Event.TRIGGERED, onMonitor);
			
			fBase = new Button();
			fBase.defaultSkin = new Image(fBaseUp);
			fBase.hoverSkin = new Image(fBaseHover);
			fBase.x = 649;
			fBase.y = 86;
			fBase.addEventListener(Event.TRIGGERED, onBase);
			
			cannon = new Button();
			cannon.defaultSkin = new Image(cannonUp);
			cannon.hoverSkin = new Image(cannonHover);
			cannon.x = 31;
			cannon.y = 330;
			cannon.addEventListener(Event.TRIGGERED, onCannon);
			
			turret = new Button();
			turret.defaultSkin = new Image(turretUp);
			turret.hoverSkin = new Image(turretHover);
			turret.x = 185;
			turret.y = 330;
			turret.addEventListener(Event.TRIGGERED, onTurret);
			
			wall = new Button();
			wall.defaultSkin = new Image(wallUp);
			wall.hoverSkin = new Image(wallHover);
			wall.x = 339;
			wall.y = 330;
			wall.addEventListener(Event.TRIGGERED, onWall);
			
			fortress = new Button();
			fortress.defaultSkin = new Image(fortressUp);
			fortress.hoverSkin = new Image(fortressHover);
			fortress.x = 494;
			fortress.y = 330;
			fortress.addEventListener(Event.TRIGGERED, onFortress);
			
			dome = new Button();
			dome.defaultSkin = new Image(domeUp);
			dome.hoverSkin = new Image(domeHover);
			dome.x = 649;
			dome.y = 330;
			dome.addEventListener(Event.TRIGGERED, onDome);
			
			/**
			 * Spawn List
			 */
			frigate = new Button();
			frigate.defaultSkin = new Image(frigateUp);
			frigate.hoverSkin = new Image(frigateHover);
			frigate.x = 31;
			frigate.y = 86;
			frigate.addEventListener(Event.TRIGGERED, onFrigate);
			
			engineer = new Button();
			engineer.defaultSkin = new Image(engineerUp);
			engineer.hoverSkin = new Image(engineerHover);
			engineer.x = 185;
			engineer.y = 86;
			engineer.addEventListener(Event.TRIGGERED, onEngineer);
			
			destroyer = new Button();
			destroyer.defaultSkin = new Image(destroyerUp);
			destroyer.hoverSkin = new Image(destroyerHover);
			destroyer.x = 339;
			destroyer.y = 86;
			destroyer.addEventListener(Event.TRIGGERED, onDestroyer);
			
			kelvin = new Button();
			kelvin.defaultSkin = new Image(kelvinUp);
			kelvin.hoverSkin = new Image(kelvinHover);
			kelvin.x = 494;
			kelvin.y = 86;
			kelvin.addEventListener(Event.TRIGGERED, onKelvin);
			
			corvette = new Button();
			corvette.defaultSkin = new Image(corvetteUp);
			corvette.hoverSkin = new Image(corvetteHover);
			corvette.x = 649;
			corvette.y = 86;
			corvette.addEventListener(Event.TRIGGERED, onCorvette);
			
			carrier = new Button();
			carrier.defaultSkin = new Image(carrierUp);
			carrier.hoverSkin = new Image(carrierHover);
			carrier.x = 31;
			carrier.y = 330;
			carrier.addEventListener(Event.TRIGGERED, onCarrier);
			
			infiltrator = new Button();
			infiltrator.defaultSkin = new Image(infiltratorUp);
			infiltrator.hoverSkin = new Image(infiltratorHover);
			infiltrator.x = 185;
			infiltrator.y = 330;
			infiltrator.addEventListener(Event.TRIGGERED, onInfiltrator);
			
			scout = new Button();
			scout.defaultSkin = new Image(scoutUp);
			scout.hoverSkin = new Image(scoutHover);
			scout.x = 339;
			scout.y = 330;
			scout.addEventListener(Event.TRIGGERED, onScout);
			
			drone = new Button();
			drone.defaultSkin = new Image(droneUp);
			drone.hoverSkin = new Image(droneHover);
			drone.x = 494;
			drone.y = 330;
			drone.addEventListener(Event.TRIGGERED, onDrone);
			
			striker = new Button();
			striker.defaultSkin = new Image(strikerUp);
			striker.hoverSkin = new Image(strikerHover);
			striker.x = 649;
			striker.y = 330;
			striker.addEventListener(Event.TRIGGERED, onStriker);
			
			/**
			 * Building actions
			 */
			spawnEngi = new Button();
			spawnEngi.defaultSkin = new Image(spawnEngiUp);
			spawnEngi.hoverSkin = new Image(spawnEngiHover);
			spawnEngi.x = 300;
			spawnEngi.y = 72;
			spawnEngi.addEventListener(Event.TRIGGERED, onSpawnEngi);
		}
		
		private function initPointers():void
		{
			var tex:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("pointer"));
			for(var i:int=0; i<8; i++)
			{
				var pImage:Image = new Image(tex);
				pImage.y = 240;
				pImage.x = 390;
				switch(i)
				{
					case 0:
					{
						//pImage.x = 
						//pImage.rotation = deg2rad(360);
						//pImage.x = 5;
						pImage.x -=3;
						pImage.y +=30;
						break;
					}
					case 1:
					{
						pImage.rotation = deg2rad(45);
						break;
					}
					case 2:
					{
						pImage.rotation = deg2rad(90);
						pImage.x = 310;
						pImage.y = 253;
						break;
					}
					case 3:
					{
						pImage.rotation = deg2rad(135);
						break;
					}
					case 4:
					{
						pImage.rotation = deg2rad(180);
						pImage.y -= 5;
						break;
					}
					case 5:
					{
						pImage.rotation = deg2rad(225);
						break;
					}
					case 6:
					{
						pImage.rotation = deg2rad(270);
						pImage.x = 470;
						pImage.y = 253;
						break;
					}
					case 7:
					{
						pImage.rotation = deg2rad(315);
						break;
					}
				}
				pointers.push(pImage);
			}
		}
	}
}