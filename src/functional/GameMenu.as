package functional
{
	//import citrus.core.starling.StarlingCitrusEngine;
	
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.motion.transitions.ScreenFadeTransitionManager;
	import feathers.motion.transitions.ScreenSlidingStackTransitionManager;
	import feathers.themes.MetalWorksMobileTheme;
	
	import functional.SkroneTheme;
	
	import playerio.*;
	
	import screens.ArenaScreen;
	import screens.CreateGameScreen;
	import screens.FactionScreen;
	import screens.GamesListScreen;
	import screens.HangarScreen;
	import screens.HelpScreen;
	import screens.LobbyScreen;
	import screens.MainMenuScreen;
	import screens.MapScreen;
	import screens.MissionsScreen;
	import screens.PlayScreen;
	import screens.ProfileLeaderboardScreen;
	import screens.ProfileScreen;
	import screens.Registration1Screen;
	import screens.Registration2Screen;
	import screens.Registration3Screen;
	import screens.ResearchScreen;
	import screens.SocialScreen;
	import screens.StoreScreen;
	import screens.TitleLeaderboardScreen;
	import screens.TitleScreen;
	
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	
	
	//[SWF(width="1024", height="768", frameRate="60", backgroundColor="#000000")]
	public class GameMenu extends Sprite
	{
		public static const TITLE:String = "titleScreen";
		public static const REGISTRATION1:String = "registration1";
		public static const REGISTRATION2:String = "registration2";
		public static const REGISTRATION3:String = "registration3";
		public static const MAIN_MENU:String = "mainMenu";
		public static const LOBBY:String = "lobby";
		public static const RESEARCH:String = "research";
		public static const PROFILE:String = "profile";
		//public static const ALLIANCE:String = "alliance";
		public static const TITLE_LEADERBOARD:String = "titleLeaderboard";
		public static const PROFILE_LEADERBOARD:String = "profileLeaderboard";
		public static const STORE:String = "store";
		public static const FACTION:String = "faction";
		public static const SOCIAL:String = "social";
		public static const HANGAR:String = "hangar";
		public static const HELP:String = "help";
		public static const ARENA:String = "arena";
		public static const MISSIONS:String = "missions";
		public static const PLAY:String = "play";
		public static const MAP:String = "map";
		public static const GAMES_LIST:String = "gameList";
		public static const CREATE_GAME:String = "createGame";
		
		public function GameMenu()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			this.addEventListener("startGame", onStartGame);
		}
		
		private function onStartGame():void
		{
			trace("wanna start game");
			removeEventListener("startGame", onStartGame);
			dispatchEventWith("startGame", true);
		}
		
		private var _theme:SkroneTheme;
		//private var _theme:MetalWorksMobileTheme;
		//private var _theme:AeonDesktopTheme;
		private var _navigator:ScreenNavigator;
		private var _transitionManager:ScreenFadeTransitionManager;
		//private var _transitionManager:ScreenSlidingStackTransitionManager;
		
		private function addedToStageHandler(event:Event):void
		{

			this._theme = new SkroneTheme(this.stage);
			//this._theme = new AeonDesktopTheme(this.stage);
			
			this._navigator = new ScreenNavigator();
			this.addChild(this._navigator);
			
			this._navigator.addScreen(TITLE, new ScreenNavigatorItem(TitleScreen,
				{
					showRegistration2: REGISTRATION2,
					showMainMenu: MAIN_MENU,
					showLeaderboard: TITLE_LEADERBOARD
					//attemptLogin: 
				}));
			
			this._navigator.addScreen(MAIN_MENU, new ScreenNavigatorItem(MainMenuScreen,
				{
					showPlay: PLAY,
					showHelp: HELP,
					showHangar: HANGAR,
					showSocial: SOCIAL,
					showFaction: FACTION,
					//showAlliance: ALLIANCE,
					showProfile: PROFILE,
					showTitle: TITLE,
					showStore: STORE,
					showLeaderboard: PROFILE_LEADERBOARD
				}));
			
			this._navigator.addScreen(HANGAR, new ScreenNavigatorItem(HangarScreen,
				{
					
				}));
			
			this._navigator.addScreen(REGISTRATION1, new ScreenNavigatorItem(Registration1Screen,
				{
					showRegistration2: REGISTRATION2,
					showRegistration3: REGISTRATION3
				}));
			
			this._navigator.addScreen(REGISTRATION2, new ScreenNavigatorItem(Registration2Screen,
				{
					showTitle: TITLE,
					showRegistration1: REGISTRATION1
				}));
			
			this._navigator.addScreen(REGISTRATION3, new ScreenNavigatorItem(Registration3Screen,
				{
					showTitle: TITLE
				}));
			
			this._navigator.addScreen(LOBBY, new ScreenNavigatorItem(LobbyScreen,
				{
					showArena: ARENA
				}));
			
			this._navigator.addScreen(RESEARCH, new ScreenNavigatorItem(ResearchScreen,
				{
					showHangar: HANGAR
				}));
			
			this._navigator.addScreen(PROFILE, new ScreenNavigatorItem(ProfileScreen,
				{
					showMainMenu: MAIN_MENU,
					showLeaderboard: PROFILE_LEADERBOARD
				}));
			
			/*this._navigator.addScreen(ALLIANCE, new ScreenNavigatorItem(StoreScreen,
				{
					
				}));*/
			
			this._navigator.addScreen(TITLE_LEADERBOARD, new ScreenNavigatorItem(TitleLeaderboardScreen,
				{
					showTitle: TITLE
				}));
			
			this._navigator.addScreen(STORE, new ScreenNavigatorItem(StoreScreen,
				{
					showMainMenu: MAIN_MENU
				}));
			
			this._navigator.addScreen(FACTION, new ScreenNavigatorItem(FactionScreen,
				{
					showMainMenu: MAIN_MENU
				}));
			
			this._navigator.addScreen(SOCIAL, new ScreenNavigatorItem(SocialScreen,
				{
					showMainMenu: MAIN_MENU
				}));
			
			this._navigator.addScreen(PLAY, new ScreenNavigatorItem(PlayScreen,
				{
					showMainMenu: MAIN_MENU,
					showArena: ARENA,
					showMap: MAP,
					showMissions: MISSIONS
				}));
			
			this._navigator.addScreen(MISSIONS, new ScreenNavigatorItem(MissionsScreen,
				{
					showPlay: PLAY
				}));
			
			this._navigator.addScreen(ARENA, new ScreenNavigatorItem(ArenaScreen,
				{
					showPlay: PLAY,
					showLobby: LOBBY,
					showGamesList: GAMES_LIST
				}));
			
			this._navigator.addScreen(HELP, new ScreenNavigatorItem(HelpScreen,
				{
					showMainMenu: MAIN_MENU
				}));
			
			this._navigator.addScreen(MAP, new ScreenNavigatorItem(MapScreen,
				{
					showPlay: PLAY
				}));
			
			this._navigator.addScreen(GAMES_LIST, new ScreenNavigatorItem(GamesListScreen,
				{
					showArena: ARENA,
					showLobby: LOBBY,
					showCreateGame: CREATE_GAME
				}));
			
			this._navigator.addScreen(CREATE_GAME, new ScreenNavigatorItem(CreateGameScreen,
				{
					showArena: ARENA,
					showLobby: LOBBY
				}));
			
			this._navigator.addScreen(PROFILE_LEADERBOARD, new ScreenNavigatorItem(ProfileLeaderboardScreen,
				{
					showProfile: PROFILE,
					showMainMenu: MAIN_MENU
				}));
			
			this._navigator.showScreen(TITLE);
			this._transitionManager = new ScreenFadeTransitionManager(this._navigator);
			this._transitionManager.duration = 1;
			
		}
	}
}