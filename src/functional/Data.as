package functional
{
	import functional.Assets;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public dynamic class Data
	{
		public static var derp:String = "test";
		public static var rank:String;
		public static var faction:uint = 1; //1 - Korbis  2-Tosse
		
		public static var p1:String = "test";
		public static var p2:String;
		public static var localP:int;
		
		public static var rating:int;
		public static var credits:int;
		//not a good idea
		
		public static var _usr:String;
		public static var _pss:String;
		
		public static var regiFaction:String = "0";
		
		public static var tileSelected:Boolean = false;
		public static var selectedX:int = -1;
		public static var selectedY:int = -1;
		
		public static var rightmostSelectedAux:int = -1; 
		
		public static var actionSelX:int = -1;
		public static var actionSelY:int = -1;
		public static var actionRange:int = -1;
		public static var aMoveRange:int = -1;
		public static var rangeT:int = 0; //0 = rectangle 		1 = diamond
		public static var selIndex:int = 0;
		
		public static var oldTileX:int = -1;
		public static var oldTileY:int = -1;
		
		public static var timeM:uint = 9;
		public static var timeS:uint = 59;
		
		public static var pTurn:uint = 0;
		public static var ticksPerSec:uint = 0;
		
		public static var ore:uint = 500;
		public static var orePerTurn:uint = 100;
		
		public static var energy:uint = 200;		//current avaliable energy
		public static var maxEnergy:uint = 400;		//total energy - used energy
		public static var usedEnergy:uint = 200;    //energy used by structures
		public static var totalEnergy:uint = 600;	//total energy
		
		public static var moveRanges:Array = new Array();
		
		public static var hasTurn:Boolean = false;
		
		public static var mapTiles:Array = new Array();
		
		public static var action:int = 0;
		public static var cancelAction:Boolean = false;
		
		public static var aBuild:String = "derp";
		public static var alertAnimDone:Boolean = true;
		
		public static var ships:Array = new Array();
		
		public static var structs:Array = new Array();
		
		public static var emptyTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tile"));
		public static var enemyTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileEnemy"));
		public static var friendlyTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileFriendly"));
		public static var selectedTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileSelected"));
		public static var actionTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileActionHover"));
		public static var hiddenTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileHidden"));
		public static var rangeTileSkin:Texture = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileRange"));
		
		public static var shieldDmg:int;
		public static var hpDmg:int;
		public static var moraleDmg:int;
		public static var died:Boolean;
		
		
		public static var movesIconNum:Image = new Image(Assets.getHudAtlas().getTexture("statIconsMoves1"));
		public static var fireIcon:Image = new Image(Assets.getHudAtlas().getTexture("statIconsFire"));
		public static var shieldsDownIcon:Image = new Image(Assets.getHudAtlas().getTexture("statIconsShield"));
		public static var noMovesIcon:Image = new Image(Assets.getHudAtlas().getTexture("noMovesIcon"));

		
		public static var cmx:int;
		public static var cmy:int;
		
		public static var floaters:Array = new Array;
		public static var floatAnimDone:Boolean = true;
		public static var alertType:String = "jesus";
		
		public static var offline:Boolean = false;
		public static var resolution:int = 1;
		public static var resH:int = 768;
		public static var resW:int = 1024;
		
		/**
		 * Global event "listeners"
		 */
		public static var hitLogin:Boolean = false;
		public static var hitStartGame:Boolean = false;
		public static var gameEnded:Boolean = false;
		
		public function Data()
		{
			
		}
	}
}