package functional
{
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.Font;
	
	import functional.Assets;
	
	import starling.animation.Juggler;
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.MatrixUtil;
	import starling.utils.VAlign;
	
	public class Hud extends Sprite
	{	
		/**
		 * Selected Item
		 */	
		public var siName:String = "ship";
		public var siNameText:TextField;

		public var siHpText:TextField;
		
		public var siShieldText:TextField;

		public var siMoraleText:TextField;
		
		public var siTimeTotal:int;
		public var siTimeSecs:int;
		public var siTimeMins:int;
		public var siTimeText:TextField;
		public var siTimeString:String;
		
		public var siOre:int;
		public var siOreText:TextField;
		
		public var siEnergy:int;
		public var siEnergyText:TextField;
		
		public var turnText:TextField;
		
		public var hpBg:Image;
		public var shieldBg:Image;
		public var moraleBg:Image;
		public var avatarBg:Image;

		public var noRepair:Alert = new Alert(Assets.getHudAtlas().getTexture("noRepair"));
		public var noBuild:Alert = new Alert(Assets.getHudAtlas().getTexture("noBuild"));
		public var noEnemies:Alert = new Alert(Assets.getHudAtlas().getTexture("noEnemies"));
		public var noMove:Alert = new Alert(Assets.getHudAtlas().getTexture("noMove"));
		public var noRecycle:Alert = new Alert(Assets.getHudAtlas().getTexture("noRecycle"));
		public var noZombify:Alert = new Alert(Assets.getHudAtlas().getTexture("noZombify"));
		public var noHeal:Alert = new Alert(Assets.getHudAtlas().getTexture("noHeal"));
		public var noSabotage:Alert = new Alert(Assets.getHudAtlas().getTexture("noSabotage"));
		//public var tileOccupied:Alert = new Alert(Assets.getHudAtlas().getTexture("no
		
		public var noMovesRemaining:Alert = new Alert(Assets.getHudAtlas().getTexture("noMovesRemaining"));
		public var deploymentZoneOccupied:Alert = new Alert(Assets.getHudAtlas().getTexture("deploymentZoneOccupied"));
		
		public var cTarget:Image;
		public var downBarBg:Image;
		public var resBg:Image;
		public var timeBg:Image;
		public var minimapBg:Image;
		public var endTurn:Image;
		public var avatar:Image;
		public var cancelActionWarning:Image;
		
		public var selActionWarning:Alert = new Alert(Assets.getHudAtlas().getTexture("selActionAlert"));
		
		public var yourTurnWarning:Alert = new Alert(Assets.getHudAtlas().getTexture("yourTurnWarning"));
		public var enemyTurnWarning:Alert = new Alert(Assets.getHudAtlas().getTexture("enemyTurnWarning"));
		public var outOfRange:Alert = new Alert(Assets.getHudAtlas().getTexture("outOfRange"));
		public var notYourTurn:Alert = new Alert(Assets.getHudAtlas().getTexture("notYourTurn"));
		
		public var arrowU:Image = new Image(Assets.getHudAtlas().getTexture("arrowUp"));
		public var arrowD:Image = new Image(Assets.getHudAtlas().getTexture("arrowDown"));
		public var arrowR:Image = new Image(Assets.getHudAtlas().getTexture("arrowRight"));
		public var arrowL:Image = new Image(Assets.getHudAtlas().getTexture("arrowLeft"));
		
		public var isU:Boolean = false, isD:Boolean = false, isL:Boolean = false, isR:Boolean = false;
		
		public var siHpQuad:Image;
		public var siShieldQuad:Image;
		public var siMoraleQuad:Image;
		
		public var siHpQ:Quad = new Quad(153, 10, 0xf7941d);
		public var siShieldQ:Quad = new Quad(153, 10, 0xf7941d);
		public var siMoraleQ:Quad = new Quad(153, 10, 0xf7941d);
		
		public var cantBuild:TextField;
		
		public var downBarContainer:Sprite = new Sprite();
		
		public var selectedState:int;
		/**
		 * 0 = nothing selected
		 * 1 = single friendly selected
		 * 2 = multiple friendlies selected
		 * 3 = single enemy selected
		 * 4 = multiple enemies selected
		 */
		
		//private static var instantiationLock:Boolean = true;
		//private static var _instance:Hud;
		
		public var emptyArea:Image;
		
		/**
		 * 1-Move			/
		 * 2-Attack			/
		 * 3-Build			/
		 * 4-Sniff			/
		 * 5-Cloak			/
		 * 6-Repair			/
		 * 7-Recycle		/
		 * 8-Zombify		/
		 * 9-Heal			/
		 * 10-Sabotage		/
		 * 11-Release (heat)/	
		 */
		
		public var buildingsContainer:Sprite = new Sprite();
		public var buildingsList:Array = new Array();
		public var building:uint
		
		/**
		 * 		1-Base							= Spawns Engineers; allows buildings to be errected in a set area around it; 2 horizontal tiles anywhere
		 * 		2-Reactor						= Produces energy that isn't wasted; buildings take a chunk of the current energy, but upon destruction, reuturn it to the pool;
		 * 										  so current (1) energy = energy capacity - energy used by buildings; units take energy for spawning but do not retain it, so (1)
		 * 									      remains the same; energy taken by units in that turn is replinished in the next turn to (1); 1 tile on anomaly
		 * 		3-Ore Mine						= Produces money each turn; it is wasted; 1 tile on ore
		 * 		4-Deployment Station			= Summons ships; requires energy to do it
		 * 		5-Ion Cannon					= Single shot powerful cannon
		 * 		6-Monitoring Array				= Reveals map, without concealed enemies
		 * 		7-Turret						= Shoots on sight both on f. and e. turn
		 * 		8-Wall							= It's a wall ffs
		 * 		9-Fortress						= Stationery well...fortress
		 * 		10-Concealing Tower				= Makes nearby friendlies invisible to the enemy 
		*/
		
		public function Hud()
		{
			super();
			
			isU = false;
			isD = false;
			isR = false;
			isL = false;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			/*var t:Tween = new Tween(this, 2, Transitions.EASE_IN);
			t.fadeTo(0);
			Starling.juggler.add(t);*/
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			timeBg = new Image(Assets.getHudAtlas().getTexture("time"));
			addChild(timeBg);
			
			endTurn = new Image(Assets.getHudAtlas().getTexture("endTurn"));
			
			resBg = new Image(Assets.getHudAtlas().getTexture("resources"));
			addChild(resBg);
			
			minimapBg = new Image(Assets.getHudAtlas().getTexture("miniMap"));
			addChild(minimapBg);
			
			downBarBg = new Image(Assets.getHudAtlas().getTexture("hudBarDownBg"));
			downBarContainer.addChild(downBarBg);
			
			avatarBg = new Image(Assets.getHudAtlas().getTexture("eAvatarFrame"));
			downBarContainer.addChild(avatarBg);
			
			//avatar = new Image(Assets.getSSAtlas().getTexture("Engineer"));
			//downBarContainer.addChild(avatar);		
			
			hpBg = new Image(Assets.getHudAtlas().getTexture("hullBg"));
			//downBarContainer.addChild(hpBg);
			
			shieldBg = new Image(Assets.getHudAtlas().getTexture("shieldBg"));
			//downBarContainer.addChild(shieldBg);
			
			moraleBg = new Image(Assets.getHudAtlas().getTexture("moraleBg"));
			//downBarContainer.addChild(moraleBg);
			
			turnText = new TextField(200, 40, "It's not your turn", "Helvetica", 20, 0xffffff);
			turnText.hAlign = HAlign.LEFT;
			turnText.vAlign = VAlign.TOP;
			turnText.x = 350;
			turnText.y = 100;
			
			cantBuild = new TextField(200, 40, "Cannot place here", "Helvetica", 20, 0xffffff);
			cantBuild.hAlign = HAlign.LEFT;
			cantBuild.vAlign = VAlign.TOP;
			cantBuild.x = 350;
			cantBuild.y = 100;
			
			siNameText = new TextField(180, 30, siName, "Helvetica", 20, 0xffffff, true);
			siNameText.hAlign = HAlign.LEFT;
			siNameText.vAlign = VAlign.TOP;
			siNameText.x = 300;
			siNameText.y = 490;
			downBarContainer.addChild(siNameText);
			
			siShieldText = new TextField(50, 20, "123", "Helvetica", 12, 0xffffff);
			siShieldText.hAlign = HAlign.LEFT;
			siShieldText.vAlign = VAlign.TOP;
			siShieldText.x = 397;
			siShieldText.y = 521;
			downBarContainer.addChild(siShieldText);
			
			siHpText = new TextField(50, 20, "123", "Helvetica", 12, 0xffffff);
			siHpText.hAlign = HAlign.LEFT;
			siHpText.vAlign = VAlign.TOP;
			siHpText.x = 397;
			siHpText.y = 546;
			downBarContainer.addChild(siHpText);
			
			siMoraleText = new TextField(50, 20, "123", "Helvetica", 12, 0xffffff);
			siMoraleText.hAlign = HAlign.LEFT;
			siMoraleText.vAlign = VAlign.TOP;
			siMoraleText.x = 397;
			siMoraleText.y = 573;
			downBarContainer.addChild(siMoraleText);

			if(Data.pTurn%2 == Data.localP) //if local player has turn
			{
				addChild(endTurn);
				if(Data.tileSelected == true)
				{		
					addChild(downBarContainer);
				}
			}	

			siOreText = new TextField(70, 30, Data.ore.toString(), "Helvetica", 20, 0xffffff, true);
			siOreText.hAlign = HAlign.LEFT;
			siOreText.vAlign = VAlign.TOP;
			siOreText.x = 650;
			siOreText.y = 170;
			this.addChild(siOreText);
			
			siEnergyText = new TextField(70, 30, Data.energy.toString(), "Helvetica", 20, 0xffffff, true);
			siEnergyText.hAlign = HAlign.LEFT;
			siEnergyText.vAlign = VAlign.TOP;
			siEnergyText.x = 740;
			siEnergyText.y = 170;
			this.addChild(siEnergyText);
			
			//siTimeTotal = Data.time;
			//siTimeString = "0" + (siTimeTotal/60).toString() + " " + (siTimeTotal-siTimeTotal/60*60).toString();
			siTimeText = new TextField(100, 40, "00:00", "Helvetica", 26, 0xffffff, true);
			siTimeText.x = 700;
			siTimeText.y = 117;
			this.addChild(siTimeText);
			
			siHpQ.x = 438;
			siHpQ.y = 551;
			siShieldQ.x = 438;
			siShieldQ.y = 526;
			siMoraleQ.x = 438;
			siMoraleQ.y = 576;
			
			if(Data.resolution == 2)
			{
				downBarContainer.y = 125;
				this.minimapBg.y = 125;
			}
			else
			{
				
			}
		}
		
		public function downBarOut():void
		{
			removeChild(downBarContainer);
			if(Data.resolution == 2)
				downBarContainer.y = 125;
			else
				downBarContainer.y = 0;
		}
		
		public function clearDownBar():void
		{
			/*Data.ships[Data.selIndex].removeChild(Data.movesIconNum);
			if(Data.ships[Data.selIndex].isBurning)
				Data.ships[Data.selIndex].removeChild(Data.fireIcon);
			if(Data.ships[Data.selIndex].shieldsDown)
				Data.ships[Data.selIndex].removeChild(Data.shieldsDownIcon);*/
			
			downBarContainer.removeChild(siShieldQ);
			downBarContainer.removeChild(siHpQ);
			downBarContainer.removeChild(siMoraleQ);
			
			downBarContainer.removeChild(shieldBg);
			downBarContainer.removeChild(hpBg);
			downBarContainer.removeChild(moraleBg);
			
			downBarContainer.removeChild(siShieldText);
			downBarContainer.removeChild(siHpText);
			downBarContainer.removeChild(siMoraleText);	
		}
		
		public function showStructHud(hp:int, hpMax:int):void
		{
			var hpPerc:int;
			hpPerc = hp*100/hpMax;
			//trace("hp: " + hp +"; hpMax: " + hpMax + "; hpPerc: " + hpPerc);
			siHpText.text = hpPerc.toString()+"%";
			//siHpQ.y
			siHpQ.scaleX = hpPerc/100;
			
			downBarContainer.addChild(hpBg);
			downBarContainer.addChild(siHpQ);
			downBarContainer.addChild(siHpText);
		}
		
		public function updateResources():void
		{
			siOreText.text = Data.ore.toString();
			siEnergyText.text = Data.energy.toString();
		}
		
		private var secondaryA:Image = new Image(Assets.getHudAtlas().getTexture("secondaryMove"));
		
		public function showSecondaryAlert():void
		{
			secondaryA.x = 150;
			addChild(secondaryA);
			var t:Tween = new Tween(secondaryA, 0.5, Transitions.LINEAR);
			t.moveTo(0, secondaryA.y);
			if(Data.action == 1) //move
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryMove");
			} else if(Data.action == 2) //attack
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryAttack");
			} else if(Data.action == 3) //build
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryBuild");
			} else if(Data.action == 6) //repair
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryRepair");
			}
			Starling.juggler.add(t);
		}
		
		public function hideSecondaryAlert():void
		{
			secondaryA.x = 0;
			addChild(secondaryA);
			var t:Tween = new Tween(secondaryA, 0.5, Transitions.LINEAR);
			t.moveTo(150, secondaryA.y);
			if(Data.action == 1) //move
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryMove");
			} else if(Data.action == 2) //attack
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryAttack");
			} else if(Data.action == 3) //build
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryBuild");
			} else if(Data.action == 6) //repair
			{
				secondaryA.texture = Assets.getHudAtlas().getTexture("secondaryRepair");
			}
			Starling.juggler.add(t);
			t.onComplete = completeSecondaryA;
		}
		
		private function completeSecondaryA():void
		{
			removeChild(secondaryA);
		}
		
		public function showShipHud():void
		{
			//trace("show ship hud");
			var hpPerc:int, shieldPerc:int, moralePerc:int;
			
			hpPerc = Data.ships[Data.selIndex].hp*100/Data.ships[Data.selIndex].maxHp;
			siHpText.text = hpPerc.toString()+"%";
			siHpQ.scaleX = hpPerc/100;
			
			downBarContainer.addChild(hpBg);
			downBarContainer.addChild(siHpQ);
			downBarContainer.addChild(siHpText);
			
			if(Data.ships[Data.selIndex].hasShield)
			{
				shieldPerc = Data.ships[Data.selIndex].shield*100/Data.ships[Data.selIndex].maxShield;
				siShieldText.text = shieldPerc.toString()+"%";
				
				downBarContainer.addChild(shieldBg);
				siShieldQ.scaleX = shieldPerc/100;
				downBarContainer.addChild(siShieldQ);
				downBarContainer.addChild(siShieldText);
			}
			
			if(Data.ships[Data.selIndex].hasMorale)
			{
				moralePerc = Data.ships[Data.selIndex].morale*100/Data.ships[Data.selIndex].maxMorale;
				siMoraleText.text = moralePerc.toString()+"%";
				
				downBarContainer.addChild(moraleBg);
				siMoraleQ.scaleX = moralePerc/100;
				downBarContainer.addChild(siMoraleQ);
				downBarContainer.addChild(siMoraleText);
			}
		}
	}
}