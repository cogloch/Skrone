package functional
{
	import functional.Assets;
	
	import screens.GameOverScreen;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameOverContainer extends Sprite
	{
		private var lostImg:Image;
		private var wonImg:Image;
		private var gOverScreenBtn:Button;
		
		public var won:int = -1;
		
		public function GameOverContainer()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			if(won == 1)
			{
				wonImg = new Image(Assets.getHudAtlas().getTexture("wonImg"));
				addChild(wonImg);
				
				//play victory music

				addChild(gOverScreenBtn);
			} 
			else if (won == 0)
			{
				lostImg = new Image(Assets.getHudAtlas().getTexture("lostImg"));
				addChild(lostImg);
				
				//play defeat music
				
				addChild(gOverScreenBtn);
			}
		}
	}
}