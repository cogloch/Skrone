package functional
{
	import flash.geom.Point;
	
	import functional.customEvents.MiniMapChangeEvent;
	import functional.customEvents.SelectedTileEvent;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	import utils.TileTouchable;

	//import flash.display.MovieClip;
	
	
	public class Tile extends Sprite
	{
		public var XPosition:int;
		public var YPosition:int;
		
		//public var tempFogFree:Boolean = false;
		
		public var moveRange:int = -1;
		
		public var state:int = 0;
		/**
		 * Tile States:
		 * 		0-Free Tile						= It'sa' freee - Marioo
		 * 
		 * 		1-Ore Tile						= Blocks ships; not destroyable; needed for ore mines
		 * 		2-Anomaly Tile					= Affects any ship in range except Engineers and Infiltrators; needed for reactors
		 * 		3-Junk							= Salvageable by Recyclers
		 * 		4-Asteroid						= Blocks anything; not destroyable
		 * 
		 * 		5-Base							= Spawns Engineers; allows buildings to be errected in a set area around it; 2 horizontal tiles anywhere
		 * 		6-Reactor						= Produces energy that isn't wasted; buildings take a chunk of the current energy, but upon destruction, reuturn it to the pool;
		 * 										  so current (1) energy = energy capacity - energy used by buildings; units take energy for spawning but do not retain it, so (1)
		 * 									      remains the same; energy taken by units in that turn is replinished in the next turn to (1); 1 tile on anomaly
		 * 		7-Ore Mine						= Produces money each turn; it is wasted; 1 tile on ore
		 * 		8-Deployment Station			= Summons ships; requires energy to do it
		 * 		9-Ion Cannon					= Single shot powerful cannon
		 * 		10-Monitoring Array				= Reveals map, without concealed enemies
		 * 		11-Turret						= Shoots on sight both on f. and e. turn
		 * 		12-Wall							= It's a wall ffs
		 * 		13-Fortress						= Stationery well...fortress
		 * 		14-Concealing Tower/Dome		= Makes nearby friendlies invisible to the enemy 
		 * 
		 * 		15-Light Frigates				= The creme' of every fleet; the suflee' of every feat; i'm horrible
		 * 		16-Engineer						= Repairs ships, buildings; builds...buildings; isn't affected by anomalies 
		 * 		17-Destroyer					= Mid-Range AOE
		 * 		18-Kelvin						= Short-Range AOE; greatly affects morale; no -> maybe ignores shields; enemy ship shields must be down 
		 * 		19-Corvette						= Provides Buffing
		 * 		20-Carrier						= Carries stuff; does not deal damage; slow but has strong hull and shields
		 * 		21-Infiltrator					= Can go invisible for as long as it wants to; isn't affected by anomalies; can turn enemy resource production buildings into
		 * 										  zombies, so they work for their liege; zombie buildings are only detectable by sniffers
		 * 		22-Sniffer						= Detects nearby hidden ships; can sniff buildings for their allegience 
		 * 		23-Drone						= Fast but with weak firepower; doesn't have morale; can turn off enemy ship shields
		 * 		24-Striker						= Fast; relies on shields; weak hull
		 * 
		 * 		25-Buildable
		 * 		26-Concealed
		 */
		
		public var team:uint; 
		
		/**
		 * 		0	=	No one's land
		 * 		1   =   Korbis
		 * 		2   =   Tosse
		 */
		
		public var isAnomaly:Boolean = false;
		
		public var isFog:Boolean = false;
		
		public var hiddenByE:Boolean = false;
		public var hiddenByF:Boolean = false;
		
		/**
		 * 		True = Fog of War for local player
		 * 		False = Clear sky-FALL...let it crumbleee
		 */
		
		//public var isBuildableBy1:Boolean=false;
		
		/**
		 * 		True = Yep
		 * 		False = Nope
		 */
		
		//public var isBuildableBy2:Boolean=false;
		
		/**
		 * 		You get the idea...
		 */
		
		public var isBuildable:Boolean = false;
		
		public var isSelected:Boolean = false;
		public var isBorder:Boolean = false;
		public var inRange:Boolean = false;
		
		public var xa:int;
		public var ya:int;
			
		public var tileImg:Image;
		//public var tileBtn:TileTouchable;
		public var tileBtn:Button;
		
		public var sIndex:int;
		
		public function Tile(x:int, y:int)
		{			
			super();
			
			this.xa = x;
			this.ya = y;
			this.XPosition = x * 100;
			this.YPosition = y * 100;
			
			this.state = 0;
			this.touchable = true;
			
			team = 0;
			
			tileImg = new Image(Assets.getHudAtlas().getTexture("tile"));
			tileImg.x = this.XPosition;
			tileImg.y = this.YPosition;
			tileImg.addEventListener(TouchEvent.TOUCH, onTileTouch);
			//tileImg.touchable = true;
			/*tileBtn = new Button(Assets.getHudAtlas().getTexture("tile"));
			tileBtn.x = this.XPosition;
			tileBtn.y = this.YPosition;
			tileBtn.addEventListener(TouchEvent.TOUCH, onTileTouch);*/
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			//trace("added tile at " + XPosition.toString() + " and " + YPosition.toString());
			
		}
		
		private function onAddedToStage():void
		{
			//trace(this.xa + " " + this.ya + " with state = " + state);
			//addEventListener(TouchEvent.TOUCH, fuckTouch);
			//addChild(tileImg);	
			//addChild(tileBtn);
			tileBtn = new Button(Data.emptyTileSkin);
			tileBtn.enabled = true;
			tileBtn.addEventListener(Event.TRIGGERED, onTileTriggered);
			tileBtn.x = this.XPosition;
			tileBtn.y = this.YPosition;
			addChild(tileBtn);
		}
		
		private function onTileTriggered(e:Event):void
		{
			if(Data.hasTurn == false)
			{
				trace("Enemy's turn");
				//_hud.addChild(enemyTurnMessage); 
				//"It's not your turn"
				dispatchEventWith(SelectedTileEvent.SELECTED_TILE, true, state);
			}
			else
			{
				if(Data.action == 0) //no action
				{
					if(isFog == true && Data.action !=1)
					{
						trace("idk what's here");
					}
					else
						if(Data.tileSelected == false) //nothing selected
						{
							if (state!=0)
							{
								Data.tileSelected = true;	
								Data.selectedX = xa;
								Data.selectedY = ya;
								if(state>=15 && state <25)
								{
									trace("heres a ship with index = " + sIndex);
									Data.selIndex = sIndex;
								}
								if(state >=5 && state <15)
									Data.selIndex= sIndex;
								trace("nothing selected now");
								trace("Selecting " + this.XPosition + " " + this.YPosition + " with the state = " + state + " and with sIndex = " + sIndex + " and faction " +
									team);
								tileBtn.upState = Data.selectedTileSkin;
								dispatchEventWith(SelectedTileEvent.SELECTED_TILE, true, state);
								//dispatch event to root
								//show info in hud
								if(state == 5)
								{
									//base
									trace("selected base");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==5)
									{
										trace("selected the left tile");
										Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}
									else
									{
										trace("selected the right tile");
										Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}	
								}
								if(state == 8)
								{
									//base
									trace("selected factory");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==8)
									{
										trace("selected the left tile");
										Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}
									else
									{
										trace("selected the right tile");
										Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}	
								}
							}
							else //nothingness selected
							{
								//do nothing
								//dispatchEventWith(SelectedTileEvent.SELECTED_TILE, true, state);
								trace("Nothing selected");
							}
						}
						else //something already selected
						{
							trace("already selected");
							if(Data.selectedX == xa && Data.selectedY == ya) //the same tile selected
							{
								//do nothing
								trace("Selected the same tile with x=" + Data.selectedX);
							}
							else if(state!=0) //other tile selected
							{
								//deselect
								if(state >=15 && state <25)
									Data.selIndex = sIndex;
								if(state >=5 && state <15)
									Data.selIndex= sIndex;
								if(Data.mapTiles[Data.selectedX][Data.selectedY].team == Data.faction)
								{
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								else if(Data.mapTiles[Data.selectedX][Data.selectedY].team != Data.faction && Data.mapTiles[Data.selectedX][Data.selectedY].team)
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.enemyTileSkin;
								else
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.emptyTileSkin;
								if(Data.mapTiles[Data.selectedX][Data.selectedY].state == 5)
								{
									//base
									trace("deselect base");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==5)
									{
										trace("selected the left tile");
										//Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}
									else
									{
										trace("selected the right tile");
										//Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}	
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								else if(Data.mapTiles[Data.selectedX][Data.selectedY].state == 8)
								{
									//base
									trace("deselect factory");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==8)
									{
										trace("selected the left tile");
										//Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}
									else
									{
										trace("selected the right tile");
										//Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}	
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								//select new tile
								Data.selectedX = xa;
								Data.selectedY = ya;
								trace("Selecting " + this.XPosition + " " + this.YPosition + " with the state = " + state + " and the index = " + sIndex + " index in Data" + 
									Data.selIndex + " with faction = " + team);

								tileBtn.upState = Data.selectedTileSkin;
								if(state == 5)
								{
									//base
									trace("selected base");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==5)
									{
										trace("selected the left tile");
										Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}
									else
									{
										trace("selected the right tile");
										Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}	
								}
								else if(state == 8)
								{
									//base
									trace("selected factory");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==8)
									{
										trace("selected the left tile");
										Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}
									else
									{
										trace("selected the right tile");
										Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
									}	
								}
								dispatchEventWith(SelectedTileEvent.SELECTED_TILE, true, state);
							}
							else //nothingness currently selected
							{
								if(Data.mapTiles[Data.selectedX][Data.selectedY].state == 5)
								{
									//base
									trace("deselect base");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==5)
									{
										trace("selected the left tile");
										//Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}
									else
									{
										trace("selected the right tile");
										//Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}	
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								if(Data.mapTiles[Data.selectedX][Data.selectedY].state == 8)
								{
									//base
									trace("deselect base");
									if(Data.mapTiles[Data.selectedX+1][Data.selectedY].state ==8)
									{
										trace("selected the left tile");
										//Data.rightmostSelectedAux = Data.selectedX+1;
										Data.mapTiles[Data.selectedX+1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}
									else
									{
										trace("selected the right tile");
										//Data.rightmostSelectedAux = Data.selectedX;
										Data.mapTiles[Data.selectedX-1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
									}	
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								//hide action bar
								if(Data.mapTiles[Data.selectedX][Data.selectedY].team == Data.faction)
								{
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
								}
								else if(Data.mapTiles[Data.selectedX][Data.selectedY].team != Data.faction && Data.mapTiles[Data.selectedX][Data.selectedY].team)
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.enemyTileSkin;
								else
									Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.emptyTileSkin;
								Data.tileSelected = false;
								Data.selectedX = -1;
								Data.selectedY = -1;
								Data.selIndex = -1;
								dispatchEventWith(SelectedTileEvent.NOTHING_TILE_SELECTED, true, state);
								//trace("Nothing selected");
							}
						}
				} // no action ends
				else
				{
					if(!inRange)
					{
						//Data.action == 0;
						//Data.action = 0;
						//Data.tileSelected = false;
						Data.cancelAction = true;
						//Data.selIndex = -1;
						trace("cancel action " + Data.action);
					}
					else
					{
						Data.actionSelX = xa;
						Data.actionSelY = ya;
						/**
						 * ^^^^^^^^^^^^^^^^^^^^^^^^^
						 * ARE YOU FUCKING KIDDING ME
						 */
					}
					//trace("sending with cancel action = " + Data.cancelAction);
					dispatchEventWith(SelectedTileEvent.SELECTED_TILE, true, state);
				}
					
			}
			
		}
		
		public function toFog():void
		{
			isFog = true;
			tileBtn.enabled = false;
			tileBtn.upState = Data.hiddenTileSkin;
			tileBtn.removeEventListener(Event.TRIGGERED, onTileTriggered);
			var coords:Point = new Point(xa, ya);
			dispatchEventWith(MiniMapChangeEvent.TO_FOG, true, coords);
		}
		
		public function toClear():void
		{
			if(!isBorder)
			{
				//trace("clearing state " + state);
				isFog = false;
				if(state != -1)
				{
					tileBtn.enabled = true;
					tileBtn.addEventListener(Event.TRIGGERED, onTileTriggered);
					if(state>=5 && state <15)
					{
						trace("structure");
						if(team != Data.faction)
						{
							trace("enemy structure");
							tileBtn.upState = Data.enemyTileSkin;
							//Data.structs[sIndex].visible = true;
							//addChild(Data.structs[sIndex]);
							
						}
					}
					else if(state>=15 && state <25)
					{
						trace("ship from faction" + Data.faction);
						if(team != Data.faction)
						{
							trace("enemy ship with sIndex = " + sIndex);
							trace("ship with that sIndex is at " + Data.ships[sIndex].tX + " " + Data.ships[sIndex].tY);
							tileBtn.upState = Data.enemyTileSkin;
							//Data.ships[sIndex].visbile = true;
							//addChild(Data.ships[sIndex]);
						}	
					}
					else
					{
						//if(state == 4)
							//removeEventListener(Event.TRIGGERED, onTileTriggered);
						tileBtn.upState = Data.emptyTileSkin;
					}
				}
				
			}
		}
		
		public function toFriendly():void
		{
			tileBtn.upState = Data.friendlyTileSkin;
			//tileBtn.enabled = true;
			if(Data.faction == 1)
				team=1;
			else
				team=2;
		}
		
		public function toEnemy():void
		{
			tileBtn.upState = Data.enemyTileSkin;
			//tileBtn.enabled = true;
			if(Data.faction == 1)
				team=2;
			else
				team=1;
		}
		
		private function onTileTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			
			trace("touched");
			
			if(touch.phase == "ended") //clicked
			{
				/*switch(state)
				{
					case 0:
					{
						trace("empty space");
						//do nothing
						//dispatchEventWith(TouchEvent.TOUCH, true);
						break;
					}
					case 1:
					{
						trace("ore");
						break;
					}
					case 2:
					{
						trace("anomaly");
						break;
					}
					case 3:
					{
						trace("junk");
						break;
					}
					case 4:
					{
						trace("asteroid");
						break;
					}
					case 5:
					{
						trace("base");
						//dispatchEventWith(Event.TRIGGERED, true);
						break;
					}
					case 6:
					{
						trace("reactor");
						break;
					}
					case 7:
					{
						trace("ore mine");
						break;
					}
					case 8:
					{
						trace("deployment station");
						break;
					}
					case 9:
					{
						trace("ion cannon");
						break;
					}
					case 10:
					{
						trace("monitoring array");
						break;
					}
					case 11:
					{
						trace("turret");
						break;
					}
					case 12:
					{
						trace("wall");
						break;
					}
					case 13:
					{
						trace("fortress");
						break;
					}
					case 14:
					{
						trace("concealing tower");
						break;
					}
					case 15:
					{
						trace("light frigate");
						break;
					}
					case 16:
					{
						trace("engineer");
						break;
					}
					case 17:
					{
						trace("destroyer");
						break;
					}
					case 18:
					{
						trace("kelvin");
						break;
					}
					case 19:
					{
						trace("corvette");
						break;
					}
					case 20:
					{
						trace("carrier");
						break;
					}
					case 21:
					{
						trace("infiltrator");
						break;
					}
					case 22:
					{
						trace("drone");
						break;
					}
					case 23:
					{
						trace("sniffer");
						break;
					}
					case 24:
					{
						trace("striker");
						break;
					}
				}*/
				
			}
		}

	}
}