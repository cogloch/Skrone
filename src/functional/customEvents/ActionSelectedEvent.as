package functional.customEvents
{
	import starling.events.Event;
	
	public class ActionSelectedEvent extends Event
	{
		public static var SELECTED_ACTION:String = "selectedAction";
		public static var SELECTED_BUILD:String = "selectedBuild";
		public static var SELECTED_SPAWN:String = "selectSpawn";
		
		public function ActionSelectedEvent(type:String, sAction:String, bubbles:Boolean=true)
		{
			super(type, bubbles);
		}
	}
}