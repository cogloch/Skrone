package functional.customEvents
{
	import starling.events.Event;
	
	public class CombatEvent extends Event
	{
		public static var DIE:String = "die";
		public static var DMG_RECEIVED:String = "dmgReceived";
		
		public function CombatEvent(type:String, bubbles:Boolean=true)
		{
			super(type, bubbles);
		}
	}
}