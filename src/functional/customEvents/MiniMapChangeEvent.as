package functional.customEvents
{
	import flash.geom.Point;
	
	import starling.events.Event;
	
	public class MiniMapChangeEvent extends Event
	{
		
		public static var TO_FOG:String = "toFog";
		public static var TO_ENEMY:String = "toEnemy";
		public static var TO_FRIENDLY:String = "toFriendly";
		public static var TO_CLEAR:String = "toClear";
		
		public var coords:Point = new Point();
		
		
		public function MiniMapChangeEvent(type:String, coords:Point, bubbles:Boolean=true)
		{
			super(type, bubbles);
		}
	}
}