package functional.customEvents
{
	import starling.events.Event;
	
	public class SelectedTileEvent extends Event
	{
		
		public static var SELECTED_TILE:String = "selectedTile";
		public static var SAME_TILE_SELECTED:String = "sameTileSelected";
		public static var NOTHING_TILE_SELECTED:String = "nothingTileSelected";
		//public static var ENEMY_TURN:String = "enemyTurn";
		
		public var tileState:int;
		public var x:int;
		public var y:int;
		public var params:Object = new Object;
		
		
		public function SelectedTileEvent(type:String, tileState:int, bubbles:Boolean=true)
		{
			super(type, bubbles);
		}
	}
}