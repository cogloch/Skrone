package functional.customEvents
{
	import starling.events.Event;
	
	public class EnemyTurnTileEvent extends Event
	{
		
		public static var ENEMY_TURN:String = "enemyTurn";
		//public static var ENEMY_TURN:String = "enemyTurn";
		
		//public var tileState:int;
		
		public function SelectedTileEvent(tileState:int, bubbles:Boolean=true)
		{
			super(ENEMY_TURN, bubbles);
		}
	}
}