package functional
{
	import flash.display.Bitmap;
	import flash.media.Sound;
	import flash.utils.Dictionary;
	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Assets
	{
		[Embed(source="/../assets/spriteSheets/interface/1.xml", mimeType="application/octet-stream")]
		public static const AXmlMenu:Class;
		
		[Embed(source="/../assets/spriteSheets/interface/1.png")]
		public static const ATexMenu:Class;
		
		[Embed(source="/../assets/spriteSheets/game/hud.xml", mimeType="application/octet-stream")]
		public static const AXmlHud:Class;
		
		[Embed(source="/../assets/spriteSheets/game/hud.png")]
		public static const ATexHud:Class;

		[Embed(source="/../assets/interface/hud/camTarget.png")]
		public static const CamTarget:Class;
		
		[Embed(source="/../assets/spriteSheets/game/bg.png")]
		public static const ATexBg:Class;
		
		[Embed(source="/../assets/spriteSheets/game/bg.xml", mimeType="application/octet-stream")]
		public static const AXmlBg:Class;
		
		[Embed(source="/../assets/game/bg/1.jpg")]
		public static const bg1:Class;
		
		//[Embed(source="/../assets/interface/hud/camTarget.png")]
		//public static const camTarget:Class;
		
		/*[Embed(source="/../assets/spriteSheets/game/shipsNstructs.png", mimeType="application/octet-stream")]
		public static const AXmlGame:Class;
		
		[Embed(source="/../assets/spriteSheets/game/shipsNstructs.png")]
		public static const ATexGame:Class;*/
		
		//[Embed(source="../assets/sound/music/Long Note Three.mp3")]
		//public static const GameMusic:Class;
		
		//[Embed(source="/../assets/interface/hud/")]
		
		[Embed(source="/../assets/spriteSheets/game/ss.png")]
		public static const ATexSS:Class;
		
		[Embed(source="/../assets/spriteSheets/game/ss.xml", mimeType="application/octet-stream")]
		public static const AXmlSS:Class;
		
		private static var sSSAtlas:TextureAtlas;
		private static var sSNSAtlas:TextureAtlas;
		private static var sMenuAtlas:TextureAtlas;
		private static var sHudAtlas:TextureAtlas;
		private static var sBgAtlas:TextureAtlas;
		
		// Texture cache
		
		private static var sTextures:Dictionary=new Dictionary();
		private static var sSounds:Dictionary = new Dictionary();
		
		public static function getSound(name:String):Sound
		{
			var sound:Sound=sSounds[name] as Sound;
			if (sound)
				return sound;
			else
				throw new ArgumentError("Sound not found: " + name);
		}
		
		/**
		 * Returns the Texture atlas instance.
		 * @return the TextureAtlas instance (there is only oneinstance per app)
		 */
		/*public static function getSNSAtlas():TextureAtlas
		{
			if (sSNSAtlas == null)
			{
				var texture:Texture=getTexture("ATexGame");
				var xml:XML=XML(new AXmlGame());
				sSNSAtlas=new TextureAtlas(texture, xml);
				
			}
			
			return sSNSAtlas;
		}*/
		
		public static function getSSAtlas():TextureAtlas
		{
			if(sSSAtlas == null)
			{
				var texture:Texture = getTexture("ATexSS");
				var xml:XML = XML(new AXmlSS());
				sSSAtlas = new TextureAtlas(texture, xml);
			}
			
			return sSSAtlas;
		}
		
		public static function getMenuAtlas():TextureAtlas
		{
			if(sMenuAtlas == null)
			{
				var texture:Texture=getTexture("ATexMenu");
				var xml:XML=XML(new AXmlMenu());
				sMenuAtlas=new TextureAtlas(texture, xml);
			}
			
			return sMenuAtlas;
		}
		
		public static function getHudAtlas():TextureAtlas
		{
			if(sHudAtlas == null)
			{
				var texture:Texture = getTexture("ATexHud");
				var xml:XML = XML(new AXmlHud());
				sHudAtlas = new TextureAtlas(texture, xml);
			}
			
			return sHudAtlas;
		}
		
		public static function getBgAtlas():TextureAtlas
		{
			if(sBgAtlas == null)
			{
				var texture:Texture = getTexture("ATexBg");
				var xml:XML = XML(new AXmlBg());
				sBgAtlas = new TextureAtlas(texture, xml);
			}
			return sBgAtlas;
		}
		
		/**
		 * Returns a texture from a texture atlas based on a string key.
		 * 
		 * @param name A key that matches a static constant of Bitmap type.
		 * @return a starling texture.
		 */
		public static function getTexture(name:String):Texture
		{
			if (sTextures[name] == undefined)
			{
				var bitmap:Bitmap=new Assets[name]();
				sTextures[name]=Texture.fromBitmap(bitmap);
			}
			
			return sTextures[name];
		}
	}
}