package functional
{
	import functional.Assets;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class FloatingFeedback extends Sprite
	{
		private var floater:Sprite = new Sprite();
		private var type:String;
		private var amount:int;
		private var fx:int, fy:int;
		private var floatingIcon:Image = new Image(Assets.getHudAtlas().getTexture("repairFloater"));
		private var amountText:TextField = new TextField(100, 30, "jesus", "Helvetica", 20, 0xed1c24, true);
		
		public function FloatingFeedback(pamount:int, ptype:String, px:int, py:int)
		{
			super();
			
			//this.x = 600;
			//this.y = 400;
			floatingIcon.texture = Assets.getHudAtlas().getTexture(ptype+"Floater");
			//floatingIcon.x = 130;
			amountText.x = 725;
			if(ptype == "repair" || ptype == "heal")
			{
				amountText.text = "+" + pamount.toString();
				amountText.y = 242;
				amountText.color = 0x8dc63f;
			}
			else
			{
				if(pamount>0)
					amountText.text = "-" + pamount.toString();
				else if(pamount<0)
				{
					pamount = 0-pamount;
					amountText.text = "+" + pamount.toString();
				}
				else if(pamount==0)
					amountText.text = pamount.toString();
				amountText.color = 0xed1c24;
				if(ptype == "shieldDmg")
				{
					amountText.y = 242;
				}
				else if(ptype == "hpDmg")
					amountText.y = 313;
				else if(ptype == "moraleDmg")
					amountText.y = 388;
			}
			addChild(floatingIcon);
			addChild(amountText);	
			//if(Data.floatAnimDone)
			addEventListener(Event.ADDED, onAdded);
		}
		
		private function onAdded(e:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			Data.floatAnimDone = false;
			//this.alpha = 1;
			this.x += 100;
			var fn:Tween = new Tween(this, 0.3, Transitions.LINEAR);
			//fn.fadeTo(0);
			fn.moveTo(0, fy);
			fn.onComplete = onCompleteIn;
			//object pool or something
			Starling.juggler.add(fn);
		}
		
		private function onCompleteIn():void
		{
			Data.floatAnimDone = true;
			this.alpha = 1;
			var fa:Tween = new Tween(this, 1.5, Transitions.EASE_IN_OUT);
			fa.fadeTo(0);
			fa.delay = 1;
			fa.onComplete = onCompleteOut;
			Starling.juggler.add(fa);
		}
		
		private function onCompleteOut():void
		{
			dispose();
		}
	}
}