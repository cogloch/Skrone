package functional.nativeFlash
{
	import flash.display.Sprite;
	
	
	import nativeScreens.*;
	
	import ui.Screen;

	public class NativeMenu extends Sprite
	{	
		private var arenaS:Arena = new Arena();
		private var challengesS:Challenges = new Challenges();
		private var createGameS:CreateGame = new CreateGame();
		private var galaxyMapS:GalaxyMap = new GalaxyMap();
		private var gamesListS:GamesList = new GamesList();
		private var hangarS:Hangar = new Hangar();
		private var helpS:Help = new Help();
		private var leaderboardS:Leaderboard = new Leaderboard();
		private var lobbyS:Lobby = new Lobby();
		private var mainMenuS:MainMenu = new MainMenu();
		private var playS:Play = new Play();
		private var registration1S:Registration1 = new Registration1();
		private var registration2S:Registration2 = new Registration2();
		private var registration3S:Registration3 = new Registration3();
		private var storeS:Store = new Store();
		private var titleS:Title = new Title();
		
		public function NativeMenu()
		{
			super();
			
			arenaS.addEventListener("showPlay", onShowPlay);
			arenaS.addEventListener("showLobby", onShowLobby);
			arenaS.addEventListener("showGamesList", onShowGamesList);
			
			/*challengesS.addEventListener();
			
			createGameS.addEventListener();
			
			galaxyMapS.addEventListener();
			
			gamesListS.addEventListener();
			
			hangarS.addEventListener();
			
			helpS.addEventListener();
			
			leaderboardS.addEventListener();
			
			lobbyS.addEventListener();
			
			mainMenuS.addEventListener();
			
			playS.addEventListener();
			
			registration1S.addEventListener();
			
			registration2S.addEventListener();
			
			registration3S.addEventListener();
			
			storeS.addEventListener();
			
			titleS.addEventListener();*/
			
			addChild(titleS);
		}
		
		private function onShowPlay():void
		{
			
		}
		
		private function onShowLobby():void
		{
			
		}
		
		private function onShowGamesList():void
		{
			
		}
	}
}