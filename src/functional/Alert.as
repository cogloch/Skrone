package functional
{
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Alert extends Sprite
	{
		private var alertBg:Image = new Image(Assets.getHudAtlas().getTexture("alertBg"));
		private var alertMsg:Image = new Image(Assets.getHudAtlas().getTexture("noRepair"));
		
		//private var insFunds:Image = new Image(Assets.getHudAtlas().getTexture("insufficientFunds"));
		
		public function Alert(texture:Texture)
		{
			super();
			alertMsg.texture = texture;
			touchable = false;
			addChild(alertBg);
			addChild(alertMsg);
			//this.alpha = 0;
			if(Data.alertAnimDone)
				addEventListener(Event.ADDED, onAdded);
			else
			{
				//queue or something
			}
		}
		
		private function onAdded(e:Event):void
		{
			//addChild(alertContainer);
			Data.alertAnimDone = false;
			this.alpha = 0;
			var t:Tween = new Tween(this, 0.5, Transitions.EASE_IN);
			t.fadeTo(1);
			Starling.juggler.add(t);
			t.onComplete = completedIn;
		}
		
		private function completedIn():void
		{
			var t:Tween = new Tween(this, 2, Transitions.EASE_IN);
			t.fadeTo(0);
			Starling.juggler.add(t);
			t.onComplete = completedOut;
		}
		
		private function completedOut():void
		{
			//trace("animation ended");
			this.removeFromParent();
			this.alpha = 1;
			Data.alertAnimDone = true;
		}
	}
}