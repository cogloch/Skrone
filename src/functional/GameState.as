package functional
{
	import buildings.*;
	import buildings.defense.Fortress;
	import buildings.defense.IonCannon;
	import buildings.defense.TurretCannon;
	import buildings.defense.Wall;
	import buildings.production.Base;
	import buildings.production.Factory;
	import buildings.production.Mine;
	import buildings.production.Reactor;
	import buildings.special.Dome;
	import buildings.special.MonitoringArray;
	
	import com.joeonmars.camerafocus.StarlingCameraFocus;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import flashx.textLayout.formats.BackgroundColor;
	
	import functional.Assets;
	import functional.Data;
	import functional.Sounds;
	import functional.customEvents.ActionSelectedEvent;
	import functional.customEvents.CombatEvent;
	import functional.customEvents.SelectedTileEvent;
	
	import npc.Asteroid;
	import npc.OreAsteroid;
	
	import playerio.*;
	
	import screens.GameOverScreen;
	
	import starling.animation.Juggler;
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.RectangleUtil;
	import starling.utils.deg2rad;
	
	import units.*;
	import units.combat.Carrier;
	import units.combat.Corvette;
	import units.combat.Destroyer;
	import units.combat.Drone;
	import units.combat.Frigate;
	import units.combat.Infiltrator;
	import units.combat.Kelvin;
	import units.combat.Sniffer;
	import units.combat.Striker;
	import units.utility.Engineer;
	
	import utils.*;
	
	public class GameState extends Sprite
	{
		private var connection:Connection;
		
		private var _cam:StarlingCameraFocus;
		private var _hud:Hud = new Hud();
		private var _aWheel:ActionsWheel = new ActionsWheel();
		private var gameArea:Rectangle;
		private var isHardwareRendering:Boolean;
		
		private var keysPressed:Object = { };
		
		public var range:Sprite = new Sprite();
		public var stageContainer:Sprite = new Sprite();
		public var background:Sprite = new Sprite();
		public var foreground:Sprite = new Sprite();
		public var ss:Sprite = new Sprite();
		public var npcs:Sprite = new Sprite();
		
		public var miniMap:Sprite = new Sprite();
		
		private var fovH:int;
		private var fovW:int;
		
		public var camTarget:Image;
		
		private var timePrevious:Number;
		private var timeCurrent:Number;
		private var elapsed:Number;
		
		public var overlayQuadBlue:Quad;
		
		public var hudBar:Quad;
		
		private var currentShipMove:int;
		private var minX:int;
		private var minY:int;
		
		private var isRepGamble:Boolean;
		private var bonusCrateEnabled:Boolean;
		
		private var selAvatar:Image = new Image(Assets.getHudAtlas().getTexture("avatarEngineer"));
		
		//private var ore:int;
		//private var energy:int;
		
		//private var time:uint;
		
		private var mapW:int;
		private var mapH:int;
		
		private var miniMapW:int = 133;
		
		private var mIsDraggin:Boolean = false;
		private var touchX:Number;
		private var touchY:Number;
		//private var touch:Touch
		
		public var map:Sprite;
		
		//public var mapTiles:Array = [];
		public var miniMapArray:Array = [];
		
		private var hasTurn:Boolean = false;
		
		private var gameOverContainer:GameOverContainer;
		
		private var cZoom:int = 2;
		private var cMoveFactor:int = 250;
		private var isZooming:Boolean = false;

		public function GameState()
		{
			super();
			
			//ore = 3000;
			//energy = 50;
			
			// Is hardware rendering?
			isHardwareRendering = Starling.context.driverInfo.toLowerCase().indexOf("software") == -1;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);	
		}
		
		//private var _leaveButton:Button;
		
		private function onAddedToStage():void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			mapW = 52;
			mapH = 50;
			if(Data.derp == Data.p1)
				Data.localP = 0; //local player is player 1
			else
				Data.localP = 1; //local p is p 2
			
			Data.pTurn = 0;
			map = new Sprite();
			miniMapArray = new Array();
			//mapTiles = new Array();
			
			var stageContainer:Sprite = new Sprite();
			//addChild(stageContainer);
			
			camTarget = new Image(Assets.getHudAtlas().getTexture("camTarget"));
			camTarget.x = 2560;
			camTarget.y = 4;
			ss.addChild(camTarget);
			
			initData();
			loadGame();
			drawGame();
			initMap();
			drawNPC();
			initMiniMap();
			//drawHud();
			
			var layersInfo:Array = [
				{name:'background', instance:this.background, ratio:0.1},
				{name:'foreground', instance:this.foreground, ratio:0.1}];
				//{name:'npcs', instance:npcs, ratio:0.1}];
			_cam = new StarlingCameraFocus(Starling.current.stage, stageContainer, camTarget, layersInfo, true);
			_cam.update();
			
			ss.touchable = false;
	
			initPlayers();
			
			foreground.addEventListener(SelectedTileEvent.SELECTED_TILE, onSelectedTile);
			foreground.addEventListener(SelectedTileEvent.NOTHING_TILE_SELECTED, onNothingSelected);
			_aWheel.addEventListener(ActionSelectedEvent.SELECTED_ACTION, onSelectedAction);
			_aWheel.addEventListener(ActionSelectedEvent.SELECTED_BUILD, onSelectedBuild);
			_aWheel.addEventListener(ActionSelectedEvent.SELECTED_SPAWN, onSelectedSpawn);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			stage.addEventListener(Event.ENTER_FRAME, onTick);
			range.touchable = false;
			if(Data.resolution == 2)
			{
				Data.resH = 768;
				Data.resW = 1024;
			} else if(Data.resolution == 1)
			{
				Data.resH = 600;
				Data.resW = 800;
			}
			this.stageContainer.scaleX = 0.8;
			this.stageContainer.scaleY = 0.8;
		}	
		
		private function onSelectedSpawn(e:Event, sShip:String):void
		{	
			if(Data.mapTiles[Data.rightmostSelectedAux+1][Data.selectedY].state !=0)
			{
				//cant spawn
				if(Data.alertAnimDone)
					_hud.addChild(_hud.deploymentZoneOccupied);
				Data.mapTiles[Data.rightmostSelectedAux+1][Data.selectedY].tileBtn.upState = Data.enemyTileSkin;
				var flashTileTween:Tween = new Tween(Data.mapTiles[Data.rightmostSelectedAux+1][Data.selectedY], 1, Transitions.EASE_IN_BOUNCE);
				flashTileTween.animate("alpha", 0);
				Starling.juggler.add(flashTileTween);
				flashTileTween.onComplete = flashTile;
			}
			else
			{
				//can spawn
				var canSpawn:Boolean=false;
				switch(sShip)
				{
					case "frigate":
					{
						if(Data.ore>=300 && Data.energy>=20)
						{
							canSpawn = true;
							Data.ore-=300;
							Data.energy-=20;
						}
						break;
					}
					case "engineer":
					{
						if(Data.ore>=400 && Data.energy>=25)
						{
							canSpawn = true;
							Data.ore-=400;
							Data.energy-=25;
						}
						break;
					}
					case "destroyer":
					{
						if(Data.ore>=600 && Data.energy>=30)
						{
							canSpawn = true;
							Data.ore-=600;
							Data.energy-=30;
						}
						break;
					}
					case "kelvin":
					{
						if(Data.ore>=700 && Data.energy>=30)
						{
							canSpawn = true;
							Data.ore-=700;
							Data.energy-=30;
						}
						break;
					}
					case "corvette":
					{
						if(Data.ore>=650 && Data.energy>=25)
						{
							canSpawn = true;
							Data.ore-=650;
							Data.energy-=25;
						}
						break;
					}
					case "carrier":
					{
						if(Data.ore>=800 && Data.energy>=30)
						{
							canSpawn = true;
							Data.ore-=800;
							Data.energy-=30;
						}
						break;
					}
					case "infiltrator":
					{
						if(Data.ore>=1100 && Data.energy>=40)
						{
							canSpawn = true;
							Data.ore-=1100;
							Data.energy-=40;
						}
						break;
					}
					case "scout":
					{
						if(Data.ore>=450 && Data.energy>=25)
						{
							canSpawn = true;
							Data.ore-=450;
							Data.energy-=25;
						}
						break;
					}
					case "drone":
					{
						if(Data.ore>=700 && Data.energy>=30)
						{
							canSpawn = true;
							Data.ore-=700;
							Data.energy-=30;
						}
						break;
					}
					case "striker":
					{
						if(Data.ore>=1600 && Data.energy>=40)
						{
							canSpawn = true;
							Data.ore-=1600;
							Data.energy-=40;
						}
						break;
					}
				}
				if(canSpawn)
				{
					removeSpawnScreen();
					_aWheel.isOn = false;
					_aWheel.isSpawn = false;
					clearMainWheel();
					removeChild(_aWheel);
					spawnFShip(sShip, Data.rightmostSelectedAux+1, Data.selectedY);
					_hud.updateResources();
				}
				else if(Data.alertAnimDone)
					_aWheel.addChild(_aWheel.insufficientF);
			}
		}
		
		private function flashTile():void
		{
			var flashTileTween:Tween = new Tween(Data.mapTiles[Data.rightmostSelectedAux+1][Data.selectedY], 0.3, Transitions.EASE_OUT_BOUNCE);
			flashTileTween.animate("alpha", 1);
			Starling.juggler.add(flashTileTween);
			Data.mapTiles[Data.rightmostSelectedAux+1][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
		}
		
		private function onSelectedBuild(e:Event, sBuild:String):void
		{	
			switch(sBuild)
			{
				case "reactor":
				{
					if(Data.ore>=800 && Data.energy>=0)
					{
						Data.aBuild = "reactor";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
				
				case "oreMine":
				{
					if(Data.ore>=1000 && Data.energy>=30)
					{
						Data.aBuild = "oreMine";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "factory":
				{
					if(Data.ore>=1500 && Data.energy>=50)
					{
						Data.aBuild = "factory";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "monitor":
				{
					if(Data.ore>=1900 && Data.energy>=80)
					{
						Data.aBuild = "monitor";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "base":
				{
					if(Data.ore>=3000 && Data.energy>=300)
					{
						Data.aBuild = "base";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "cannon":
				{
					if(Data.ore>=1600 && Data.energy>=60)
					{
						Data.aBuild = "cannon";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "turret":
				{
					if(Data.ore>=700 && Data.energy>=30)
					{
						Data.aBuild = "turret";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "wall":
				{
					if(Data.ore>=500 && Data.energy>=20)
					{
						Data.aBuild = "wall";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "fortress":
				{
					if(Data.ore>=1100 && Data.energy>=50)
					{
						Data.aBuild = "fortress";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
				case "dome":
				{
					if(Data.ore>=700 && Data.energy>=70)
					{
						Data.aBuild = "dome";
						removeBuildScreen();
						_aWheel.isOn = false;
						removeChild(_aWheel);
					}
					else if(Data.alertAnimDone)
						_aWheel.addChild(_aWheel.insufficientF);
					break;
				}
					
					
			}
			
			/*var eX:int;
			var eY:int;
			var eLos:int;
			var eWidth:int;
			var eHeight:int;
			
			spawnBuilding(sBuild, Data.faction, eX, eY, eLos, eWidth, eHeight);*/
		}
		
		private function centerCamOn(cX:int, cY:int):void
		{
			camTarget.y = -1 * cY*600;
			_cam.update();
			camTarget.x = -1 * cX*800;
			_cam.update();
		}
		
		private function clearMainWheel():void
		{
			_aWheel.removeChild(_aWheel.pointers[0]);
			_aWheel.removeChild(_aWheel.move);
			
			switch(Data.mapTiles[Data.selectedX][Data.selectedY].state)
			{
				case 5:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.spawnEngi);
				}
				case 15:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					break;
				}
				case 16:
				{
					_aWheel.removeChild(_aWheel.pointers[2]);
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.pointers[6]);
					_aWheel.removeChild(_aWheel.build);
					_aWheel.removeChild(_aWheel.repair);
					_aWheel.removeChild(_aWheel.recycle);
					_aWheel.removeChild(_aWheel.infoSec);
					break;
				}
				case 17:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					break;	
				}
				case 18:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					break;	
				}
				case 19:
				{
					_aWheel.removeChild(_aWheel.pointers[2]);
					_aWheel.removeChild(_aWheel.heal);
					break;	
				}
				case 20:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					break;	
				}
				case 21:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					_aWheel.removeChild(_aWheel.pointers[2]);
					//if(Data.infiltrators[Data.mapTiles[Data.mapTiles[Data.selectedX][Data.selectedY]].sIndex].isCloaked)	
					_aWheel.removeChild(_aWheel.cloak);
					_aWheel.removeChild(_aWheel.pointers[6]);
					_aWheel.removeChild(_aWheel.zombify);
					break;	
				}
				case 22:
				{
					_aWheel.removeChild(_aWheel.pointers[2]);
					_aWheel.removeChild(_aWheel.sniff);
					break;	
				}
				case 23:
				{
					_aWheel.removeChild(_aWheel.pointers[2]);
					_aWheel.removeChild(_aWheel.sabotage);
					break;	
				}
				case 24:
				{
					_aWheel.removeChild(_aWheel.pointers[4]);
					_aWheel.removeChild(_aWheel.attack);
					_aWheel.removeChild(_aWheel.pointers[2]);
					_aWheel.removeChild(_aWheel.cloak);
					break;	
				}
			}
		}
		
		private function onSelectedAction(e:Event, sAction:String):void
		{
			//trace("action selected: " + sAction);
			_aWheel.isOn = false;
			
			var cTileIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;

			removeChild(_aWheel);

			clearMainWheel();
			
			switch(sAction)
			{
				case "move":
				{
					Data.action = 1;
					Data.actionRange = Data.ships[cTileIndex].moves;
					genMoveRange();
					_hud.showSecondaryAlert();
					break;
				}
				case "attack":
				{
					Data.action = 2;
					_hud.showSecondaryAlert();
					if(Data.mapTiles[Data.selectedX][Data.selectedY].state>=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state<25)
						Data.actionRange = Data.ships[cTileIndex].attackRange;
					else 
						Data.actionRange = Data.structs[cTileIndex].range;
					genAttackRange();
					break;
				}
				case "build":
				{
					Data.action = 3;
					_hud.showSecondaryAlert();
					Data.actionRange = Data.ships[cTileIndex].buildRange;
					genBuildRange();
					
					_aWheel.isBuild = true;
					trace("show building screen");
					_aWheel.addChild(_aWheel.reactor);
					_aWheel.addChild(_aWheel.oreMine);
					_aWheel.addChild(_aWheel.factory);
					_aWheel.addChild(_aWheel.monitor);
					_aWheel.addChild(_aWheel.fBase);
					_aWheel.addChild(_aWheel.cannon);
					_aWheel.addChild(_aWheel.turret);
					_aWheel.addChild(_aWheel.wall);
					_aWheel.addChild(_aWheel.fortress);
					_aWheel.addChild(_aWheel.dome);
					
					addChild(_aWheel);
					
					break;
				}
				case "sniff":
				{
					Data.actionRange = Data.ships[cTileIndex].sniffRange;
					genSniffRange();
					break;
				}
				case "cloak":
				{
					break;
				}
				case "repair":
				{
					Data.action = 6;
					_hud.showSecondaryAlert();
					Data.actionRange = Data.ships[cTileIndex].repairRange;
					genRepairRange();
					break;
				}
				case "recycle":
				{
					Data.actionRange = Data.ships[cTileIndex].recycleRange;
					genRecycleRange();
					break;
				}
				case "zombify":
				{
					Data.actionRange = Data.ships[cTileIndex].zombifyRange;
					genZombifyRange();
					break;
				}
				case "heal":
				{
					Data.actionRange = Data.ships[cTileIndex].healRange;
					genHealRange();
					break;
				}
				case "sabotage":
				{
					Data.actionRange = Data.ships[cTileIndex].sabotageRange;
					genSabotageRange();
					break;
				}
				case "release":
				{
					break;
				}
			}	
		}
		private function removeBuildScreen():void
		{
			//trace("remove building screen");
			_aWheel.isBuild = false;
			_aWheel.removeChild(_aWheel.reactor);
			_aWheel.removeChild(_aWheel.oreMine);
			_aWheel.removeChild(_aWheel.factory);
			_aWheel.removeChild(_aWheel.monitor);
			_aWheel.removeChild(_aWheel.fBase);
			_aWheel.removeChild(_aWheel.cannon);
			_aWheel.removeChild(_aWheel.turret);
			_aWheel.removeChild(_aWheel.wall);
			_aWheel.removeChild(_aWheel.fortress);
			_aWheel.removeChild(_aWheel.dome);
		}
		
		private function removeSpawnScreen():void
		{
			_aWheel.isSpawn = false;
			//trace("show building screen");
			_aWheel.removeChild(_aWheel.frigate);
			_aWheel.removeChild(_aWheel.engineer);
			_aWheel.removeChild(_aWheel.destroyer);
			_aWheel.removeChild(_aWheel.kelvin);
			_aWheel.removeChild(_aWheel.corvette);
			_aWheel.removeChild(_aWheel.carrier);
			_aWheel.removeChild(_aWheel.infiltrator);
			_aWheel.removeChild(_aWheel.scout);
			_aWheel.removeChild(_aWheel.drone);
			_aWheel.removeChild(_aWheel.striker);
		}
		
		private function onNothingSelected(e:Event, state:int):void
		{
			_hud.removeChild(_hud.downBarContainer);
			
			_hud.removeChild(_hud.siHpQuad);
			_hud.removeChild(_hud.siShieldQuad);
			_hud.removeChild(_hud.siMoraleQuad);
			if(Data.resolution == 0)
				selAvatar.y = 125;
			_hud.removeChild(selAvatar);
		}
		
		private function onSelectedTile(e:Event, state:int):void
		{
			if(Data.hasTurn == false)
			{
				//trace("Enemy's turn");
				//_hud.addChild(enemyTurnMessage); 
				//"It's not your turn"
				if(Data.alertAnimDone)
					_hud.addChild(_hud.notYourTurn);
			}
			else
			{
				//trace("selected with state " + state);
				//trace("Data.cancel action = " + Data.cancelAction);
				if(Data.action == 0) //no action selected
				{			
					/*if(_hud.isD)
						_hud.removeChild(_hud.arrowD);
					if(_hud.isU)
						_hud.removeChild(_hud.arrowU);
					if(_hud.isL)
						_hud.removeChild(_hud.arrowL);
					if(_hud.isR)
						_hud.removeChild(_hud.arrowR);*/
					//Data.selIndex = Data.mapTiles[Data.selectedX][Data.selectedY]
					if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=5 && Data.mapTiles[Data.selectedX][Data.selectedY].state <15)
					{
						trace("current tile state: " + Data.mapTiles[Data.selectedX][Data.selectedY].state + " with name " + Data.structs[Data.selIndex].type);
						
						_hud.siNameText.text = Data.structs[Data.selIndex].type;
						
						if(Data.structs[Data.selIndex].faction == Data.faction)
							_hud.avatarBg.texture = Assets.getHudAtlas().getTexture("fAvatarFrame");
						else
							_hud.avatarBg.texture = Assets.getHudAtlas().getTexture("eAvatarFrame");
						
						selAvatar.texture = Assets.getHudAtlas().getTexture("avatar"+_hud.siNameText.text);
						
						if(Data.resolution == 2)
							selAvatar.y = 125;
						
						if((Data.structs[Data.selIndex].faction == Data.faction) && (Data.mapTiles[Data.selectedX][Data.selectedY].state == 9) || (Data.mapTiles[Data.selectedX][Data.selectedY].state == 11) ||
							(Data.mapTiles[Data.selectedX][Data.selectedY].state == 13))
						{
							if(!Data.structs[Data.selIndex].numAttacks)
							{
								if(Data.alertAnimDone)
									_hud.addChild(_hud.noMovesRemaining);
								Data.tileSelected = false;
								Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
							}
							else
							{
								trace("make attack range for defensive");
								Data.action = 2;
								Data.actionRange = Data.structs[Data.mapTiles[Data.selectedX][Data.selectedY].sIndex].range;
								genAttackRange();
								
								_hud.clearDownBar();
								_hud.showStructHud(Data.structs[Data.selIndex].hp, Data.structs[Data.selIndex].maxHp);
								_hud.addChild(_hud.downBarContainer);
								_hud.addChild(selAvatar);
							}
						}
						else
						{
							_hud.clearDownBar();
							_hud.showStructHud(Data.structs[Data.selIndex].hp, Data.structs[Data.selIndex].maxHp);
							_hud.addChild(_hud.downBarContainer);
							_hud.addChild(selAvatar);
						}
					}
					else if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <25)
					{
						if(Data.ships[Data.selIndex].cMoves)
						{
							trace("has moves");
							if(Data.ships[Data.selIndex].faction == Data.faction)
								if(Data.alertAnimDone)
									_hud.addChild(_hud.selActionWarning);
							Data.action = 0;
							_hud.siNameText.text = Data.ships[Data.selIndex].type;
							//_hud.siShieldText.text = Data.ships[Data.selIndex].shield.toString();
							//_hud.siHpText.text = Data.ships[Data.selIndex].hp.toString();
							
							if(Data.ships[Data.selIndex].faction == Data.faction)
								_hud.avatarBg.texture = Assets.getHudAtlas().getTexture("fAvatarFrame");
							else
								_hud.avatarBg.texture = Assets.getHudAtlas().getTexture("eAvatarFrame");
							
							selAvatar.texture = Assets.getHudAtlas().getTexture("avatar"+_hud.siNameText.text);
							if(Data.resolution == 2)
								selAvatar.y = 125;
							
							_hud.clearDownBar();
							_hud.showShipHud();
							_hud.addChild(_hud.downBarContainer);
							_hud.addChild(selAvatar);
						}
						else
						{
							trace("moves: " + Data.ships[Data.selIndex].cMoves);
							trace("Selected ship with index = " + Data.selIndex);
							if(Data.ships[Data.selIndex].faction == Data.faction)
							{
								if(Data.alertAnimDone)
									_hud.addChild(_hud.noMovesRemaining);
								Data.tileSelected = false;
								Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
							}
						}
					}
				}
				else
				{
					//action selected
					if(Data.cancelAction)
					{
						trace("attempt cancel action");
						if(Data.action == 1)
							clearMoveRange();
						else
							clearRange();
						Data.cancelAction = false;
						Data.tileSelected = false;
						Data.action = 0;
						Data.actionRange = 0;
						Data.actionSelX = -1;
						Data.actionSelY = -1;
						Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
						_hud.clearDownBar();
						if(Data.resolution == 2)
							selAvatar.y = 125;
						_hud.removeChild(selAvatar);
						//_hud.downBarOut();
					}
					else
						processAction();
				}
			}
		}

		private function nextShip(type:String = "ship"):void
		{
			_hud.hideSecondaryAlert();
			foreground.touchable = true;
			trace("warp to next ship in queue");
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.friendlyTileSkin;
			
			if(type == "ship")
			{
				Data.ships[Data.selIndex].cMoves = 0;
				Data.mapTiles[Data.ships[Data.selIndex].tX][Data.ships[Data.selIndex].tY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIcon);
				Data.mapTiles[Data.ships[Data.selIndex].tX][Data.ships[Data.selIndex].tY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIconBg);
				Data.mapTiles[Data.ships[Data.selIndex].tX][Data.ships[Data.selIndex].tY].tileBtn.addChild(Data.ships[Data.selIndex].noMovesIcon);
			}
			else 
			{
				clearRange();
				Data.structs[Data.selIndex].numAttacks = 0;
				Data.mapTiles[Data.structs[Data.selIndex].tX][Data.structs[Data.selIndex].tY].tileBtn.removeChild(Data.structs[Data.selIndex].movesIcon);
				Data.mapTiles[Data.structs[Data.selIndex].tX][Data.structs[Data.selIndex].tY].tileBtn.removeChild(Data.structs[Data.selIndex].movesIconBg);
				Data.mapTiles[Data.structs[Data.selIndex].tX][Data.structs[Data.selIndex].tY].tileBtn.addChild(Data.structs[Data.selIndex].noMovesIcon);
			}
			Data.tileSelected = false;
			//Data.mapTiles[Data.ships[Data.selIndex].tX][Data.ships[Data.selIndex].tY].tileBtn.enabled = false;
			if(Data.resolution == 2)
				selAvatar.y = 125;
			_hud.removeChild(selAvatar);
			_hud.downBarOut();
			Data.action = 0;
			//Data.selIndex = -1;
			Data.tileSelected = false;
		}
		
		private function processAction():void
		{
			/*if(Data.mapTiles[Data.actionSelX][Data.actionSelY].inRange != true)
			{
				//_hud.addChild(_hud.outOfRange);
				trace(Data.actionSelX + " and " + Data.actionSelY + " nope");
				trace(Data.mapTiles[Data.actionSelX][Data.actionSelY].state);
				trace(Data.mapTiles[Data.actionSelX][Data.actionSelY].inRange);
				if(Data.action == 1)
				{
					clearMoveRange();
					Data.action=0;
				}
				else
				{
					Data.action = 0;
					clearRange();
				}
			}
			else
			{}*/
			
			/**
			 *  WHEN SELECTED TILE IS THE ACTION TARGET TILE
			 */
			switch(Data.action)
			{
				case 1:
				{
					//move
					trace("ship " + Data.selIndex + " wants to move");
					attemptMove();
					break;
				}
				case 2:
				{
					//attack
					attemptAttack();
					break;
				}
					
				case 3:
				{
					//build
					spawnFBuilding(Data.aBuild, Data.actionSelX, Data.actionSelY);
					break;
				}
					
				case 4:
				{
					//sniff
					attemptSniff();
					break;
				}
					
				case 5:
				{
					//cloak
					attemptCloak();
					break;
				}
					
				case 6:
				{
					//repair
					attemptRepair();
					break;
				}
					
				case 7:
				{
					//recycle
					attemptRecycle();
					break;
				}
					
				case 8:
				{
					//zombify
					attemptZombify();
					break;
				}
					
				case 9:
				{
					//heal
					attemptHeal();	
					break;
				}
					
				case 10:
				{
					//sabotage
					attemptSabotage();
					break;
				}
					
				case 11:
				{
					//release
					attemptRelease();
					break;
				}
					
			}	
			//Data.action = 0;
		}
		
		private var rangeDone:Boolean = false;
		private var endedMove:Boolean = true;
		
		private var numMoved:int=0;
		private var directions:Array = new Array();
		private var movedOnX:Array = new Array();
		private var movedOnY:Array = new Array();
		private var moveTileIndex:int = -1;
		private var movedX:int;
		private var movedY:int;
		
		private function attemptMove():void
		{
			trace("attempting move by ship with index = " + Data.selIndex);
			if(!Data.ships[Data.selIndex].cMoves)
			{
				clearMoveRange();
				nextShip();
			}
			else
			{
				//stage.touchable=false;
				//trace("pulli");
				//trace(Data.actionSelX + " pula " + Data.actionSelY);
				//genMoveRange();
				generatePathToObjective();
				//genPathToObj
				//fixPathToObject();
				//genActualPath();	
				//moveShip();
			}
		}
		
		private function genActualPath():void
		{
			var minRange:int = 50;
			var cX:int = Data.selectedX;
			var cY:int = Data.selectedY;
			var mx:int, my:int;
			var dir:int = 0;
			//trace("moveRange "+Data.mapTiles[cX][cY].moveRange);
			for(var i:int = 0; i<Data.mapTiles[Data.selectedX][Data.selectedY].moveRange; i++)
			{
				//trace("SUGIPULA");
				minRange=50;
				if(Data.mapTiles[cX+1][cY].moveRange<minRange && Data.mapTiles[cX+1][cY].moveRange!=-1)
				{
					//right
					minRange = Data.mapTiles[cX+1][cY].moveRange;
					mx= cX+1;
					my= cY;
					dir = 2;
				}
				if(Data.mapTiles[cX][cY+1].moveRange<minRange && Data.mapTiles[cX][cY+1].moveRange!=-1)
				{
					//down
					minRange = Data.mapTiles[cX][cY+1].moveRange;
					mx=cX;
					my=cY+1;
					dir = 3;
				}
				if(Data.mapTiles[cX-1][cY].moveRange<minRange && Data.mapTiles[cX-1][cY].moveRange!=-1)
				{
					//left
					minRange = Data.mapTiles[cX-1][cY].moveRange;
					mx=cX-1;
					my=cY;
					dir = 4;
				}
				if(Data.mapTiles[cX][cY-1].moveRange<minRange && Data.mapTiles[cX][cY-1].moveRange!=-1)
				{
					//up
					minRange = Data.mapTiles[cX][cY-1].moveRange;
					mx=cX;
					my=cY-1;
					dir = 1;
				}		
				cX=mx;
				cY=my;
				
				//trace("pushing a direction: " + dir + " with min range " + minRange);
				movedOnX.push(mx);
				movedOnY.push(my);
				directions.push(dir);
			}
			//trace("numdirs: " + directions.length);
			moveShip();
		}
		
		private var secvIn:int, secvSf:int, secvM:int, shipMoved:Boolean = true, cDir:int, rtnDne:Boolean = true, moveIndex:int;
		private var movesQueue:Array = new Array();
		
		private function moveShip():void
		{
			//movesQueue = new Array();
			stage.touchable = false;
			trace("dirs: " + directions.length);
			for(var k:int = 0; k<directions.length;k++)
				trace("dir: " + directions[k]);
			var pDir:int=0;
			var i2:int, last:int;
			moveIndex = 0;
			var angle:int, currentUpAngle:int = 0;
			//var tempX:int = 1, tempY:int=1;
			var pt:Point = new Point(Data.ships[Data.selIndex].x, Data.ships[Data.selIndex].y);
			for(var i:int=0; i<directions.length; i++)
			{
				if(Data.ships[Data.selIndex].cRotation == directions[i])
				{
					trace("move forward");
					var tm:Tween = new Tween(Data.ships[Data.selIndex], 1, Transitions.EASE_IN_OUT);
					tm.onComplete = moveDone;
					
					cDir = directions[i];
					//mShip();
					if(cDir==1)
					{
						pt.y-=100;
						tm.moveTo(pt.x, pt.y);
					}
					else if(cDir==2)
					{
						pt.x += 100;
						tm.moveTo(pt.x, pt.y);
					}
					else if(cDir==3)
					{
						pt.y += 100;
						tm.moveTo(pt.x, pt.y);
					}
					else if(cDir==4)
					{
						pt.x -= 100;
						tm.moveTo(pt.x, pt.y);
					}
					movesQueue.push(tm);
				}
				else
				{
					trace("rotate");
					cDir = directions[i];
					var tr:Tween = new Tween(Data.ships[Data.selIndex], 1, Transitions.EASE_IN_OUT);
					if(cDir==1)
						angle = 0;
					else if(cDir==2)
						angle = 90;
					else if(cDir==3)
						angle = 180;
					else if(cDir==4)
					{
						if(currentUpAngle == 0)
							
						angle = 270;
					}
					tr.animate("rotation", deg2rad(angle));
					tr.onComplete = rotationDone;
					movesQueue.push(tr);
					i--;
					Data.ships[Data.selIndex].cRotation = cDir;
				}
			}
			//var shipMovesJuggler:Juggler;
			var mDelay:int = 0;
			for(i = 0; i<movesQueue.length-1; i++)
			{
				movesQueue[i].delay = mDelay++;
				movesQueue[i].nextTween = movesQueue[i+1];
			}
			if((Data.mapTiles[Data.actionSelX][Data.actionSelY].state != 0 || Data.mapTiles[Data.actionSelX][Data.actionSelY].state != 2) && movesQueue.length>1)
			{
				movesQueue[movesQueue.length-2].onComplete = clearMovesQueue;
				for(i=0; i<movesQueue.length-1; i++)
					Starling.juggler.add(movesQueue[i]);
			}
			else
			{
				movesQueue[movesQueue.length-1].delay = mDelay;
				movesQueue[movesQueue.length-1].onComplete =  clearMovesQueue;
				for(i=0; i<movesQueue.length; i++)
					Starling.juggler.add(movesQueue[i]);
			}	
			//moveTileIndex = 0;
			//movedX = Data.selectedX;
			//movedY = Data.selectedY;
			//trace("start movement from movedX = " + movedX + " and movedY = " + movedY);
			//Data.mapTiles[movedX][movedY].tileBtn.upState = Data.selectedTileSkin;
		}
		
		private function clearMovesQueue():void
		{
			stage.touchable = true;
			while(movesQueue.length)
				movesQueue.pop();
			moveCompleted();
		}
	
		private function moveDone():void
		{
			trace("move done from movedX = " + this.movedX + " and movedY " + this.movedY);
			updateShipLos(Data.selIndex);
			
			//Data.mapTiles[this.movedX][this.movedY].tileBtn.upState = Data.emptyTileSkin;
			
			//trace("current tile in move queue: " + movedOnX[moveTileIndex] + " " + movedOnY[moveTileIndex]);
			moveTileIndex++;
			var movedX:int = movedOnX[moveTileIndex];
			var movedY:int = movedOnY[moveTileIndex];
			
			//Data.mapTiles[movedX][movedY].tileBtn.upState = Data.selectedTileSkin;
		}
		
		private function rotationDone():void
		{
			trace("rotation done");
		}
		
		private function forwardShip():void
		{
			//Starling.juggler.purge();
			//foreground.touchable = false;
			trace("forward");
			Data.ships[Data.selIndex].cRotation = cDir;
			//Data.ships[Data.selIndex].cMoves-=secvM;
			var tm:Tween = new Tween(Data.ships[Data.selIndex], 2, Transitions.EASE_IN_OUT);
			if(cDir==1)
				tm.moveTo(Data.ships[Data.selIndex].x, Data.ships[Data.selIndex].y-(secvM+1)*100);
			else if(cDir==2)
				tm.moveTo(Data.ships[Data.selIndex].x+(secvM+1)*100, Data.ships[Data.selIndex].y);
			else if(cDir==3)
				tm.moveTo(Data.ships[Data.selIndex].x, Data.ships[Data.selIndex].y+(secvM+1)*100);
			else if(cDir==4)
				tm.moveTo(Data.ships[Data.selIndex].x-(secvM+1)*100, Data.ships[Data.selIndex].y);
			secvM=0;
			Starling.juggler.add(tm);
			tm.onComplete = fComplete;
		}
		
		private function fComplete():void
		{
			shipMoved = true;	
			Starling.juggler.purge();
		}
		
		private function moveCompleted():void
		{
			//moving = false;
			//Starling.juggler.removeTweens(Data.ships[Data.selIndex]);
			//stageContainer.scaleX = 1;
			//stageContainer.scaleY = 1;
			//Data.mapTiles[movedX][movedY].tileBtn.upState = Data.emptyTileSkin;
			stage.touchable = true;
			var tempRange:int = Data.mapTiles[Data.selectedX][Data.selectedY].moveRange;
			clearMoveRange();
			Data.ships[Data.selIndex].cMoves-=tempRange;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.emptyTileSkin;
			
			if(Data.mapTiles[Data.selectedX][Data.selectedY].isAnomaly)
				Data.mapTiles[Data.selectedX][Data.selectedY].state = 2;
			else
			{
				Data.mapTiles[Data.selectedX][Data.selectedY].state = 0;
			}
			
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIconBg);
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIcon);
			Data.mapTiles[Data.selectedX][Data.selectedY].sIndex = -1;
			Data.mapTiles[Data.selectedX][Data.selectedY].team = 0;
			//Data.mapTiles[Data.selectedX][Data.selectedY].sInde
			
			//trace(Data.mapTiles[28][6].sIndex);
			Data.selectedX = Data.actionSelX;
			Data.selectedY = Data.actionSelY;
			
			Data.ships[Data.selIndex].movesIconBg.x = Data.selectedX;
			Data.ships[Data.selIndex].movesIconBg.y = Data.selectedY;
			Data.ships[Data.selIndex].movesIcon.x = Data.selectedX;
			Data.ships[Data.selIndex].movesIcon.y = Data.selectedY;
			
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.addChild(Data.ships[Data.selIndex].movesIconBg);
			Data.ships[Data.selIndex].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[Data.selIndex].cMoves);
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.addChild(Data.ships[Data.selIndex].movesIcon);
			Data.mapTiles[Data.selectedX][Data.selectedY].sIndex = Data.selIndex;
			
			Data.mapTiles[Data.selectedX][Data.selectedY].state = Data.ships[Data.selIndex].sType;
			Data.mapTiles[Data.selectedX][Data.selectedY].toFriendly();
			Data.mapTiles[Data.actionSelX][Data.actionSelY].tileBtn.upState = Data.selectedTileSkin;
			
			Data.ships[Data.selIndex].tX = Data.selectedX;
			Data.ships[Data.selIndex].tY = Data.selectedY;
			
			updateShipLos(Data.selIndex);
			
			if(Data.ships[Data.selIndex].cMoves)
				genMoveRange();	
			else
			{
				nextShip();
			}
			trace("moves remaining: " + Data.ships[Data.selIndex].cMoves);
		}
		
		private function updateShipFloaters():void
		{
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIconBg);
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.removeChild(Data.ships[Data.selIndex].movesIcon);
			
			if(Data.ships[Data.selIndex].cMoves)
			{
				Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.addChild(Data.ships[Data.selIndex].movesIconBg);
				Data.ships[Data.selIndex].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[Data.selIndex].cMoves);
				Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.addChild(Data.ships[Data.selIndex].movesIcon);
			}
			else
				nextShip();
		}
		
		private function updateShipLos(losIndex:int):void
		{
			var sLos:int = Data.ships[losIndex].los;
			var bx:int= Data.ships[losIndex].tX-sLos;
			var bxEnd:int =  Data.ships[losIndex].tX+sLos;
			var by:int = Data.ships[losIndex].tY-sLos;
			var byEnd:int = Data.ships[losIndex].tY+sLos;
			if(bx<3)
				bx=3;
			if(bxEnd>mapW-3)
				bxEnd = mapW-3;
			if(by<3)
				by=3;
			if(byEnd>mapH-3)
				byEnd = mapH-3;
			for(var i:int=bx; i<= bxEnd; i++)
			{
				//trace("doing it");
				for(var j:int=by; j<= byEnd; j++)
				{
					//trace("here");
					if(Data.mapTiles[i][j].isFog)
					{
						Data.mapTiles[i][j].toClear();
						if(Data.mapTiles[i][j].state >=5 && Data.mapTiles[i][j].state <15)
							foreground.addChild(Data.structs[Data.mapTiles[i][j].sIndex]);
						else if(Data.mapTiles[i][j].state >=15 && Data.mapTiles[i][j].state <25)
						{
							trace("adding enemy ship to foreground");
							foreground.addChild(Data.ships[Data.mapTiles[i][j].sIndex]);
						}
					}
				}
			}
			if(Data.tileSelected)
				Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
		}
		
		private function generatePathToObjective():void
		{
			/**
			 * Clear General Ship Path
			 */
			//foreground.touchable = false;
			var oldDirSize:int = directions.length;
			for(var i:int = 0; i<oldDirSize; i++)
			{
				directions.pop();
				movedOnX.pop();
				movedOnY.pop();
			}
			var cIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;
			var slice:int = Data.ships[cIndex].cMoves*2+1;
			
			var bx:int = Data.selectedX - Data.ships[cIndex].cMoves-1;
			var bxEnd:int = Data.selectedX + Data.ships[cIndex].cMoves+1;
			
			var uppLimit:int = Data.selectedY-Data.ships[cIndex].cMoves-1;
			var lowLimit:int = Data.selectedY+Data.ships[cIndex].cMoves+1;
			
			if(bx<3)
				bx=3;
			if(bxEnd>49)
				bxEnd = 49;
			if(uppLimit<3)
				uppLimit = 3;
			if(lowLimit > 47)
				lowLimit = 47;
			for(i = bx-1; i<=bxEnd; i++)
				for(var j:int = uppLimit; j<= lowLimit; j++)
				{	
					if(Data.mapTiles[i][j].moveRange>0)
						Data.mapTiles[i][j].moveRange = 0;
				}
			/**
			 * Lee Pathfinding
			 */
			var lx:int = Data.actionSelX;
			var ly:int = Data.actionSelY;
			var tileQueue:Array = new Array();
			tileQueue.push(Data.mapTiles[lx][ly]);
			Data.mapTiles[lx][ly].moveRange = 0;
			
			//trace("generating lee object to ship");
			var inC:int = 0, sfC:int = 0;
			do
			{
				lx = tileQueue[inC].xa;
				ly = tileQueue[inC].ya;
				//if(tileQueue[inC].xa==Data.actionSelX && tileQueue[inC].ya == Data.actionSelY)
					//trace(Data.mapTiles[Data.actionSelX][Data.actionSelY].moveRange);
				if(Data.mapTiles[lx+1][ly].moveRange == 0 && (lx+1 != Data.actionSelX || ly != Data.actionSelY))
				{
					tileQueue.push(Data.mapTiles[lx+1][ly]);
					sfC++;
					Data.mapTiles[lx+1][ly].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("moveRange: "+Data.mapTiles[lx][ly].moveRange+1);
				}
				if(Data.mapTiles[lx][ly+1].moveRange == 0 && (lx != Data.actionSelX || ly+1 != Data.actionSelY)) 
				{
					tileQueue.push(Data.mapTiles[lx][ly+1]);
					sfC++;
					Data.mapTiles[lx][ly+1].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("moveRange: "+Data.mapTiles[lx][ly].moveRange+1);
					//trace("y");
				}
				if(Data.mapTiles[lx-1][ly].moveRange == 0 && (lx-1 != Data.actionSelX || ly != Data.actionSelY)) 
				{
					tileQueue.push(Data.mapTiles[lx-1][ly]);
					sfC++;
					Data.mapTiles[lx-1][ly].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("moveRange: "+Data.mapTiles[lx][ly].moveRange+1);
					//trace("y");
				}
				if(Data.mapTiles[lx][ly-1].moveRange == 0 && (lx != Data.actionSelX || ly-1 != Data.actionSelY)) 
				{
					tileQueue.push(Data.mapTiles[lx][ly-1]);
					sfC++;
					Data.mapTiles[lx][ly-1].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("moveRange: "+Data.mapTiles[lx][ly].moveRange+1);
					//trace("y");
				}
				inC++;
			}
			while(inC<=sfC);
			//trace("tiles traced: " + inC); 
			genActualPath();
		}
		
		private function attemptAttack():void
		{
			if(Data.mapTiles[Data.selectedX][Data.selectedY].state <15 )
				defensiveCombat();
			else
				shipCombat();
		}
		
		private function shipCombat():void
		{
			//do combat stuff
			var aIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;
			var dIndex:int = Data.mapTiles[Data.actionSelX][Data.actionSelY].sIndex;
			
			var dHp:int, dMorale:int, dShield:int;
			
			if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state>=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state <25)
			{
				trace("against a ship");
				dHp = Data.ships[dIndex].hp;
				dMorale = Data.ships[dIndex].morale;
				dShield = Data.ships[dIndex].shield;
			} else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state>=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state <15)
				dHp = Data.structs[dIndex].hp;
			
			//attacker
			var aSpeed:int = Data.ships[aIndex].speed;
			var aDmg:int = Data.ships[aIndex].dmg;
			var aMorale:int = Data.ships[aIndex].morale;	
			
			var tDmg:int=0; 
			
			var hpDmg:int=0, shieldDmg:int=0, moraleDmg:int=0;
			
			var projectile:int = 0;
			
			switch(Data.ships[aIndex].type)
			{
				case "Frigate":
				{
					/**
					 * Standard cannon fodder
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;
				}
				case "Destroyer":
				{
					/**
					 * Best against structures
					 * Heavy weapons
					 * Slow projectiles, therefore, not effective against ships
					 */
					trace("destroyer combat");
					projectile = 2;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;
				}
				case "Kelvin":
				{
					/**
					 * Not effective against structures
					 * Morale Dmg
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;
				}
				case "Carrier":
				{
					/**
					 * Good against both ships and structures
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;
				}
				case "Infiltrator":
				{
					/**
					 * Shit against structures and combat in general
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;	
				}
				case "Drone":
				{
					/**
					 * Good against ships
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;	
				}
				case "Striker":
				{
					/**
					 * Fucks the shit out of ships
					 */
					projectile = 1;
					if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
					{
						//combat againt structs
						tDmg = aDmg*(aMorale/100)*1;		
					}
					else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
					{
						//combat against ships
						tDmg = aDmg*(aMorale/100)*1;		
					}
					break;	
				}	
			}
			
			trace("tDmg" + tDmg);
			
			if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
			{
				showStructCombatFeedback(tDmg);
				Data.structs[dIndex].hp -= tDmg;
				if(Data.structs[dIndex].hp < 1)
					structDeath(dIndex);
			} else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
			{	
				if(dShield)
				{
					if(dShield>=tDmg)
					{
						shieldDmg = tDmg;
						moraleDmg -= 5;
					}
					else 
					{
						tDmg-=dShield;
						shieldDmg = dShield;
						hpDmg = dHp;
						moraleDmg = 5;
					}
				}
				else
				{
					if(dHp-tDmg>Data.ships[dIndex].maxHp/3)
					{
						hpDmg = tDmg;
						moraleDmg = 5;
					}
					else
					{
						hpDmg = tDmg;
						moraleDmg = 15;
					}
				}
				Data.ships[dIndex].hp -= hpDmg;
				Data.ships[dIndex].morale -= moraleDmg;
				Data.ships[dIndex].shield -= shieldDmg;
				
				if(Data.ships[dIndex].hp < 1)
				{ 
					Data.ships[aIndex].morale +=20;
					if(Data.ships[aIndex].morale>100)
						Data.ships[aIndex].morale = 100;
					moraleDmg = dMorale;
					shipDeath(dIndex);
				}
				
				showShipCombatFeedback(hpDmg, shieldDmg, moraleDmg);
			}
			
			clearRange();
			Data.action = 0;
			nextShip();
			
			trace("ship combat");
			
			var dir:int = 1;
		}
		
		private function shipDeath(shipIndex:int):void
		{
			trace("fuck this ide");
			var pmX:int = Data.actionSelX;
			var pmY:int = Data.actionSelY;
			Data.ships[shipIndex].dead = true;
			foreground.removeChild(Data.ships[shipIndex], true);
			Data.mapTiles[pmX][pmY].state = 0;
			Data.mapTiles[pmX][pmY].team = 0;
			Data.mapTiles[pmX][pmY].tileBtn.upState = Data.emptyTileSkin;
			Data.mapTiles[pmX][pmY].sIndex = -1;
		}
		
		private function structDeath(structIndex:int):void
		{
			trace("fuck this ide");
			
			var pmX:int = Data.actionSelX;
			var pmY:int = Data.actionSelY;
			
			Data.structs[structIndex].dead = true;
			foreground.removeChild(Data.structs[structIndex], true);
			Data.mapTiles[pmX][pmY].state = 0;
			Data.mapTiles[pmX][pmY].team = 0;
			Data.mapTiles[pmX][pmY].tileBtn.upState = Data.emptyTileSkin;
			Data.mapTiles[pmX][pmY].sIndex = -1;
		}
		
		private function showShipCombatFeedback(hpDmg:int, shieldDmg:int, moraleDmg:int):void
		{
			//trace("show floating bubble at x= " + Data.actionSelX*100 + " " + Data.actionSelY*100);
			var hpFeedback:FloatingFeedback = new FloatingFeedback(hpDmg, "hpDmg", Data.actionSelX*100, Data.actionSelY*100);
			_hud.addChild(hpFeedback);
			var shieldFeedback:FloatingFeedback = new FloatingFeedback(shieldDmg, "shieldDmg", Data.actionSelX*100, Data.actionSelY*100);
			_hud.addChild(shieldFeedback);
			var moraleFeedback:FloatingFeedback = new FloatingFeedback(moraleDmg, "moraleDmg", Data.actionSelX*100, Data.actionSelY*100);
			_hud.addChild(moraleFeedback);
		}
		
		private function showStructCombatFeedback(hpDmg:int):void
		{
			var hpFeedback:FloatingFeedback = new FloatingFeedback(hpDmg, "hpDmg", Data.actionSelX*100, Data.actionSelY*100);
			_hud.addChild(hpFeedback);
		}
		
		private function defensiveCombat():void
		{
			//do combat stuff
			var aIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;
			var dIndex:int = Data.mapTiles[Data.actionSelX][Data.actionSelY].sIndex;
			
			var dHp:int, dMorale:int, dShield:int;
			
			if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state>=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state <25)
			{
				trace("against a ship");
				dHp = Data.ships[dIndex].hp;
				dMorale = Data.ships[dIndex].morale;
				dShield = Data.ships[dIndex].shield;
			} else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state>=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state <15)
				dHp = Data.structs[dIndex].hp;
			
			//attacker
			var aDmg:int = Data.structs[aIndex].dmg;
			
			var tDmg:int=0; 
			
			var hpDmg:int=0, shieldDmg:int=0, moraleDmg:int=0;
			
			var projectile:int = 0;
			
			switch(Data.structs[aIndex].type)
			{			
				case "Turret":
				{
					trace("ittsa turret with attack dmg = " + Data.structs[aIndex].dmg);
					tDmg = aDmg;
					break;	
				}	
				case "Cannon":
				{
					tDmg = aDmg;
					break;
				}
				case "Fortress":
				{
					tDmg = aDmg;
					break;
				}
			}
			 
			trace("tDmg" + tDmg);
			
			if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=5 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<15)
			{
				showStructCombatFeedback(tDmg);
				Data.structs[dIndex].hp -= tDmg;
				if(Data.structs[dIndex].hp < 1)
					structDeath(dIndex);
			} else if(Data.mapTiles[Data.actionSelX][Data.actionSelY].state >=15 && Data.mapTiles[Data.actionSelX][Data.actionSelY].state<25)
			{	
				if(dShield)
				{
					if(dShield>=tDmg)
					{
						shieldDmg = tDmg;
						moraleDmg -= 5;
					}
					else 
					{
						tDmg-=dShield;
						shieldDmg = dShield;
						hpDmg = dHp;
						moraleDmg = 5;
					}
				}
				else
				{
					if(dHp-tDmg>Data.ships[dIndex].maxHp/3)
					{
						hpDmg = tDmg;
						moraleDmg = 5;
					}
					else
					{
						hpDmg = tDmg;
						moraleDmg = 15;
					}
				}
				Data.ships[dIndex].hp -= hpDmg;
				Data.ships[dIndex].morale -= moraleDmg;
				Data.ships[dIndex].shield -= shieldDmg;
				
				if(Data.ships[dIndex].hp < 1)
				{ 
					Data.ships[aIndex].morale +=20;
					if(Data.ships[aIndex].morale>100)
						Data.ships[aIndex].morale = 100;
					moraleDmg = dMorale;
					shipDeath(dIndex);
				}
				
				showShipCombatFeedback(hpDmg, shieldDmg, moraleDmg);
			}
			
			Data.structs[aIndex].numAttacks--;
			
			if(Data.structs[aIndex].numAttacks < 1)
			{
				Data.action = 0;
				nextShip("struct");
			}
			else
			{
				Data.structs[Data.selIndex].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.structs[aIndex].numAttacks);
			}
			
			trace("defensive combat");
		}
		
		private function attemptSniff():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
		}
		
		private function attemptCloak():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
		}
		
		private function attemptRepair():void
		{
			var structTargetIndex:int = Data.mapTiles[Data.actionSelX][Data.actionSelY].sIndex;
			var hpDif:int = Data.structs[structTargetIndex].maxHp-Data.structs[structTargetIndex].hp;
			
			if(hpDif<Data.ships[Data.selIndex].repairPower)
				Data.structs[structTargetIndex].hp = Data.structs[structTargetIndex].maxHp;
			else
				Data.structs[structTargetIndex].hp+=Data.ships[Data.selIndex].repairPower;
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
			else
			{
				Data.ships[Data.selIndex].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[Data.selIndex].cMoves);
			}
			
			showRepairFeedback();
		}
		
		private function showRepairFeedback():void
		{
			trace("show floating bubble at x= " + Data.actionSelX*100 + " " + Data.actionSelY*100);
			var repairFeedback:FloatingFeedback = new FloatingFeedback(Data.ships[Data.selIndex].repairPower, "repair", Data.actionSelX*100, Data.actionSelY*100);
			_hud.addChild(repairFeedback);
		}
		
		private function attemptRecycle():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
		}
		
		private function attemptZombify():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}	
		}
		
		private function attemptHeal():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
		}
		
		private function attemptSabotage():void
		{
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}	
		}
		
		private function attemptRelease():void
		{
			var aX:int = Data.selectedX;
			var aY:int = Data.selectedY;
			Data.ships[Data.selIndex].cMoves--;
			trace("cMoves: " + Data.ships[Data.selIndex].cMoves);
			if(Data.ships[Data.selIndex].cMoves < 1)
			{
				nextShip();	
			}
		}
		
		public function drawGame():void
		{
			//bring shit together
			stageContainer.addChild(background);
			stageContainer.addChild(ss);
			stageContainer.addChild(foreground);
			stageContainer.addChild(npcs);
			addChild(stageContainer);
			
			npcs.touchable = false;
			npcs.flatten();
			_hud.touchable = false;
			addChild(_hud);
			miniMap.y = 455;
			miniMap.x = 12;
			addChild(miniMap);
			
		}
	
		private function initPlayers():void
		{
			//spawn player 1 base
			//spawn player 2 base
			if(Data.derp == Data.p1) //if current player is player 1
			{
				Data.hasTurn = true;
				
				spawnFBuilding("base", mapW/2, 6); 
				spawnFBuilding("reactor", mapW/2, 5);
				spawnFBuilding("oreMine", mapW/2, 7);
				
				spawnFShip("engineer", mapW/2+1, 7);
				spawnFShip("destroyer", mapW/2+4, 7);
				
				
				spawnEShip("frigate", mapW/2+2, 8);		
				spawnEBuilding("dome", mapW/2+2, 9);
				
				centerCamOn(Data.structs[0].tX, Data.structs[0].tY);
				regenShieldsAndMoves();
			}
			else
			{
				Data.hasTurn = false;
			
				spawnFBuilding("base", mapW/2, mapH-6); 
				spawnFBuilding("reactor", mapW/2, mapH-5);
				spawnFBuilding("oreMine", mapW/2, mapH-7);
				
				spawnFShip("engineer", mapW/2+1, 7);
		
				centerCamOn(Data.structs[1].tX, Data.structs[1].tY);
				regenShieldsAndMoves();
			}	
		}
		
		private function loadGame():void
		{
			
		}
		
		private function initMap():void
		{
			var xbg:int = 0, ybg:int = 0;
			var i:int, j:int;
			for(i = 0; i<13; i++)
			{
				for(j = 0; j<8; j++)
				{
					var bg:Image = new Image(Assets.getBgAtlas().getTexture("space2"));
					bg.x = xbg;
					bg.y = ybg;
					background.addChild(bg);
					xbg+=648;
				}
				ybg+=393;
				xbg = 0;
			}
			
			background.flatten();
			
			for (i = 0; i<mapW; i++)
			{
				Data.mapTiles[i] = [];
				for(j = 0; j<mapH; j++)
				{
					var tile:Tile = new Tile(i, j);
					Data.mapTiles[i][j] = tile;
					tile.isBorder = false;
					foreground.addChild(tile);
				}
			}

			/**
			 * BORDER
			 */
			for(i = 0; i<mapW; i++)
			{
				for(j=0; j<3; j++)
				{
					Data.mapTiles[i][j].isBorder = true;
					Data.mapTiles[i][j].toFog();
				}
				for(j=mapH-1; j>=mapH-2; j--)
				{
					Data.mapTiles[i][j].isBorder = true;
					Data.mapTiles[i][j].toFog();
				}
			}
			
			/**
			 * STANGA
			 */
			for(i=0; i<=2; i++)
			{
				for(j=0; j<mapH; j++)
				{
					Data.mapTiles[i][j].isBorder = true;
					Data.mapTiles[i][j].toFog();
				}
			}
			
			/**
			 * DREAPTA
			 */
			for(i=mapW-1; i>=mapW-2; i--)
			{
				for(j=0; j<mapH; j++)
				{
					Data.mapTiles[i][j].isBorder = true;
					Data.mapTiles[i][j].toFog();
				}
			}
			
			/**
			 * FOG
			 */
			for(i = 3; i<mapW-3; i++)
				for(j = 3; j<mapH-3; j++)
				{
					//Data.mapTiles[i][j].toFog();
				}
		}
		
		private function initMiniMap():void
		{
			var x:int=0, y:int=0;
			for(var i:int = 3; i<mapW-3; i++)
			{
				miniMapArray[i] = [];
				for(var j:int = 3; j<mapH-3; j++)
				{
					var miniMapQ:Quad = new Quad((miniMapW+6)/mapW, (miniMapW+6)/mapH, 0xffffff);
					if(Data.mapTiles[i][j].isFog==true)
						miniMapQ.color = 0x000000;			//black
					else if(Data.mapTiles[i][j].team == 0)
					{
						if(Data.mapTiles[i][j].state == 0)
							miniMapQ.color = 0x000000;		//light grey
						else if(Data.mapTiles[i][j].state == 1)
							miniMapQ.color = 0x000000;
						else if(Data.mapTiles[i][j].state == 2)
							miniMapQ.color = 0x000000;
						else if(Data.mapTiles[i][j].state == 3)
							miniMapQ.color = 0x000000;
						else if(Data.mapTiles[i][j].state == 4)
							miniMapQ.color = 0x000000;
					}
					else if(Data.mapTiles[i][j].team == 1)
					{
						if(Data.faction == 1)
							miniMapQ.color = 0x000000;      //green
						else
							miniMapQ.color = 0x000000;		//red
					}
					else if(Data.mapTiles[i][j].team == 2)
					{
						if(Data.faction == 2)
							miniMapQ.color = 0x000000;		//green
						else
							miniMapQ.color = 0x000000;		//red
					}
					miniMapQ.x = x;
					miniMapQ.y = y;
					miniMapArray[i][j] = miniMapQ;
					//miniMapArray[i][j].addEventListener(TouchEvent.TOUCH, onMiniMapTouch);
					miniMap.addChild(miniMapArray[i][j]);
					
					y += miniMapQ.height;
				}
				x += miniMapQ.width;
				y = 0;
			}
		}
		
		/*private function onMiniMapTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch.phase == "ended")
				trace("touch");
		}*/
		
		private function leave_triggeredHandler():void
		{
			//game over
			//enemy is winner
		}
	
		private function drawHud():void
		{
			//_hud = new Hud();
			//addChild(_hud);
			//trace(stage.height);
			//_hud.addEventListener(TouchEvent.TOUCH, onActionTouch);
		}
		
		private function clearObstacles():void
		{
			
		}
		
		private function initAsteroids():void
		{	
			/**
			 * |---|---|
			 * | 1 | 2 |
			 * |---|---|
			 * | 3 | 4 |
			 * |---|---|
			 */
			
			/**
			 * Coords List
			 */
			var xs:Array = new Array(3, 3, 4, 3, 4, 5, 4, 5, 5, 9, 10, 9, 10, 11, 10, 11, 12, 11, 12, 13, 12, 8, 8, 9, 8, 9, 10, 9, 10,11, 10, 11, 12, 11, 12, 12, 14, 15, 16,
				17, 17, 17, 18, 19, 20, 21, 22, 22, 23, 24, 25, 18, 17, 16, 17, 15, 16, 15, 15, 15, 15, 15, 15);
			var ys:Array = new Array(13, 14, 14, 15, 15, 15, 16, 16, 17, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15, 18, 19, 19, 20, 20, 20, 21, 21, 21, 22, 22, 22, 23, 23,
				24, 24, 24, 24, 24, 20, 19, 18, 17, 16, 15, 15, 14, 14, 14, 14, 11, 11, 10, 10, 9, 9, 8, 7, 6, 5, 4, 3);
			
			var xas:int, yas:int; //jesus
			//trace("num of xs: " + xs.length);
			for(var i:int = 0; i<xs.length; i++)
			{
				xas=xs[i];
				yas=ys[i];
				
				var astro:Asteroid = new Asteroid(xas, yas);
				astro.flatten();
				astro.touchable = false;
				foreground.addChild(astro);
				Data.mapTiles[xas][yas].state = 4;
				
				var astro2:Asteroid = new Asteroid(mapW-xas, yas);
				astro2.flatten();
				astro2.touchable = false;
				foreground.addChild(astro2);
				Data.mapTiles[mapW-xas][yas].state = 4;
				
				var astro3:Asteroid = new Asteroid(xas, mapH-yas);
				astro3.flatten();
				astro3.touchable = false;
				foreground.addChild(astro3);
				Data.mapTiles[xas][mapH-yas].state = 4;
				
				var astro4:Asteroid = new Asteroid(mapW-xas, mapH-yas);
				astro4.flatten();
				astro4.touchable = false;
				foreground.addChild(astro4);
				Data.mapTiles[mapW-xas][mapH-yas].state = 4;
			}
		}
		
		private function initOreFields():void
		{
			/**
			 * |---|---|
			 * | 1 | 2 |
			 * |---|---|
			 * | 3 | 4 |
			 * |---|---|
			 */
			
			/**
			 * Coords List
			 */
			var xs:Array = new Array(4, 4, 5, 5, 22, 23, 21, 22, 21, 23, 24, 25, 22, 22, 21, 22, 23, 3, 3, 4);
			var ys:Array = new Array(4, 5, 4, 5, 4, 4, 5, 5, 6, 19, 18, 18, 22, 23, 24, 24, 24, 23, 24, 24);
			
			var xas:int, yas:int; //jesus
			
			for(var i:int = 0; i<xs.length; i++)
			{
				xas=xs[i];
				yas=ys[i];
				
				var astro:OreAsteroid = new OreAsteroid(xs[i], ys[i]);
				astro.flatten();
				astro.touchable = false;
				foreground.addChild(astro);
				Data.mapTiles[xas][yas].state = 1;
				
				var astro2:OreAsteroid = new OreAsteroid(mapW-xs[i], ys[i]);
				astro2.flatten();
				astro2.touchable = false;
				foreground.addChild(astro2);
				Data.mapTiles[mapW-xas][yas].state = 1;
				
				var astro3:OreAsteroid = new OreAsteroid(xs[i], mapH-ys[i]);
				astro3.flatten();
				astro3.touchable = false;
				foreground.addChild(astro3);
				Data.mapTiles[xas][mapH-yas].state = 1;
				
				var astro4:OreAsteroid = new OreAsteroid(mapW-xs[i], mapH-ys[i]);
				astro4.flatten();
				astro4.touchable = false;
				foreground.addChild(astro4);
				Data.mapTiles[mapW-xas][mapH-yas].state = 1;
			}
		}
		
		private function initObstacles():void
		{
			initAsteroids();
			initOreFields();
		}
		
		private function drawNPC():void
		{
			initObstacles();
		}
		
		private function zoomComplete():void
		{
			isZooming = false;
			//if(cZoom == 1)
				//cZoom = 2;
		    //else if(cZoom == 2)
				//cZoom = 3;
			//else if(cZoom == 3)
				//cZoom = 1;
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			keysPressed[e.keyCode] = false;
		}
		//ORIGINAL CHANGE BY 150
		private function onKeyDown(e:KeyboardEvent):void
		{
			keysPressed[e.keyCode] = true;
			if(e.keyCode == Keyboard.Z) //zoom
			{
				/**
				 * current Zoom
				 * 1->2->3->1->2
				 * 1 = 0.50 x 0.50
				 * 2 = 0.85 x 0.85 
				 * 3 = 1.00 x 1.00
				 */	
				trace("isZooming = " + isZooming);
				trace("current zoom: " + cZoom);
				if(!isZooming)
				{
					var zTween:Tween = new Tween(stageContainer, 0.75, Transitions.EASE_IN_OUT);
					switch(cZoom)
					{
						case 1:
						{
							//to 2
							trace("current zoom is 0.5 x 0.5");
							stageContainer.scaleX = 0.95;
							stageContainer.scaleY = 0.95;
							zTween.scaleTo(0.8);
							cZoom = 2;
							cMoveFactor = 325;
							break;
						}
						case 2:
						{
							//to 3
							trace("current zoom is 0.85 x 0.85");
							stageContainer.scaleX = 0.8;
							stageContainer.scaleY = 0.8;
							zTween.scaleTo(0.65);
							cZoom = 3;
							cMoveFactor = 400;
							break;
						}
						case 3:
						{
							//to 1
							trace("current zoom is 1 x 1");
							stageContainer.scaleX = 0.65;
							stageContainer.scaleY = 0.65;
							zTween.scaleTo(0.95);
							cZoom = 1;
							cMoveFactor = 250;
							break;
						}
					}
					isZooming = true;
					zTween.onComplete = zoomComplete;
					Starling.juggler.add(zTween);
				}
			}
			
			if(e.keyCode == Keyboard.M) //move action
			{
				if(Data.action == 0)
				{
					if(Data.tileSelected)
						if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
						{
							Data.action = 1;
							_hud.showSecondaryAlert();
							Data.actionRange = Data.ships[Data.mapTiles[Data.selectedX][Data.selectedY].sIndex].moves;
							genMoveRange();
						}
				}
				else
				{
					clearMoveRange();
					Data.action = 0;
				}
			}
			
			if(e.keyCode == Keyboard.T) //attack action
			{
				if(Data.action == 0)
				{
					if(Data.tileSelected)
						if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
						{
							if(Data.mapTiles[Data.selectedX][Data.selectedY].state != 16 && Data.mapTiles[Data.selectedX][Data.selectedY].state != 19)
							{
								Data.action = 2;
								_hud.showSecondaryAlert();
								Data.actionRange = Data.ships[Data.mapTiles[Data.selectedX][Data.selectedY].sIndex].attackRange;
								genAttackRange();
							}
						} else if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=5 && Data.mapTiles[Data.selectedX][Data.selectedY].state <15)
						{
							Data.action = 2;
							_hud.showSecondaryAlert();
							Data.actionRange = Data.structs[Data.mapTiles[Data.selectedX][Data.selectedY].sIndex].range;
							genAttackRange();
						}
				}
				else
				{
					clearRange();
					_hud.hideSecondaryAlert();
					Data.action = 0;
				}
			}
			
			if(e.keyCode == Keyboard.B) //build action
			{
				if(Data.action == 0)
				{
					if(Data.tileSelected)
						if(Data.mapTiles[Data.selectedX][Data.selectedY].state == 16)
						{
							Data.action = 3;
							_hud.showSecondaryAlert();
							Data.actionRange = Data.ships[Data.mapTiles[Data.selectedX][Data.selectedY].sIndex].buildRange;
							genBuildRange();
							
							_aWheel.isBuild = true;
							//trace("show building screen");
							_aWheel.addChild(_aWheel.reactor);
							_aWheel.addChild(_aWheel.oreMine);
							_aWheel.addChild(_aWheel.factory);
							_aWheel.addChild(_aWheel.monitor);
							_aWheel.addChild(_aWheel.fBase);
							_aWheel.addChild(_aWheel.cannon);
							_aWheel.addChild(_aWheel.turret);
							_aWheel.addChild(_aWheel.wall);
							_aWheel.addChild(_aWheel.fortress);
							_aWheel.addChild(_aWheel.dome);
							
							addChild(_aWheel);
						}	
				}
			}
			
			if(e.keyCode == Keyboard.O) //sniff action or recycle action
			{
				if(Data.tileSelected)
					if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
					{
						Data.action = 4; //sniff
						Data.action = 5; //recycle
					}	
			}
			
			if(e.keyCode == Keyboard.C) //cloak action
			{
				if(Data.tileSelected)
					if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
					{
						Data.action = 6;
					}	
			}
			
			if(e.keyCode == Keyboard.R) //cloak action
			{
				if(Data.tileSelected)
					if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
					{
						Data.action = 7;
					}	
			}
			
			if(e.keyCode == Keyboard.SPACE) //acshuns
			{	
				if(Data.tileSelected)
				if(((Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state<25) && Data.ships[Data.selIndex].faction
					==Data.faction) || ((Data.mapTiles[Data.selectedX][Data.selectedY].state >=5 && Data.mapTiles[Data.selectedX][Data.selectedY].state<15) 
						&& Data.structs[Data.selIndex].faction==Data.faction))
				{
					if(_aWheel.isBuild)
						_aWheel.isOn=true;
					if(_aWheel.isOn )
					{
						trace("action wheel is on");
						trace("aWheel.isBuild = " + _aWheel.isBuild);
						_aWheel.isOn = false;
						if(_aWheel.isBuild)
						{
							_aWheel.isOn = true;
							_aWheel.isBuild = false;
							removeBuildScreen();
							trace("removing build screen");
							trace("adding engi screen");
							_aWheel.addChild(_aWheel.pointers[4]);
							_aWheel.addChild(_aWheel.pointers[0]);
							_aWheel.addChild(_aWheel.pointers[2]);
							_aWheel.addChild(_aWheel.pointers[6]);
							_aWheel.addChild(_aWheel.move);
							_aWheel.addChild(_aWheel.build);
							_aWheel.addChild(_aWheel.repair);
							_aWheel.addChild(_aWheel.recycle);
							if(Data.action == 3)
								clearRange();
							trace("cancel build action");
							//Data.actionRange = -1;
							if(Data.aBuild != "derp")
								Data.aBuild = "derp";
							Data.action = 0;
							_hud.removeChild(_hud.cancelActionWarning);
							
						}
						else if(_aWheel.isSpawn)
						{
							trace("removing spawn screen");
							_aWheel.isOn = false;
							_aWheel.isSpawn = false;
							removeSpawnScreen();
							clearMainWheel();
							removeChild(_aWheel);
						}
						else
						{
							clearMainWheel();
							removeChild(_aWheel);
						}
					}
					else if(Data.action!=0 && (Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state<25))
					{
						//action canceled
						trace("action " + Data.action + " cancelled");
						if(Data.action == 1)
							clearMoveRange();
						else
							clearRange();
						trace("cancel action");
						//Data.actionRange = -1;
						if(Data.aBuild != "derp")
							Data.aBuild = "derp";
						_hud.hideSecondaryAlert();
						Data.action = 0;
						_aWheel.isOn = false;
						_hud.removeChild(_hud.cancelActionWarning);
					}
					else if(!Data.action)
					{
						/**
						 * Action Wheel
						 */
						trace("action wheel attempt");
						if(Data.tileSelected==true)
						{
							_aWheel.isOn = true;
							trace("action wheel accepted");
							trace("ship action for ship x: " + Data.selectedX + " y: " + Data.selectedY);
							if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=15 && Data.mapTiles[Data.selectedX][Data.selectedY].state <=24)
							{
								/**
								 * Ship actions
								 */
								
								addChild(_aWheel);
								_aWheel.addChild(_aWheel.infoSec);
								switch(Data.mapTiles[Data.selectedX][Data.selectedY].state)
								{
									case 15:
									{
										//frigate
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.attack);
										break;
									}
									case 16:
									{
										//engi
										trace("engi wheel");
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.pointers[6]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.build);
										_aWheel.addChild(_aWheel.repair);
										_aWheel.addChild(_aWheel.recycle);
										break;
									}
									case 17:
									{
										//destroyer
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.attack);
										break;
									}
									case 18:
									{
										//kelvin
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.release);
										_aWheel.addChild(_aWheel.attack);
										break;
									}
									case 19:
									{
										//corvette
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.heal);
										break;
									}
									case 20:
									{
										//carrier
										//_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										//_aWheel.addChild(_aWheel.attack);
										break;
									}
									case 21:
									{
										//infiltrator
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.pointers[6]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.cloak);
										_aWheel.addChild(_aWheel.zombify);
										_aWheel.addChild(_aWheel.attack);
										break;
									}
									case 22:
									{
										//sniffer
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.sniff);
										break;
									}
									case 23:
									{
										//drone
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.sabotage);
										break;
									}
									case 24:
									{
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.pointers[0]);
										_aWheel.addChild(_aWheel.pointers[2]);
										_aWheel.addChild(_aWheel.move);
										_aWheel.addChild(_aWheel.cloak);
										_aWheel.addChild(_aWheel.attack);
										break;
									}
								}
							}
							else if(Data.mapTiles[Data.selectedX][Data.selectedY].state >=5 && Data.mapTiles[Data.selectedX][Data.selectedY].state <15)
							{
								/**
								 * Building actions
								 */
								clearMainWheel();
								switch(Data.mapTiles[Data.selectedX][Data.selectedY].state)
								{
									case 5:
									{
										//base
										_aWheel.addChild(_aWheel.infoSec);
										_aWheel.addChild(_aWheel.pointers[4]);
										_aWheel.addChild(_aWheel.spawnEngi);
										addChild(_aWheel);
										break;
									}
									case 8:
									{
										//factory
										_aWheel.isSpawn = true;
										//trace("show building screen");
										_aWheel.addChild(_aWheel.frigate);
										_aWheel.addChild(_aWheel.engineer);
										_aWheel.addChild(_aWheel.destroyer);
										_aWheel.addChild(_aWheel.kelvin);
										_aWheel.addChild(_aWheel.corvette);
										_aWheel.addChild(_aWheel.carrier);
										_aWheel.addChild(_aWheel.infiltrator);
										_aWheel.addChild(_aWheel.scout);
										_aWheel.addChild(_aWheel.drone);
										_aWheel.addChild(_aWheel.striker);
										addChild(_aWheel);
										break;
									}
								}
							}
						}	
					}
				}
				
			}
			camMoveWASD();
		}
		
		private function camMoveWASD():void
		{
			/**
			 * Limits for:
			 * 		Zoom factor:
			 * 			1->
			 * 				W= 0
			 * 				A= 2500
			 * 				S= -41000
			 * 				D= -46000
			 * 			2->
			 * 				W=
			 * 				A=
			 * 				S=
			 * 				D=
			 * 			3->
			 * 				W=
			 * 				A=
			 * 				S=
			 * 				D=
			 */
			var limitW:int, limitD:int, limitA:int, limitS:int;
			if(cZoom == 1)
			{
				limitW = 0;
				limitA = 2500;
				limitS = -43600; 
				limitD = -40800;
			} else if(cZoom == 2)
			{
				limitW = 0;
				limitA = 2500;
				limitS = -42200; 
				limitD = -39500;
			} else if(cZoom == 3)
			{
				limitW = 0;
				limitA = 2500;
				limitS = -40600; 
				limitD = -36800;
			}
			if(keysPressed[Keyboard.A] || keysPressed[Keyboard.LEFT]) //move cam to left
			{
				
				if(camTarget.x<limitA) //if not hit left bound
				{
					trace(camTarget.x);
					camTarget.x+=cMoveFactor;
					_cam.update();
				}			
			}
			if(keysPressed[Keyboard.D] || keysPressed[Keyboard.RIGHT]) //move cam to right
			{
				if(camTarget.x>limitD) //right bound
				{
					camTarget.x-=cMoveFactor;
					_cam.update();
					//trace(camTarget.x);
				}	
			}
			if(keysPressed[Keyboard.W] || keysPressed[Keyboard.UP]) //move cam to up
			{
				if(camTarget.y<limitW) //up bound
				{
					camTarget.y+=cMoveFactor;
					_cam.update();
				}
				//trace(camTarget.y);	
			}
			if(keysPressed[Keyboard.S] || keysPressed[Keyboard.DOWN]) //move cam to down
			{
				if(camTarget.y>limitS) //down bound
				{
					trace(camTarget.y);
					camTarget.y-=cMoveFactor;
					_cam.update();
				}			
			}
		}
		
		private function initData():void
		{
			Data.tileSelected = false;
			//Data.selectedX = -1;
			//Data.selectedY = -1;
			
			Data.actionSelX = -1;
			Data.actionSelY = -1;
			Data.action = 0;
			Data.aBuild = "derp";
			
			Data.ore = 10000;
			Data.orePerTurn = 100;
			Data.energy = 1000;
			Data.maxEnergy = 400;
			Data.usedEnergy = 200;
			Data.totalEnergy = 600;
		}
		
		private function spawnEBuilding(type:String, x:int, y:int):void
		{
			Data.mapTiles[x][y].toEnemy();
			var faction:int;
			if(Data.faction == 1)
				faction = 2;
			else
				faction = 1;
			switch(type)
			{
				case "base": 
				{ 
					Data.mapTiles[x][y].state = 5; 
					Data.mapTiles[x+1][y].state = 5;
					Data.mapTiles[x][y].isBuildable = false;
					Data.mapTiles[x+1][y].isBuildable = false;
					Data.mapTiles[x+1][y].toEnemy();
					
					var base:Base = new Base();
					base.faction = faction;
					base.tX = x;
					base.tY = y;
					base.x = x*100;
					base.y = y*100;
					
					for(var i:int = x-base.buildRange; i<=x+base.buildRange; i++)
						for(var j:int = y-base.buildRange; j<=y+base.buildRange; j++)
							if((i>3 && i<mapH-3) && (j>3 && j<mapW-3))
								if(Data.mapTiles[i][j].state == 0)
								{
									Data.mapTiles[i][j].isBuildable = true;
								}
					
					Data.mapTiles[x][y].isBuildable = false;
					Data.mapTiles[x+1][y].isBuildable = false;
					
					base.touchable = false;
					
					Data.structs.push(base);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					Data.mapTiles[x+1][y].sIndex = Data.structs.length-1;
					break; 
				}
				case "factory": 
				{ 
					trace("build factory");
					Data.mapTiles[x][y].state = 8; 
					Data.mapTiles[x][y].isBuildable = false;
					Data.mapTiles[x+1][y].state = 8;
					Data.mapTiles[x+1][y].isBuildable = false;
					Data.mapTiles[x+1][y].toEnemy();
					
					var factory:Factory = new Factory();
					factory.faction = faction;
					factory.tX = x;
					factory.tY = y;
					factory.width = 200;
					factory.height = 100;
					factory.x = x*100;
					factory.y = y*100;

					factory.touchable = false;
					
					Data.structs.push(factory);
					
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					Data.mapTiles[x+1][y].sIndex = Data.structs.length-1;
					break;
				}
				case "reactor": 
				{ 
					Data.mapTiles[x][y].state = 6; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var reactor:Reactor = new Reactor();
					reactor.faction = faction;
					reactor.tX = x;
					reactor.tY = y;
					reactor.pivotX = 50;
					reactor.pivotY = 50;
					reactor.x = x*100+25;
					reactor.y = y*100+50;
					reactor.width = 100;
					reactor.height = 100;
					
					reactor.touchable = false;
					
					Data.structs.push(reactor);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break;
				}
				case "mine":
				{ 
					Data.mapTiles[x][y].state = 7; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var mine:Mine = new Mine();
					mine.faction = faction;
					mine.tX = x;
					mine.tY = y;
					mine.width = 100;
					mine.height = 100;
					mine.pivotX = mine.width/2;
					mine.pivotY = mine.height/2;
					mine.x = x*100+25;
					mine.y = y*100+50;
					
					mine.touchable = false;
					
					Data.structs.push(mine);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break; 
				}
				case "cannon": 
				{ 
					Data.mapTiles[x][y].state = 9; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var ion:IonCannon = new IonCannon();
					ion.faction = faction;
					ion.tX = x;
					ion.tY = y;
					ion.width = 100;
					ion.height = 100;
					ion.pivotX = ion.width/2;
					ion.pivotY = ion.height/2;
					ion.x = x*100+25;
					ion.y = y*100+50;
					
					ion.touchable = false;
					
					Data.structs.push(ion);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break; 
				}
				case "monitor": 
				{ 
					Data.mapTiles[x][y].state = 10;
					Data.mapTiles[x][y].isBuildable = false;
					
					var ar:MonitoringArray = new MonitoringArray();
					ar.faction = faction;
					ar.tX = x;
					ar.tY = y;
					ar.width = 100;
					ar.height = 100;
					ar.pivotX = ar.width/2;
					ar.pivotY = ar.height/2;
					ar.x = x*100+25;
					ar.y = y*100+50;
					
					ar.touchable = false;
					
					Data.structs.push(ar);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break; 
				}
				case "turret":
				{ 
					Data.mapTiles[x][y].state = 11; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var tu:TurretCannon = new TurretCannon();
					tu.faction = faction;
					tu.tX = x;
					tu.tY = y;
					tu.width = 100;
					tu.height = 100;
					tu.pivotX = tu.width/2;
					tu.pivotY = tu.height/2;
					tu.x = x*100+25;
					tu.y = y*100+50;
					
					tu.touchable = false;
					
					Data.structs.push(tu);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break;
				}
				case "wall":
				{ 
					Data.mapTiles[x][y].state = 12;
					Data.mapTiles[x][y].isBuildable = false;
					
					var w:Wall = new Wall();
					w.faction = faction;
					w.tX = x;
					w.tY = y;
					w.width = 100;
					w.height = 100;
					w.pivotX = w.width/2;
					w.pivotY = w.height/2;
					w.x = x*100+25;
					w.y = y*100+50;
		
					w.touchable = false;
					
					Data.structs.push(w);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break;
				}
				case "fortress":
				{ 
					Data.mapTiles[x][y].state = 13; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var fo:Fortress = new Fortress();
					fo.faction = faction;
					fo.tX = x;
					fo.tY = y;
					fo.width = 100;
					fo.height = 100;
					fo.pivotX = fo.width/2;
					fo.pivotY = fo.height/2;
					fo.x = x*100+25;
					fo.y = y*100+50;

					fo.touchable = false;
					
					Data.structs.push(fo);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break; 
				}
				case "dome":
				{
					Data.mapTiles[x][y].state = 14; 
					Data.mapTiles[x][y].isBuildable = false;
					
					var dome:Dome = new Dome();
					dome.faction = faction;
					dome.tX = x;
					dome.tY = y;
					dome.width = 100;
					dome.height = 100;
					dome.pivotX = dome.width/2;
					dome.pivotY = dome.height/2;
					dome.x = x*100+25;
					dome.y = y*100+50;
					
					dome.touchable = false;
					
					Data.structs.push(dome);
					foreground.addChild(Data.structs[Data.structs.length-1]);
					Data.mapTiles[x][y].sIndex = Data.structs.length-1;
					break;
				}
			}
		}
		
		private function spawnFBuilding(type:String, x:int, y:int):void
		{
			var ok:Boolean=true, los:int;
			
			switch(type)
			{
				case "base":
				{
					if(Data.mapTiles[x][y].state != 0 || Data.mapTiles[x+1][y].state != 0 )
						ok=false;	
					break;
				}
				case "factory":
				{
					if((Data.mapTiles[x][y].state != 0 || Data.mapTiles[x+1][y].state != 0 ) || (!Data.mapTiles[x][y].isBuildable || !Data.mapTiles[x+1][y].isBuildable))
						ok=false;
					break;
				}
				default:
				{
					if(Data.mapTiles[x][y].state || !Data.mapTiles[x][y].isBuildable)
						ok=false;
					break;
				}	
			}
			
			if(ok==false)
				_hud.addChild(_hud.cantBuild);
			else
			{
				if(Data.tileSelected)
					if(--Data.ships[Data.selIndex].cMoves>0)
						Data.ships[Data.selIndex].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[Data.selIndex].cMoves);
					else
						nextShip();
				_hud.hideSecondaryAlert();
				Data.action = 0;
				Data.aBuild = "derp";
				clearRange();
				if(_hud.cantBuild)
					_hud.removeChild(_hud.cantBuild);
				Data.mapTiles[x][y].toFriendly();
				var cms:int;
				switch(type)
				{
					case "base": 
					{ 
						Data.mapTiles[x][y].state = 5; 
						Data.mapTiles[x+1][y].state = 5;
						Data.mapTiles[x][y].isBuildable = false;
						Data.mapTiles[x+1][y].isBuildable = false;
						Data.mapTiles[x+1][y].toFriendly();
						
						var base:Base = new Base();
						base.faction = Data.faction;
						base.tX = x;
						base.tY = y;
						base.x = x*100;
						base.y = y*100;
						los = base.los;
						
						for(var i:int = x-base.buildRange; i<=x+base.buildRange; i++)
							for(var j:int = y-base.buildRange; j<=y+base.buildRange; j++)
								if((i>3 && i<mapH-3) && (j>3 && j<mapW-3))
									if(Data.mapTiles[i][j].state == 0)
									{
										Data.mapTiles[i][j].isBuildable = true;
									}
						
						Data.mapTiles[x][y].isBuildable = false;
						Data.mapTiles[x+1][y].isBuildable = false;
						
						base.touchable = false;
						
						Data.structs.push(base);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						Data.mapTiles[x+1][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 3000;
						Data.usedEnergy += 300;
						Data.energy -= 300;
						break; 
					}
					case "factory": 
					{ 
						trace("build factory");
						Data.mapTiles[x][y].state = 8; 
						Data.mapTiles[x][y].isBuildable = false;
						Data.mapTiles[x+1][y].state = 8;
						Data.mapTiles[x+1][y].isBuildable = false;
						Data.mapTiles[x+1][y].toFriendly();
						
						var factory:Factory = new Factory();
						factory.faction = Data.faction;
						factory.tX = x;
						factory.tY = y;
						factory.width = 200;
						factory.height = 100;
						factory.x = x*100;
						factory.y = y*100;
						
						los = factory.los;
						
						factory.touchable = false;
						
						Data.structs.push(factory);
						
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						Data.mapTiles[x+1][y].sIndex = Data.structs.length-1;
						
						
						Data.ore -= 1500;
						Data.usedEnergy += 50;
						Data.energy -=50;
						break;
					}
					case "reactor": 
					{ 
						var reactor:Reactor = new Reactor();
						reactor.faction = Data.faction;
						reactor.tX = x;
						reactor.tY = y;
						reactor.pivotX = 50;
						reactor.pivotY = 50;
						reactor.x = x*100+25;
						reactor.y = y*100+50;
						reactor.width = 100;
						reactor.height = 100;
						Data.mapTiles[x][y].state = 6; 
						Data.mapTiles[x][y].isBuildable = false;
						
						//reactor.setStats();
						
						los = reactor.los;
						
						reactor.touchable = false;
						
						Data.structs.push(reactor);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 800;
						Data.totalEnergy += 200;
						Data.energy +=200;
						break;
					}
					case "oreMine":
					{ 
						trace("spawning mine");
						Data.mapTiles[x][y].state = 7; 
						Data.mapTiles[x][y].isBuildable = false;
						
						var mine:Mine = new Mine();
						mine.faction = Data.faction;
						mine.tX = x;
						mine.tY = y;
						mine.width = 100;
						mine.height = 100;
						mine.pivotX = mine.width/2;
						mine.pivotY = mine.height/2;
						mine.x = x*100+25;
						mine.y = y*100+50;
						
						los = mine.los;
						
						mine.touchable = false;
						
						Data.structs.push(mine);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 1000;
						Data.orePerTurn += 200;
						Data.usedEnergy += 30;
						Data.energy -= 30;
						break; 
					}
					case "cannon": 
					{ 
						Data.mapTiles[x][y].state = 9; 
						Data.mapTiles[x][y].isBuildable = false;
						
						var ion:IonCannon = new IonCannon();
						ion.faction = Data.faction;
						ion.tX = x;
						ion.tY = y;
						ion.width = 100;
						ion.height = 100;
						ion.pivotX = ion.width/2;
						ion.pivotY = ion.height/2;
						ion.x = x*100+25;
						ion.y = y*100+50;
						
						los = ion.los;
						
						ion.touchable = false;
						
						Data.structs.push(ion);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 1600;
						Data.usedEnergy += 60;
						Data.energy -= 60;
						
						cms = Data.structs.length-1;
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIconBg);
						Data.structs[cms].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.structs[cms].numAttacks);
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIcon);
						break; 
					}
					case "monitor": 
					{ 
						Data.mapTiles[x][y].state = 10;
						Data.mapTiles[x][y].isBuildable = false;
						
						var ar:MonitoringArray = new MonitoringArray();
						ar.faction = Data.faction;
						ar.tX = x;
						ar.tY = y;
						ar.width = 100;
						ar.height = 100;
						ar.pivotX = ar.width/2;
						ar.pivotY = ar.height/2;
						ar.x = x*100+25;
						ar.y = y*100+50;
						
						los = ar.los;
						
						ar.touchable = false;
						
						Data.structs.push(ar);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 1900;
						Data.usedEnergy += 80;
						Data.energy -= 80;
						break; 
					}
					case "turret":
					{ 
						Data.mapTiles[x][y].state = 11; 
						Data.mapTiles[x][y].isBuildable = false;
						
						var tu:TurretCannon = new TurretCannon();
						tu.faction = Data.faction;
						tu.tX = x;
						tu.tY = y;
						tu.width = 100;
						tu.height = 100;
						tu.pivotX = tu.width/2;
						tu.pivotY = tu.height/2;
						tu.x = x*100+25;
						tu.y = y*100+50;
						
						los = tu.los;
						
						tu.touchable = false;

						Data.structs.push(tu);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 700;
						Data.usedEnergy += 30;
						Data.energy -= 30;
						
						cms = Data.structs.length-1;
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIconBg);
						Data.structs[cms].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.structs[cms].numAttacks);
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIcon);
						break;
					}
					case "wall":
					{ 
						Data.mapTiles[x][y].state = 12;
						Data.mapTiles[x][y].isBuildable = false;
						
						var w:Wall = new Wall();
						w.faction = Data.faction;
						w.tX = x;
						w.tY = y;
						w.width = 100;
						w.height = 100;
						w.pivotX = w.width/2;
						w.pivotY = w.height/2;
						w.x = x*100+25;
						w.y = y*100+50;
						
						los = w.los;
						
						w.touchable = false;

						Data.structs.push(w);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 500;
						Data.usedEnergy += 20;
						Data.energy -= 20;
						break;
					}
					case "fortress":
					{ 
						Data.mapTiles[x][y].state = 13; 
						Data.mapTiles[x][y].isBuildable = false;
						
						var fo:Fortress = new Fortress();
						fo.faction = Data.faction;
						fo.tX = x;
						fo.tY = y;
						fo.width = 100;
						fo.height = 100;
						fo.pivotX = fo.width/2;
						fo.pivotY = fo.height/2;
						fo.x = x*100+25;
						fo.y = y*100+50;
						
						los = fo.los;
						
						fo.touchable = false;
						
						Data.structs.push(fo);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 1100;
						Data.usedEnergy +=50;
						Data.energy -= 50;	
						
						cms = Data.structs.length-1;
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIconBg);
						Data.structs[cms].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.structs[cms].numAttacks);
						Data.mapTiles[x][y].tileBtn.addChild(Data.structs[cms].movesIcon);
						
						break; 
					}
					case "dome":
					{
						trace("dome here");
						Data.mapTiles[x][y].state = 14; 
						Data.mapTiles[x][y].isBuildable = false;
						
						var dome:Dome = new Dome();
						dome.faction = Data.faction;
						dome.tX = x;
						dome.tY = y;
						dome.width = 100;
						dome.height = 100;
						dome.pivotX = dome.width/2;
						dome.pivotY = dome.height/2;
						dome.x = x*100+25;
						dome.y = y*100+50;
						
						los = dome.los;
						
						dome.touchable = false;
						
						Data.structs.push(dome);
						foreground.addChild(Data.structs[Data.structs.length-1]);
						Data.mapTiles[x][y].sIndex = Data.structs.length-1;
						
						Data.ore -= 700;
						Data.usedEnergy += 70;
						Data.energy -= 70;
						break;
					}
				}
				for(i = x-los; i<=x+los; i++)
					for(j = y-los; j<= y+los; j++)
					{
						if((i>3 && i<mapH-3) && (j>3 && j<=mapW-3))
							if(Data.mapTiles[i][j].isFog == true)
								Data.mapTiles[i][j].toClear();
					}
				Data.maxEnergy = Data.totalEnergy - Data.usedEnergy;
				_hud.updateResources();
				if(type!="base" && type != "factory")
				{
					Data.structs[Data.structs.length-1].alpha = 0;
					Data.structs[Data.structs.length-1].scaleX = 0;
					Data.structs[Data.structs.length-1].scaleY = 0;
					var tReactor:Tween = new Tween(Data.structs[Data.structs.length-1], 0.3, Transitions.LINEAR);
					tReactor.fadeTo(1);
					tReactor.animate("scaleX", 0.5);
					tReactor.animate("scaleY", 1);
					Starling.juggler.add(tReactor);
				}
			}
		}
		
		private function spawnEShip(type:String, x:int, y:int):void
		{
			Data.mapTiles[x][y].isBuildable = false;
			var faction:int;
			if(Data.faction == 1)
				faction = 2;
			else 
				faction = 1;
			
			switch(type)
			{
				case "frigate":
				{ 
					var frigate:Frigate = new Frigate();
					frigate.faction = faction;
					Data.mapTiles[x][y].team = faction;
					frigate.tX = x;
					frigate.tY = y;
					frigate.x = x*100+50;
					frigate.y = y*100+50;
					
					frigate.touchable = false;
					
					frigate.cRotation = 2;
					frigate.pivotX = frigate.width/2;
					frigate.pivotY = frigate.height/2;
					frigate.rotation = deg2rad(90);
					frigate.cMoves = frigate.moves;
					
					Data.ships.push(frigate);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 15;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "engineer":
				{ 
					var engi:Engineer = new Engineer();
					engi.faction = faction;
					Data.mapTiles[x][y].team = faction;
					engi.tX = x;
					engi.tY = y;
					engi.x = x*100+50;
					engi.y = y*100+50;
					
					engi.touchable = false;
					
					engi.cRotation = 2;
					engi.pivotX = engi.width/2;
					engi.pivotY = engi.height/2;
					engi.rotation = deg2rad(90);
					engi.cMoves = engi.moves;
					
					Data.ships.push(engi);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 16;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "destroyer":
				{ 
					var destroyer:Destroyer = new Destroyer();
					destroyer.faction = faction;
					Data.mapTiles[x][y].team = faction;
					destroyer.tX = x;
					destroyer.tY = y;
					destroyer.x = x*100;
					destroyer.y = y*100;
					
					destroyer.touchable = false;
					
					Data.ships.push(destroyer);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 17;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "kelvin":
				{ 
					var kelvin:Kelvin = new Kelvin();
					kelvin.faction = faction;
					Data.mapTiles[x][y].team = faction;
					kelvin.tX = x;
					kelvin.tY = y;
					kelvin.x = x*100;
					kelvin.y = y*100;
					
					kelvin.touchable = false;
					
					Data.ships.push(kelvin);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 18;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "corvette":
				{
					var corvette:Corvette = new Corvette();
					corvette.faction = faction;
					Data.mapTiles[x][y].team = faction;
					corvette.tX = x;
					corvette.tY = y;
					corvette.x = x*100;
					corvette.y = y*100;
					
					corvette.touchable = false;
					
					Data.ships.push(corvette);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 19;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break; 
				}
				case "carrier":
				{ 
					var carrier:Carrier = new Carrier();
					carrier.faction = faction;
					Data.mapTiles[x][y].team = faction;
					carrier.tX = x;
					carrier.tY = y;
					carrier.x = x*100;
					carrier.y = y*100;
					
					carrier.touchable = false;
					
					Data.ships.push(engi);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 20;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "infiltrator":
				{ 
					var inf:Infiltrator = new Infiltrator();
					inf.faction = faction;
					Data.mapTiles[x][y].team = faction;
					inf.tX = x;
					inf.tY = y;
					inf.x = x*100;
					inf.y = y*100;
					
					inf.touchable = false;
					
					Data.ships.push(inf);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 21;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "scout":
				{ 
					var sni:Sniffer = new Sniffer();
					sni.faction = faction;
					Data.mapTiles[x][y].team = faction;
					sni.tX = x;
					sni.tY = y;
					inf.x = x*100;
					inf.y = y*100;
					
					inf.touchable = false;
					
					Data.ships.push(sni);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 22;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "drone":
				{
					var drone:Drone = new Drone();
					drone.faction = faction;
					Data.mapTiles[x][y].team = faction;
					drone.tX = x;
					drone.tY = y;
					drone.x = x*100;
					drone.y = y*100;
					
					drone.touchable = false;
					
					Data.ships.push(drone);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 23;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "striker":
				{ 
					var striker:Striker = new Striker();
					striker.faction = faction;
					Data.mapTiles[x][y].team = faction;
					striker.tX = x;
					striker.tY = y;
					striker.x = x*100;
					striker.y = y*100;
					
					striker.touchable = false;
					
					Data.ships.push(striker);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 24;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break; 
				}
			}
			
			Data.ships[Data.ships.length-1].tX = x;
			Data.ships[Data.ships.length-1].tY = y;
			
			Data.mapTiles[x][y].toEnemy();
		}
		
		private function spawnFShip(type:String, x:int, y:int):void
		{
			Data.mapTiles[x][y].isBuildable = false;
			var los:int;
			switch(type)
			{
				case "frigate":
				{ 
					var frigate:Frigate = new Frigate();
					frigate.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					frigate.tX = x;
					frigate.tY = y;
					frigate.x = x*100+50;
					frigate.y = y*100+50;
					
					frigate.touchable = false;
					
					frigate.cRotation = 2;
					frigate.pivotX = frigate.width/2;
					frigate.pivotY = frigate.height/2;
					frigate.rotation = deg2rad(90);
					frigate.cMoves = frigate.moves;
					
					los = frigate.los;
					
					Data.ships.push(frigate);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 15;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "engineer":
				{ 
					var engi:Engineer = new Engineer();
					engi.faction = Data.faction;
					trace("spawning engi with faction = " + engi.faction);
					Data.mapTiles[x][y].team = Data.faction;
					engi.tX = x;
					engi.tY = y;
					engi.x = x*100+50;
					engi.y = y*100+50;
					
					engi.touchable = false;

					engi.cRotation = 2;
					engi.pivotX = engi.width/2;
					engi.pivotY = engi.height/2;
					engi.rotation = deg2rad(90);
					engi.cMoves = engi.moves;
					
					los = engi.los;
					
					Data.ships.push(engi);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					//trace(Data.engineers[Data.engineers.length-1].x + " " + Data.engineers[Data.engineers.length-1].y );
					Data.mapTiles[x][y].state = 16;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					//trace(Data.engineers.length)
					break;
				}
				case "destroyer":
				{ 
					var destroyer:Destroyer = new Destroyer();
					destroyer.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					
					destroyer.tX = x;
					destroyer.tY = y;
					destroyer.x = x*100+50;
					destroyer.y = y*100+50;
					
					destroyer.cRotation = 2;
					destroyer.pivotX = destroyer.width/2;
					destroyer.pivotY = destroyer.height/2;
					destroyer.rotation = deg2rad(90);
					destroyer.cMoves = destroyer.moves;
					
					destroyer.touchable = false;
					
					los = destroyer.los;
					
					Data.ships.push(destroyer);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 17;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "kelvin":
				{ 
					var kelvin:Kelvin = new Kelvin();
					kelvin.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					kelvin.tX = x;
					kelvin.tY = y;
					kelvin.x = x*100+50;
					kelvin.y = y*100+50;
					
					kelvin.cRotation = 2;
					kelvin.pivotX = kelvin.width/2;
					kelvin.pivotY = kelvin.height/2;
					kelvin.rotation = deg2rad(90);
					kelvin.cMoves = kelvin.moves;
					
					kelvin.touchable = false;
					
					los = kelvin.los;
					
					Data.ships.push(kelvin);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 18;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "corvette":
				{
					var corvette:Corvette = new Corvette();
					corvette.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					corvette.tX = x;
					corvette.tY = y;
					corvette.x = x*100+50;
					corvette.y = y*100+50;
					
					corvette.cRotation = 2;
					corvette.pivotX = corvette.width/2;
					corvette.pivotY = corvette.height/2;
					corvette.rotation = deg2rad(90);
					corvette.cMoves = corvette.moves;
					
					corvette.touchable = false;
					
					los = corvette.los;
					
					Data.ships.push(corvette);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 19;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break; 
				}
				case "carrier":
				{ 
					var carrier:Carrier = new Carrier();
					carrier.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					carrier.tX = x;
					carrier.tY = y;
					carrier.x = x*100+50;
					carrier.y = y*100+50;
					
					carrier.cRotation = 2;
					carrier.pivotX = carrier.width/2;
					carrier.pivotY = carrier.height/2;
					carrier.rotation = deg2rad(90);
					carrier.cMoves = carrier.moves;
					
					carrier.touchable = false;
					
					los = carrier.los;
					
					Data.ships.push(engi);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 20;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "infiltrator":
				{ 
					var inf:Infiltrator = new Infiltrator();
					inf.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					inf.tX = x;
					inf.tY = y;
					inf.x = x*100+50;
					inf.y = y*100+50;
					
					inf.cRotation = 2;
					inf.pivotX = inf.width/2;
					inf.pivotY = inf.height/2;
					inf.rotation = deg2rad(90);
					inf.cMoves = inf.moves;
					
					inf.touchable = false;
					
					los = inf.los;
					
					Data.ships.push(inf);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 21;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "scout":
				{ 
					var sni:Sniffer = new Sniffer();
					sni.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					sni.tX = x;
					sni.tY = y;
					sni.x = x*100+50;
					sni.y = y*100+50;
					
					sni.cRotation = 2;
					sni.pivotX = sni.width/2;
					sni.pivotY = sni.height/2;
					sni.rotation = deg2rad(90);
					sni.cMoves = sni.moves;
					
					sni.touchable = false;
					
					los = sni.los;
					
					Data.ships.push(sni);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 22;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "drone":
				{
					var drone:Drone = new Drone();
					drone.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					drone.tX = x;
					drone.tY = y;
					drone.x = x*100+50;
					drone.y = y*100+50;
					
					drone.cRotation = 2;
					drone.pivotX = drone.width/2;
					drone.pivotY = drone.height/2;
					drone.rotation = deg2rad(90);
					drone.cMoves = drone.moves;
					
					drone.touchable = false;
					
					los = drone.los;
					
					Data.ships.push(drone);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 23;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break;
				}
				case "striker":
				{ 
					var striker:Striker = new Striker();
					striker.faction = Data.faction;
					Data.mapTiles[x][y].team = Data.faction;
					striker.tX = x;
					striker.tY = y;
					striker.x = x*100+50;
					striker.y = y*100+50;
					
					striker.cRotation = 2;
					striker.pivotX = striker.width/2;
					striker.pivotY = striker.height/2;
					striker.rotation = deg2rad(90);
					striker.cMoves = striker.moves;
					
					striker.touchable = false;
					
					los = striker.los;
					
					Data.ships.push(striker);
					foreground.addChild(Data.ships[Data.ships.length-1]);
					Data.mapTiles[x][y].state = 24;
					Data.mapTiles[x][y].sIndex = Data.ships.length-1;
					break; 
				}
			}
			
			Data.ships[Data.ships.length-1].tX = x;
			Data.ships[Data.ships.length-1].tY = y;
			
			Data.mapTiles[x][y].toFriendly();
			
			var cms:int = Data.ships.length-1;
			Data.mapTiles[x][y].tileBtn.addChild(Data.ships[cms].movesIconBg);
			Data.ships[cms].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[cms].moves);
			Data.mapTiles[x][y].tileBtn.addChild(Data.ships[cms].movesIcon);
			
			updateShipLos(cms);
		}	

		private function genMoveRange():void
		{
			var movesRange:int = Data.ships[cIndex].cMoves;
			var cIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;
			var slice:int = Data.ships[cIndex].cMoves*2+1;
			
			var bx:int = Data.selectedX - Data.ships[cIndex].cMoves;
			var bxEnd:int = bx + slice-1;
			var byU:int = Data.selectedY;
			var byD:int = Data.selectedY;
			
			var canAct:int = 0;
			for(var i:int = Data.selectedX-1; i<=Data.selectedX+1; i++)
				for(var j:int = Data.selectedY-1; j<=Data.selectedY+1; j++)
					if(Data.mapTiles[i][j].state >=15 || Data.mapTiles[i][j].state <25 || Data.mapTiles[i][j].state == 0 || Data.mapTiles[i][j].state == 2)
						canAct++;
			if(canAct==1)
			{
				_hud.addChild(_hud.noMove);
				Data.action = 0;
			}
			else
			{
				/*for(i = bx-1; i<= bxEnd+1; i++)
					for(j= Data.selectedY-slice-1; j<=Data.selectedY+slice+1; j++)
						Data.mapTiles[i][j].moveRange = -1;*/
				for(slice; slice>=1; slice--)
				{
					//trace("diamodn slice size = " + slice);
					for(i = bx; i<=bxEnd; i++)
					{
						//trace("check state: " + Data.mapTiles[i][byU].state);
						if(((Data.mapTiles[i][byU].state >=15 && Data.mapTiles[i][byU].state <25) || Data.mapTiles[i][byU].state == 0 || Data.mapTiles[i][byU].state == 2) 
								&& Data.mapTiles[i][byU].isBorder != true)	
						{
							//canAct = true; 
							//trace("mark avaliable state up: " + Data.mapTiles[i][byU].state);
							//if(Data.mapTiles[i][byU].state<15
							if(Data.mapTiles[i][byU].state == 0 || Data.mapTiles[i][byU].state == 2)
							{
								Data.mapTiles[i][byU].inRange = true;
								Data.mapTiles[i][byU].tileBtn.upState = Data.rangeTileSkin;
								if(Data.mapTiles[i][byU].isFog)
								{
									Data.mapTiles[i][byU].tileBtn.enabled = true;
									//Data.mapTiles[i][byU].tempFogFree = true;
								}
							}
							Data.mapTiles[i][byU].moveRange = 0;
						}
						
						if((byU!=byD && (Data.mapTiles[i][byD].state >=15 && Data.mapTiles[i][byD].state <25) || Data.mapTiles[i][byD].state == 0 
							|| Data.mapTiles[i][byD].state == 2) && Data.mapTiles[i][byD].isBorder != true)	
						{
							if(Data.mapTiles[i][byU].state == 0 || Data.mapTiles[i][byU].state == 2)
							{
								Data.mapTiles[i][byD].tileBtn.upState = Data.rangeTileSkin;
								Data.mapTiles[i][byD].inRange = true;
								if(Data.mapTiles[i][byD].isFog)
								{
									Data.mapTiles[i][byD].tileBtn.enabled = true;
									//Data.mapTiles[i][byD].isFog = false;
								}
							}
							Data.mapTiles[i][byD].moveRange = 0;
						}
					}
					bx++;
					bxEnd--;
					byU--;
					byD++;
					//trace("new diamon upper slice layout: x from " + bx + " to " + bxEnd + " on y = " + byU); 
				}
				Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
				Data.mapTiles[Data.selectedX][Data.selectedY].inRange = false;
			}
			/**
			 * Lee Path Generation
			 */
			var lx:int = Data.selectedX;
			var ly:int = Data.selectedY;
			var tileQueue:Array = new Array();
			tileQueue.push(Data.mapTiles[lx][ly]);
			Data.mapTiles[lx][ly].moveRange = 0;
			
			var inC:int = 0, sfC:int = 0;
			//var numMarked:int = 0;
			//var l2x:int; var l2y:int;
			do
			{
				//for every tile in the queue add avaliable nearby tiles to the queue
				lx = tileQueue[inC].xa;
				ly = tileQueue[inC].ya;
				//trace("--------->generating for tile with x = " + lx + "and y = " + ly);
				if(Data.mapTiles[lx+1][ly].moveRange == 0 && (lx+1 != Data.selectedX || ly != Data.selectedY))
				{
					//trace("Right tile with moveRange = " + Data.mapTiles[lx+1][ly].moveRange.toString());
					tileQueue.push(Data.mapTiles[lx+1][ly]);
					sfC++;
					Data.mapTiles[lx+1][ly].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("Right tile with new moveRange = " + Data.mapTiles[lx+1][ly].moveRange);
					//trace("moveRange " + Data.mapTiles[lx][ly].moveRange+1);
				}
				//trace("Lower tile coords: x= " + Data.mapTiles[lx][ly+1].xa + " and " + Data.mapTiles[lx][ly+1].ya + " with move range = " + Data.mapTiles[lx][ly+1].moveRange);
				//trace("Lower tile state " + Data.mapTiles[lx][ly+1].state);
				if(Data.mapTiles[lx][ly+1].moveRange == 0 && (lx != Data.selectedX || ly+1 != Data.selectedY)) 
				{
					tileQueue.push(Data.mapTiles[lx][ly+1]);
					sfC++;
					Data.mapTiles[lx][ly+1].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("push");
					//trace("moveRange " + Data.mapTiles[lx][ly].moveRange+1);
					//trace("Lower tile with new moveRange = " + Data.mapTiles[lx][ly+1].moveRange);
				}
				if(Data.mapTiles[lx-1][ly].moveRange == 0 && (lx-1 != Data.selectedX || ly != Data.selectedY)) 
				{
					tileQueue.push(Data.mapTiles[lx-1][ly]);
					sfC++;
					Data.mapTiles[lx-1][ly].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("push");
					//trace("moveRange " + Data.mapTiles[lx][ly].moveRange+1);
					//trace("Left tile with new moveRange = " + Data.mapTiles[lx-1][ly].moveRange);
				}
				if(Data.mapTiles[lx][ly-1].moveRange == 0 && (lx != Data.selectedX || ly-1 != Data.selectedY)) 
				{
					tileQueue.push(Data.mapTiles[lx][ly-1]);
					sfC++;
					Data.mapTiles[lx][ly-1].moveRange = Data.mapTiles[lx][ly].moveRange+1;
					//trace("push");
					//trace("moveRange " + Data.mapTiles[lx][ly].moveRange+1);
					//trace("Upper tile with new moveRange = " + Data.mapTiles[lx][ly-1].moveRange);
				}
				//trace("end of queue: " + sfC);
				//trace("right tile x tested: " + (lx+1).toString() + " with moveRange = " + Data.mapTiles[lx+1][ly].moveRange);
				//trace("queue x = " + lx + " and queue y = " + ly);
				inC++;
			}
			while(inC<=sfC);
			//trace(sfC + " firstly reachables");
			
			/**
			 * Clear unreachable tiles
			 */
			slice = Data.ships[cIndex].cMoves*2+1;
			
			bx= Data.selectedX - Data.ships[cIndex].cMoves-1;
			bxEnd = Data.selectedX + Data.ships[cIndex].cMoves+1;
			
			var uppLimit:int = Data.selectedY-Data.ships[cIndex].cMoves-1;
			var lowLimit:int = Data.selectedY+Data.ships[cIndex].cMoves+1;
			
			if(bx<3)
				bx=3;
			if(bxEnd>49)
				bxEnd = 49;
			if(uppLimit<3)
				uppLimit = 3;
			if(lowLimit > 47)
				lowLimit = 47;
			var num:int = 0;
			//trace("i starts with " + (bx-1).toString() + " ends with " + (bxEnd+1).toString());
			//trace("j starts with " + uppLimit + " ends with " + lowLimit);
			for(i = bx-1; i<=bxEnd; i++)
				for(j = uppLimit; j<= lowLimit; j++)
					if(Data.mapTiles[i][j].moveRange == 0 && !(i==Data.selectedX && j==Data.selectedY))
					{
						//unreachable tile
						//trace("unreachable: " + i + " " + j);
						Data.mapTiles[i][j].moveRange = -1;
						Data.mapTiles[i][j].inRange = false;
						if(Data.mapTiles[i][j].isFog)
						{
							Data.mapTiles[i][j].toFog();
						}
						else if(Data.mapTiles[i][j].team == 0)
							Data.mapTiles[i][j].tileBtn.upState = Data.emptyTileSkin;
						else if(Data.mapTiles[i][j].team == Data.faction)
							Data.mapTiles[i][j].tileBtn.upState = Data.friendlyTileSkin;
						else
							Data.mapTiles[i][j].tileBtn.upState = Data.enemyTileSkin;
					}
					else if(Data.mapTiles[i][j].moveRange!=-1)
					{
						num++;
						//trace("reachable: " + i + " " + j + " with range = " + Data.mapTiles[i][j].moveRange);
					}
			//trace(num + " reachables");
			//trace("moves needed for 29x6: " + Data.mapTiles[28][6].moveRange);
			//trace("moves needed for 30x6: " + Data.mapTiles[29][6].moveRange);
			//trace("current tile: " + Data.selectedX + " " + Data.selectedY);
			//trace("End of queue: " + sfC);
			
		}
		
		private function genAttackRange():void
		{
			var canAct:Boolean = false;
			var cIndex:int;
			trace("generating attack range");
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].state>=15 && Data.mapTiles[i][j].state <25 )
					{
						//against ship
						cIndex = Data.mapTiles[i][j].sIndex;
						if(Data.ships[cIndex].faction != Data.faction)
						{
							canAct = true;
							Data.mapTiles[i][j].inRange = true;
							Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
						}
					}
					else if (Data.mapTiles[i][j].state>=5 && Data.mapTiles[i][j].state <15)
					{
						//struct
						cIndex = Data.mapTiles[i][j].sIndex;
						if(Data.structs[cIndex].faction != Data.faction)
						{
							canAct = true;
							Data.mapTiles[i][j].inRange = true;
							Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
						}
					}
				}
			if(!canAct)
			{
				Data.action = 0;
				_hud.addChild(_hud.noEnemies);
			}
		}

		private function genBuildRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
					if(Data.mapTiles[i][j].isBuildable)
					{
						canAct = true;
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
			if(!canAct)
			{
				Data.action = 0;
				_hud.addChild(_hud.noBuild);
			}
		}
		
		private function genSniffRange():void
		{
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].state!=4)
					{
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
				}
		}

		private function genRepairRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
					if(Data.mapTiles[i][j].state>=5 && Data.mapTiles[i][j].state<15)
					{
						//trace("drawing repair area");
						if(Data.selectedX != i || Data.selectedY != j)
						{
							canAct = true;
							Data.mapTiles[i][j].inRange = true;
							Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
						}
					}
			if(!canAct)
			{
				//no nearby tiles to repair 
				Data.action = 0;
				_hud.addChild(_hud.noRepair);
			}
		}

		private function genRecycleRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].state == 3)
					{
						canAct = true;
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
				}
			if(!canAct)
			{
				//no nearby tiles to repair
				Data.action = 0;
				_hud.addChild(_hud.noRecycle);
			}
		}

		private function genZombifyRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].faction != Data.faction)
					{
						canAct = true;
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
				}
			if(!canAct)
			{
				Data.action = 0;
				_hud.addChild(_hud.noZombify);
			}
		}
		
		private function genHealRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].faction == Data.faction && Data.mapTiles[i][j].state >=15 && Data.mapTiles[i][j].state < 25)
					{
						canAct = true;
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
				}
			if(!canAct)
			{
				Data.action = 0;
				_hud.addChild(_hud.noHeal);
			}
		}
		
		private function genSabotageRange():void
		{
			var canAct:Boolean = false;
			for(var i:int = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(var j:int = Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					if(Data.mapTiles[i][j].faction != Data.faction && Data.mapTiles[i][j].state >=15 && Data.mapTiles[i][j].state < 25)
					{
						canAct = true;
						Data.mapTiles[i][j].inRange = true;
						Data.mapTiles[i][j].tileBtn.upState = Data.rangeTileSkin;
					}
				}
			if(!canAct)
			{
				Data.action = 0;
				_hud.addChild(_hud.noSabotage);
			}
		}
		
		private function clearMoveRange():void
		{
			trace("clearing move range");
			var movesRange:int = Data.ships[cIndex].cMoves;
			var cIndex:int = Data.mapTiles[Data.selectedX][Data.selectedY].sIndex;
			var slice:int = Data.ships[cIndex].cMoves*2+1;
			
			var bx:int = Data.selectedX - Data.ships[cIndex].cMoves;
			var bxEnd:int = bx + slice;
			var byU:int = Data.selectedY;
			var byD:int = Data.selectedY;
			
			var uppLimit:int = Data.selectedY-slice-1;
			var lowLimit:int = Data.selectedY+slice+1;
			if(bx<4)
				bx=4;
			if(bxEnd>48)
				bxEnd = 48;
			if(uppLimit<3)
				uppLimit = 3;
			if(lowLimit > 47)
				lowLimit = 47;
			for(var i:int = bx-1; i<=bxEnd+1; i++)
				for(var j:int = 3; j<= Data.selectedY+slice+1; j++)
					Data.mapTiles[i][j].moveRange = -1;

			
			for(slice; slice>=1; slice--)
			{
				//trace("diamodn slice size = " + slice);
				//trace("diamond upper slice layout: x from " + bx + " to " + bxEnd + " on y = " + byU); 
				//trace("clear move range");
				for(i = bx; i<=bxEnd; i++)
				{
					
					if((Data.mapTiles[i][byU].state >=15 || Data.mapTiles[i][byU].state <25 || Data.mapTiles[i][byU].state == 0 || Data.mapTiles[i][byU].state == 2) 
						&& Data.mapTiles[i][byU].isBorder !=true)
					{
						Data.mapTiles[i][byU].inRange = false;
						if(Data.mapTiles[i][byU].isFog)
						{
							Data.mapTiles[i][byU].tileBtn.upState = Data.hiddenTileSkin;
							Data.mapTiles[i][byU].tileBtn.enabled = false;
						}
						else if(Data.mapTiles[i][byU].team == 0)
							Data.mapTiles[i][byU].tileBtn.upState = Data.emptyTileSkin;
						else if(Data.mapTiles[i][byU].team == Data.faction)
							Data.mapTiles[i][byU].tileBtn.upState = Data.friendlyTileSkin;
						else if(Data.mapTiles[i][byU].team != Data.faction)
							Data.mapTiles[i][byU].tileBtn.upState = Data.enemyTileSkin;
							
					}
					
					if((Data.mapTiles[i][byD].state >=15 || Data.mapTiles[i][byD].state <25 || Data.mapTiles[i][byD].state == 0 || Data.mapTiles[i][byD].state == 2)
						&& Data.mapTiles[i][byU].isBorder !=true)
					{
						//Data.mapTiles[i][byD].tileBtn.upState = Data.rangeTileSkin;
						Data.mapTiles[i][byD].inRange = false;
						if(Data.mapTiles[i][byD].isFog)
						{
							Data.mapTiles[i][byD].tileBtn.upState = Data.hiddenTileSkin;
							Data.mapTiles[i][byD].tileBtn.enabled = false;						
						}
						else if(Data.mapTiles[i][byD].team == 0)
							Data.mapTiles[i][byD].tileBtn.upState = Data.emptyTileSkin;
						else if(Data.mapTiles[i][byD].team == Data.faction)
							Data.mapTiles[i][byD].tileBtn.upState = Data.friendlyTileSkin;
						else if(Data.mapTiles[i][byD].team != Data.faction)
							Data.mapTiles[i][byD].tileBtn.upState = Data.enemyTileSkin;
					}
				}
				bx++;
				bxEnd--;
				byU--;
				byD++;
			}
			Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.selectedTileSkin;
		}
		
		private function clearRange():void
		{
			var i:int, j:int, slice:int;
			var t1:int = Data.selectedX-Data.actionRange, t2:int = Data.selectedX + Data.actionRange;
			//trace("Start clear position x= " + t1 + " and end clear position x= " + t2);
			trace("clear Range");
			for(i = Data.selectedX-Data.actionRange; i<= Data.selectedX+Data.actionRange; i++)
				for(j= Data.selectedY-Data.actionRange; j<= Data.selectedY+Data.actionRange; j++)
				{
					//trace(i + "x" + j + " -> " + Data.mapTiles[i][j].state);
					Data.mapTiles[i][j].inRange = false;
					
					if(Data.mapTiles[i][j].isFog)
					{
						Data.mapTiles[i][j].tileBtn.upState = Data.hiddenTileSkin;
						Data.mapTiles[i][j].tileBtn.enabled = false;						
					}
					else if(Data.mapTiles[i][j].team == 0)
						Data.mapTiles[i][j].tileBtn.upState = Data.emptyTileSkin;
					else if(Data.mapTiles[i][j].team == Data.faction && !(i==Data.selectedX && j==Data.selectedY))
						Data.mapTiles[i][j].tileBtn.upState = Data.friendlyTileSkin;
					else if(Data.mapTiles[i][j].team != Data.faction)
						Data.mapTiles[i][j].tileBtn.upState = Data.enemyTileSkin;
				}
		}
		
		private function showPositionArrows():void
		{
			//trace(-1 * Data.mapTiles[Data.selectedX][Data.selectedY].xa*600 - 5200);
			if(-1 * Data.mapTiles[Data.selectedX][Data.selectedY].xa*600 + 400 < camTarget.x)
			{
				if(_hud.isR == false)
				{
					_hud.addChild(_hud.arrowR);
					_hud.isR = true;
				}
			}	
			else if(_hud.isR == true)
			{
				_hud.isR = false;
				_hud.removeChild(_hud.arrowR);
			}
			
			if(-1 * Data.mapTiles[Data.selectedX][Data.selectedY].xa*600 - 5900 > camTarget.x)
			{
				if(_hud.isL == false)
				{
					_hud.addChild(_hud.arrowL);
					_hud.isL = true;
				}
			}	
			else if(_hud.isL == true)
			{
				_hud.isL = false;
				_hud.removeChild(_hud.arrowL);
			}
			
			if(-1 * Data.mapTiles[Data.selectedX][Data.selectedY].ya*600 - 3250 > camTarget.y)
			{
				if(_hud.isU == false)
				{
					_hud.addChild(_hud.arrowU);
					_hud.isU = true;
				}
			}	
			else if(_hud.isU == true)
			{
				_hud.isU = false;
				_hud.removeChild(_hud.arrowU);
			}
			
			if(-1 * Data.mapTiles[Data.selectedX][Data.selectedY].ya*600 + 3500 < camTarget.y)
			{
				if(_hud.isD == false)
				{
					_hud.addChild(_hud.arrowD);
					_hud.isD = true;
				}
			}	
			else if(_hud.isD == true)
			{
				_hud.isD = false;
				_hud.removeChild(_hud.arrowD);
			}
		}
		
		private function onTick(e:Event):void
		{
			_cam.update();
			//trace(camTarget.x.toString()+" "+camTarget.y.toString());
			if(Data.tileSelected)
			{
				//showPositionArrows();
			}
			Data.ticksPerSec+=1;
			if(Data.ticksPerSec == 1)
			{
				Data.ticksPerSec = 0;
				if(Data.timeS-1<0)
				{
					if(Data.timeM == 0)
					{
						//next turn
						Data.timeM = 9;
						Data.timeS = 59;
						Data.pTurn++;
						if(Data.pTurn % 2 == Data.localP)
						{
							initTurn();
						}
						else
						{
							killTurn();
						}
					}
					else
					{
						Data.timeM--;
						Data.timeS = 59;
					}
				}
				else
				{
					Data.timeS--;
				}
			}
			if(Data.timeS>=10)
				_hud.siTimeText.text = "0" + Data.timeM.toString() + ":" + Data.timeS.toString();
			else
				_hud.siTimeText.text = "0" + Data.timeM.toString() + ":0" + Data.timeS.toString();
		}

		private function killTurn():void
		{
			//_hud.endTurn.x = 0;
			_hud.hideSecondaryAlert();
			var t:Tween = new Tween(_hud.endTurn, 2, Transitions.EASE_OUT);
			if(Data.resolution == 2)
				t.moveTo(230, 0);
			else 
				t.moveTo(105, 0);
			Starling.juggler.add(t);
			t.onComplete = _hud.endTurn.removeFromParent;
			_hud.clearDownBar();
			//_hud.removeChild(_hud.endTurn);
			_hud.addChild(_hud.enemyTurnWarning);
			if(Data.action == 1)
				clearMoveRange();
			else
				clearRange();
			if(Data.selectedX != -1 && Data.selectedY != -1)
			{
				if(Data.mapTiles[Data.selectedX][Data.selectedY].team == Data.faction)
				{
					Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileFriendly"));
				}
				else if(Data.mapTiles[Data.selectedX][Data.selectedY].team != Data.faction && Data.mapTiles[Data.selectedX][Data.selectedY].team)
					Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Texture.fromTexture(Assets.getHudAtlas().getTexture("tileEnemy"));
				else
					Data.mapTiles[Data.selectedX][Data.selectedY].tileBtn.upState = Data.emptyTileSkin;
				Data.tileSelected = false;
				
				if(_hud.isU)
					_hud.removeChild(_hud.arrowU);
				if(_hud.isD)
					_hud.removeChild(_hud.arrowD);
				if(_hud.isL)
					_hud.removeChild(_hud.arrowL);
				if(_hud.isR)
					_hud.removeChild(_hud.arrowR);
				
				var t2:Tween = new Tween(_hud.downBarContainer, 2, Transitions.EASE_OUT);
				t2.moveTo(0, 120);
				Starling.juggler.add(t2);
				t.onComplete = _hud.downBarOut;
				/*_hud.removeChild(_hud.downBarContainer);
				_hud.removeChild(_hud.siHpQuad);
				_hud.removeChild(_hud.siShieldQuad);
				_hud.removeChild(_hud.siMoraleQuad);*/
				if(Data.resolution == 2)
					selAvatar.y = 125;
				_hud.removeChild(selAvatar);
			}
			if(_aWheel.isOn)
			{
				clearMainWheel();
				removeChild(_aWheel);
				if(_aWheel.isBuild)
				{
					_aWheel.isBuild = false;
					removeBuildScreen();
				}
				else if(_aWheel.isSpawn)
				{
					_aWheel.isSpawn = false;
					removeSpawnScreen();
				}
				_aWheel.isOn = false;
				Data.selectedX = -1;
				Data.selectedX = -1;
			}
			if(Data.action)
			{
				clearRange();
				Data.action = 0;
				if(Data.aBuild != "derp")
					Data.aBuild = "derp";
				//Data.actionRange = -1;
				Data.actionSelX = -1;
				Data.actionSelY = -1;
			}
			Data.hasTurn = false;
		}
		
		private function initTurn():void
		{
			if(Data.resolution == 2)
				_hud.endTurn.x = 300;
			else
				_hud.endTurn.x = 100;
			
			_hud.addChild(_hud.endTurn);
			var t:Tween = new Tween(_hud.endTurn, 2, Transitions.EASE_OUT);
			if(Data.resolution == 2)
				t.moveTo(200, 0);
			else
				t.moveTo(0, 0);
			Starling.juggler.add(t);
			_hud.addChild(_hud.yourTurnWarning);
			Data.hasTurn = true;
			
			if(_hud.turnText)
				_hud.removeChild(_hud.turnText)
			
			if(Data.resolution == 2)
				_hud.downBarContainer.y = 125;
			else
				_hud.downBarContainer.y = 0;
			
			regenShieldsAndMoves();
			regenEnergy();
			prodOre();
		}
		
		private function regenShieldsAndMoves():void
		{
			for(var i:int=0; i<=Data.ships.length-1; i++)
			{
				if(Data.ships[i].faction == Data.faction && !Data.ships[i].dead) //current player's ship
				{
					//cData.ships[i].removeChild(movesIconBg
					Data.ships[i].cMoves = Data.ships[i].moves;
					//Data.ships[i].oldMoves = Data.ships[i].moves;
					Data.ships[i].regenShield();
					
					var cmX:int = Data.ships[i].tX;
					var cmY:int = Data.ships[i].tY;
					
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.ships[i].movesIconBg);
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.ships[i].movesIcon);
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.ships[i].noMovesIcon);
					
					Data.mapTiles[cmX][cmY].tileBtn.addChild(Data.ships[i].movesIconBg);
					Data.ships[i].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[i].cMoves);
					Data.mapTiles[cmX][cmY].tileBtn.addChild(Data.ships[i].movesIcon);
				}
			}
			for(i=0; i<=Data.structs.length-1; i++)
				if(Data.structs[i].faction == Data.faction && !Data.structs[i].dead && Data.structs[i].isDefensive) //current player's struct
				{
					Data.structs[i].numAttacks = Data.structs[i].maxNumAttacks;
					
					cmX = Data.structs[i].tX;
					cmY = Data.structs[i].tY;
					
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.structs[i].movesIconBg);
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.structs[i].movesIcon);
					Data.mapTiles[cmX][cmY].tileBtn.removeChild(Data.structs[i].noMovesIcon);
					
					Data.mapTiles[cmX][cmY].tileBtn.addChild(Data.structs[i].movesIconBg);
					Data.structs[i].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.structs[i].cMoves);
					Data.mapTiles[cmX][cmY].tileBtn.addChild(Data.structs[i].movesIcon);
				}
		}
		
		private function refreshStatIcons(i:int):void
		{
			//Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].tileBtn.enabled = true;
			Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].removeChild(Data.ships[i].noMovesIcon);
			Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].removeChild(Data.ships[i].movesIconBg);
			Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].removeChild(Data.ships[i].movesIcon);
			
			Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].addChild(Data.ships[i].movesIconBg);
			Data.ships[i].movesIcon.texture = Assets.getHudAtlas().getTexture("statIconsMoves"+Data.ships[i].cMoves);
			Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].addChild(Data.ships[i].movesIcon);
			
			//Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].removeChild(noMovesIcon);
			//Data.mapTiles[Data.ships[i].tX][Data.ships[i].tY].removeChild(noMovesIcon);
		}
		
		private function regenEnergy():void
		{
			Data.energy = Data.totalEnergy - Data.usedEnergy;
			_hud.updateResources();
		}
		
		private function prodOre():void
		{
			Data.ore += Data.orePerTurn;	
			_hud.updateResources();
		}
	
	}
}