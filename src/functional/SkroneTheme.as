package functional
{
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.themes.MetalWorksMobileTheme;
	
	import flash.text.TextFormat;
	
	import starling.display.DisplayObjectContainer;
	
	public class SkroneTheme extends MetalWorksMobileTheme
	{
		public static const MENU_BIG_BUTTON:String = "menuBigButton";
		public static const NO_BG_BUTTON:String = "noBgButton";
		
		public const HNeueSlimTextFormat:TextFormat = new TextFormat("Helvetica Neue LT Com", 18, 0xffffff);
		//public const HNeue2:TextFormat:TextFormat = new TextFormat("Helvetica Neue LT
		
		public function SkroneTheme(root:DisplayObjectContainer, scaleToDPI:Boolean=true)
		{
			super(root, scaleToDPI);
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			this.setInitializerForClass( Button, menuBigButtonInit, MENU_BIG_BUTTON);
			this.setInitializerForClass( Button, noBgButtonInit, NO_BG_BUTTON);
		}
		
		private function menuBigButtonInit(button:Button):void
		{
			button.defaultLabelProperties.textFormat = this.HNeueSlimTextFormat;
		}
		
		private function noBgButtonInit(button:Button):void
		{
			button.defaultLabelProperties.textFormat = this.HNeueSlimTextFormat;
		}
	}
}