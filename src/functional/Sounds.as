package functional
{
	import flash.media.Sound;

	public class Sounds
	{
		//[Embed(source="/../assets/sound/soundFx/click.wav")]
		//public static const SND_CLICK:Class;
		
		//by Kevin McLeod (http://incompetech.com)
		[Embed(source="/../assets/sound/music/Mechanolith.mp3")]
		public static const SND_MENU:Class;
		
		[Embed(source="../assets/sound/music/Long Note Three.mp3")]
		public static const GameMusic:Class;
		
		//public static var sndClick:Sound = new Sounds.SND_CLICK() as Sound;
		public static var sndMenu:Sound = new Sounds.SND_MENU() as Sound;
		public static var inGameMusic:Sound = new Sounds.GameMusic() as Sound;
	}
}