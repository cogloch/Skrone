package
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import functional.GameMenu;
	import functional.GameState;
	import functional.Login;
	
	import functional.Data;
	
	import playerio.*;
	
	[SWF(width="800", height="600", frameRate="30", backgroundColor="#000000")]
	
	public class GameProx extends flash.display.Sprite
	{
		private var _gameMenu:MenuInstance;
		private var _gameState:Skrone;
		private var _login:Login;
		
		private static var _client:Client;
		private static var _playerobject:DatabaseObject;
		
		public function GameProx()
		{
			super();
			trace("its the proxy root thingy");
			
			_login = new Login();
			addChild(_login);
		
			trace("show initial menu");
			_gameMenu = new MenuInstance();
			addChild(_gameMenu);
			
			addEventListener(Event.ENTER_FRAME, onTick);
		}
		
		private function faLogin():void
		{
			trace("attempt login");
			//_login.attemptLogin();
			PlayerIO.quickConnect.simpleConnect(
				stage, 
				'skrone-wiq21wmxkueja2yp64kia', 
				Data._usr, 
				Data._pss, 
				function(client:Client):void{
					trace("Connected", client); 
				}, function(e:PlayerIOError):void{
					trace("Error: ", e)
				}
			);
		}
		
		private function onStartGame():void
		{
			trace("start actual game");
			//removeChild(_gameMenu);64
			//_gameMenu.myStarling.dispose();
		
			_gameState = new Skrone();
			_gameState.myStarling.makeCurrent();
			addChild(_gameState);
		}
		
		private function onGameEnded():void
		{
			trace("show menu after game ended");
			removeChild(_gameState);
			_gameMenu = new MenuInstance();
			addChild(_gameMenu);
		}
		
		private function onTick(event:Object):void
		{
			if(Data.hitLogin)
			{
				trace("login hit");
				Data.hitLogin = false;	
				faLogin();
			}
			else if(Data.hitStartGame)
			{
				trace("start game listened");
				Data.hitStartGame = false;
				onStartGame();
			}
			else if(Data.gameEnded)
			{
				trace("g ended");
				Data.gameEnded = false;
			}
		}
	}
}