package buildings
{
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Defense extends Structure
	{
		public var range:int;
		public var numAttacks:int;
		public var dmg:int;
		public var maxNumAttacks:int;
		
		public var movesIcon:Image = new Image(Assets.getHudAtlas().getTexture("statIconsMoves"+numAttacks));
		public var movesIconBg:Image = new Image(Assets.getHudAtlas().getTexture("statIconsTurns"));
		public var noMovesIcon:Image = new Image(Assets.getHudAtlas().getTexture("noMovesIcon"));
		
		public function Defense()
		{
			super();
			isDefensive = true;
		}
	}
}