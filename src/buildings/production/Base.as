package buildings.production
{
	import buildings.NonCombat;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Base extends NonCombat
	{
		public var buildRange:int;
		
		public function Base()
		{
			super();
			type = "Base";
			sType = 5;
			
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 400;
			maxHp = 400;
			los = 4;
			buildRange = 7;
		}
	}
}