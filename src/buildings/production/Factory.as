package buildings.production
{
	import buildings.NonCombat;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Factory extends NonCombat
	{
		public function Factory()
		{
			super();
			type = "Deployment Station";
			sType = 8;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			los = 1;
		}
	}
}