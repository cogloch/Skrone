package buildings.production
{
	import buildings.NonCombat;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Reactor extends NonCombat
	{
		public function Reactor()
		{
			super();
			type = "Reactor";
			sType = 6;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		public function setStats():void
		{
			hp = 110;
			maxHp = 110;
			los = 2;
		}
	}
}