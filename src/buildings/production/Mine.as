package buildings.production
{
	import buildings.NonCombat;
	
	import functional.Assets;
	
	import starling.display.Image;

	public class Mine extends NonCombat
	{
		public function Mine()
		{
			type = "Ore Mine";
			sType = 7;
			icon.texture = Assets.getSSAtlas().getTexture("Ore Mine");
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 130;
			maxHp = 130;
			los = 2;
		}
	}
}