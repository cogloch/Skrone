package buildings
{
	import functional.Assets;
	import functional.Data;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Structure extends Sprite
	{	
		public var faction:int;
		public var isDefensive:Boolean=false;
		public var tX:int;
		public var tY:int;
		
		public var hp:int;
		public var maxHp:int;
		public var los:int;
		
		//public var sX:int;
		//public var sY:int;
		
		public var type:String;
		public var sType:int;
		
		public var dead:Boolean = false;
		
		public var icon:Image = new Image(Assets.getSSAtlas().getTexture("Base"));
		
		public function Structure()
		{
			super();
			
			addChild(icon);
			touchable = false;
		}
		
		private function die():void
		{
			//dispatchEventWith("die", true);
		}
		
		public function getDmg(dmgAmount:int, eMorale:int):void
		{
			hp-=dmgAmount;
			
			//feedback
			if(hp<1)
			{
				Data.died = true;
				die();
			}
			else
				Data.died = false;
			
			Data.hpDmg = dmgAmount;
		}
		
		private function attack():void
		{
			
		}
		
		
	}
}