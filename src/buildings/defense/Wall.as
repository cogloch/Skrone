package buildings.defense
{
	import buildings.Defense;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Wall extends Defense
	{
		public function Wall()
		{
			super();
			type = "Energy Field";
			sType = 12;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 100;
			maxHp = 100;
			los = 1;
			range = 0;
			numAttacks = 0;
			dmg = 0;
		}
	}
}