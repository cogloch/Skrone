package buildings.defense
{
	import buildings.Defense;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class TurretCannon extends Defense
	{
		public function TurretCannon()
		{
			super();
			type = "Turret";
			sType = 11;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 80;
			maxHp = 80;
			los = 6;
			range = 3;
			numAttacks = 2;
			maxNumAttacks = 2;
			dmg = 50;
		}
	}
}