package buildings.defense
{
	import buildings.Defense;
	
	import starling.display.Image;
	
	import functional.Assets;
	
	public class Fortress extends Defense
	{	
		public function Fortress()
		{
			super();
			type = "Fortress";
			sType = 13;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 200;
			maxHp = 200;
			los = 3;
			range = 3;
			numAttacks = 3;
			maxNumAttacks = 4;
			dmg = 40;
		}
	}
}