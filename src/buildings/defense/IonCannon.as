package buildings.defense
{
	import buildings.Defense;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class IonCannon extends Defense
	{
		public function IonCannon()
		{
			super();
			type = "Cannon";
			sType = 9;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			los = 150;
			range = 4;
			numAttacks = 1;
			maxNumAttacks = 1;
			dmg = 90;
		}
	}
}