package buildings.special
{
	import buildings.NonCombat;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class Dome extends NonCombat
	{
		public function Dome()
		{
			super();
			type = "Stealth Dome";
			sType = 14;
			icon.texture = Assets.getSSAtlas().getTexture(type);
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 140;
			maxHp = 140;
			
			los = 4;
		}
	}
}