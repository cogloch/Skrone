package buildings.special
{
	import buildings.NonCombat;
	import buildings.Structure;
	
	import functional.Assets;
	
	import starling.display.Image;
	
	public class MonitoringArray extends NonCombat
	{
		public function MonitoringArray()
		{
			super();
			type = "N-DAR";
			sType = 10;
			icon.texture = Assets.getSSAtlas().getTexture("N-DAR");
			
			setStats();
		}
		
		private function setStats():void
		{
			hp = 150;
			maxHp = 150;
			los = 3;
		}
	}
}