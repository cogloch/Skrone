package 
{
	//import citrus.core.starling.StarlingCitrusEngine;
	
	import feathers.system.DeviceCapabilities;
	
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.system.Capabilities;
	
	import functional.GameMenu;
	import functional.GameState;
	
	import playerio.*;
	
	import screens.GameOverScreen;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	//import utils.AssetManager;
	
	//import screens.TitleScreen;
	/**
	 * Resolutions:
	 * 1-800x600
	 * 2-1024-768
	 */
	
	[SWF(width="800", height="600", frameRate="30", backgroundColor="#000000")]
	
	public class Skrone extends Sprite
	{
		public var myStarling:Starling;
		
		public function Skrone()
		{
			super();
			trace("the game state bitch");
			
			if(this.stage)
			{
				stage.align = StageAlign.TOP_LEFT;
				stage.scaleMode = StageScaleMode.NO_SCALE;
			}

			//DeviceCapabilities.dpi = 130;
			//DeviceCapabilities.screenPixelWidth = 800;
			//DeviceCapabilities.screenPixelHeight = 600;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function start():void
		{
			/**
			 * This is where it starts
			 */
			//this.graphics.clear();
			
			//var isMac:Boolean = Capabilities.manufacturer.indexOf("Macintosh") != -1;
			//Starling.handleLostContext = !isMac;
			
			//Starling.multitouchEnabled = true;
			//stage.removeChildren();
			myStarling = new Starling(GameState, this.stage);
			myStarling.antiAliasing = 16;
			myStarling.showStats = false;
			//myStarling.showStatsAt("left", "top", 1);
			myStarling.start();
			//var stage:starling.display.Stage = Starling.current.stage;
			//var myGame:GameMenu = stage.getChildAt(0) as GameMenu;
		}
		
		protected function onAddedToStage(event:Object):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			start();
		}
		
	}
}