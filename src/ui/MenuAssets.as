package ui
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	
	public class MenuAssets
	{	
		/**
		 * Shared assets
		 */
		[Embed(source="/../assets/bg.PNG")]
		public static var bg:Class;
		
		/**
		 * Title screen assets
		 */
		public var tHeader:Class;
		public var tGameLogo:Class;
		public var tDevLogo:Class;
		public var tLoginBg:Class;
		public var tTabBg:Class;
		
		/**
		 * Registration 1 assets
		 */
		public var r1Header:Class;
		public var r1Tosse:Class;
		public var r1Korbis:Class;
		
		/**
		 * Registration 2 assets
		 */
		public var r2Header:Class;
		
		/**
		 * Home assets
		 */
		
		/**
		 * Profile assets
		 */
		
		/**
		 * Play assets
		 */
		
		/**
		 * Help assets
		 */
		
		/**
		 * Leaderboard assets
		 */
		
		/**
		 * Lobby assets
		 */
		
		/**
		 * Arena assets
		 */
		
		/**
		 * Games list assets
		 */
		
		/**
		 * Create game assets
		 */
		
		/**
		 * Challenges assets
		 */
		
		/**
		 * Galaxy map assets
		 */
		
		/**
		 * Store assets
		 */
		
		/**
		 * Social assets
		 */
		
		/**
		 * Hangar assets
		 */
		
		public function MenuAssets()
		{
		}
	}
}