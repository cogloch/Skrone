package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	import feathers.controls.TabBar;
	import feathers.data.ListCollection;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	[Event(name="showMainMenu", type="starling.events.Event")]
	[Event(name="showProfile", type="starling.events.Event")]
	public class ProfileLeaderboardScreen extends Screen
	{
		public const SHOW_MAIN_MENU:String = "showMainMenu";
		public const SHOW_PROFILE:String = "showProfile";
		public function ProfileLeaderboardScreen()
		{
			super();
		}
		
		/*private var _showTitle:Button; //if got from title
		private var _showProfile:Button; //if got from profile*/
		private var _backButton:Button;
		
		private var _categories:TabBar;
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			this._categories = new TabBar();
			this._categories.dataProvider = new ListCollection(
				[
					{ label: "ELO" },
					{ label: "Experience" },
					{ label: "Credits" },
				]);
			this._categories.addEventListener(Event.CHANGE, categories_changeHandler);
			this.addChild(this._categories);
			
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._header = new Header;
			this._header.title = "Leaderboard";
			this.addChild(this._header);
			this._header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._categories.validate();
			this._categories.width = this.actualWidth;
			//down
			this._categories.y = this.actualHeight - this._header.height;
			//up
			//this._categories.y  = this._header.height;
		}
		
		private function categories_changeHandler():void
		{
			
		}
		
		private function back_triggeredHandler():void
		{
			trace("Leaderboard back triggered");
			this.dispatchEventWith(SHOW_PROFILE);
		}
	}
}




