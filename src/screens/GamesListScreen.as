package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.data.ListCollection;
	
	import functional.Data;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	[Event(name="showCreateGame", type="starling.events.event")]
	[Event(name="showArena", type="starling.events.Event")]
	[Event(name="showLobby", type="starling.events.Event")]
	
	public class GamesListScreen extends Screen
	{
		public const SHOW_CREATE_GAME:String = "showCreateGame";
		public const SHOW_ARENA:String = "showArena";
		public const SHOW_LOBBY:String = "showLobby";
		
		public var items:Array = [];
		
		public function GamesListScreen()
		{
			super();
		}
		
		private var _header:Header;
		
		private var _backButton:Button;
		private var _createButton:Button;
		public var _joinButton:Button;
		
		private var _gamesList:List;
		
		override protected function initialize():void
		{
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._createButton = new Button();
			this._createButton.label = "Create Game";
			this._createButton.addEventListener(Event.TRIGGERED, create_triggeredHandler);
			this.addChild(this._createButton);
			
			this._header = new Header();
			this._header.title = "Games List";
			this._header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this._header.rightItems = new <DisplayObject>
				[
					this._createButton
				];
			this.addChild(this._header);
			
			populate_list();
			
			this._gamesList = new List();
			this._gamesList.dataProvider = new ListCollection(items);
			this._gamesList.isSelectable = true;
			this._gamesList.scrollerProperties.hasElasticEdges = false;
			this._gamesList.itemRendererProperties.labelField = "text";
			this._gamesList.addEventListener(Event.CHANGE, list_changeHandler);
			this.addChildAt(this._gamesList, 0);
			
			this._joinButton = new Button();
			this._joinButton.label = "Join";
			this._joinButton.addEventListener(Event.TRIGGERED, join_triggeredHandler);
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._gamesList.validate();
			this._gamesList.y = this._header.height;
			this._gamesList.width = this.actualWidth;
			this._gamesList.height = this.actualHeight - this._gamesList.y;
			
			this._joinButton.validate();
			//this._joinButton.x = 50;
			//this._joinButton.y = 50;
		}
		
		private function back_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_ARENA);
		}
		
		private function create_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_CREATE_GAME);
		}
		
		public function populate_list():void
		{
			//get rooms
			if(Data.derp == "test" || Data.derp == "Pandam0nium13")
			{
				//offline
				trace("Populate offline list");
				for(var i:int = 0; i < 50; i++)
				{
					var item:Object = {text: "Room " + (i + 1).toString()};
					items.push(item);
				}
				items.fixed;
			}
			else
			{
				//online
				//get rooms
			}
		}
		
		public function list_changeHandler():void
		{
			this._joinButton.x = 620;
			this._joinButton.y = this._gamesList.selectedItem.y + 10; 
			trace(this._joinButton.x.toString + " by " + this._joinButton.y.toString);
			this._joinButton.validate();
			this.addChild(this._joinButton);
		}
		
		private function join_triggeredHandler():void
		{
			//join room
			trace("Join room");
			this.dispatchEventWith(SHOW_LOBBY);
		}
	}
}