package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Screen;
	
	import starling.events.Event;
	
	[Event(name="showPlay", type="starling.events.Event")]
	
	/**
	 * CHALLANGES SCREEN
	 */
	
	public class MissionsScreen extends Screen
	{
		public const SHOW_PLAY:String = "showPlay";
		
		public function MissionsScreen()
		{
			super();
		}
		
		private var _questsList:List;
		
		private var _backButton:Button;
		private var _acceptMissionButton:Button;
		private var _collectRewardButton:Button;
		//branching dialog - probably an end feature, although pretty impressive
		//private var _dialogOption1:Button;
		//private var _dialogOption2:Button;
		//private var _dialogOption3:Button;
		//ending branching dialog
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			/*
				Pull from db a characteristic array _dbQuests with the following values:
					0 if quest is unavaliable
					1 if quest is not accepted but avaliable
					2 if quest is accepted but not complete - avaliable
					3 if quest is complete - unavaliable
				Also pull _dbMaxQuestId - the biggest id of avaliable quests(quest ids ranging from 1 to n)
				Go from j=1 to maxQuestId
					If _dbQuests is 1 || 2 then it is avaliable
						Set _quests[i] to _dbQuests[j]
			*/
			/*
				Option 2(much facster and easier)
				Make an array of instances of a class Quest
			*/
			var _quests:Array = [];
			_quests[1] = "quest1";
			_quests[2] = "quest2";
			_quests[3] = "quest3";
			
			this._questsList = new List();
			this.addChild(this._questsList);
			
			this._acceptMissionButton = new Button();
			this._acceptMissionButton.label = "Accept Mission";
			this._acceptMissionButton.addEventListener(Event.TRIGGERED, acceptMission_triggeredHandler);
			this.addChild(this._acceptMissionButton);
			
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._header = new Header;
			this._header.title = "Challenges";
			this.addChild(this._header);
			
			this._collectRewardButton = new Button();
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._backButton.validate();
			this._backButton.x = 700;
			this._backButton.y = 560;
			
			if(1==1)
			{
				this._acceptMissionButton.validate();
			}
		}
		
		private function back_triggeredHandler():void
		{
			trace("Back Clicked");
			this.dispatchEventWith(SHOW_PLAY);
		}
		
		private function acceptMission_triggeredHandler():void
		{
			//accept mission
		}
	}
}