package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.ToggleSwitch;
	
	import functional.Assets;
	import functional.Data;
	import functional.GameState;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.textures.Texture;
	
	[Event(name="showArena", type="starling.events.Event")]
	public class LobbyScreen extends Screen
	{
		public var _p1:String = Data.derp; //test value
		public var _p2:String;
		public var _host:String = _p1;
		
		private static var _p1rdy:Boolean;
		private static var _p2rdy:Boolean = true;
		
		public const SHOW_ARENA:String = "showArena";
		
		public function LobbyScreen()
		{
			super();
		}
		
		private var _leaveButton:Button;
		private var _startGameButton:Button;	
		
		private var _map:Image;
		
		private var _header:Header;
		
		private var _tip:Label;
		
		private var _p1Avatar:Button;
		private var _p1Name:Label;
		private var _p1Rank:Label;
		private var _p1Rating:Label;
		private var _p1RdyLabel:Label;
		private var _p1RdyToggle:ToggleSwitch;
		
		private var _p2Avatar:Button;
		private var _p2Name:Label;
		private var _p2Rank:Label;
		private var _p2Rating:Label;
		private var _p2RdyLabel:Label;
		private var _p2RdyToggle:ToggleSwitch;
		
		private var lHint:Image;
		private var lChatBg:Image;
		private var playerLobbyBg:Image;
		private var mapLobby:Image;
		
		override protected function initialize():void
		{
			this.mapLobby = new Image(Assets.getMenuAtlas().getTexture("mapLobby"));
			this.mapLobby.x = 0;
			this.mapLobby.y = -50;
			this.addChild(mapLobby);
			
			this.playerLobbyBg = new Image(Assets.getMenuAtlas().getTexture("playerLobbyBg"));
			this.playerLobbyBg.x = 0;
			this.playerLobbyBg.y = -50;
			this.addChild(playerLobbyBg);
			
			this.lHint = new Image(Assets.getMenuAtlas().getTexture("hintLobby"));
			lHint.y = -50;
			lHint.x = -10;
			this.addChild(lHint);
			
			this.lChatBg = new Image(Assets.getMenuAtlas().getTexture("chatLobby"));
			lChatBg.x = 0;
			lChatBg.y = -40;
			this.addChild(lChatBg);
			
			this._p1Avatar = new Button();
			this._p1Avatar.isEnabled = false; //test value
			this._p1Avatar.addEventListener(Event.TRIGGERED, avatar_triggeredHandler);
			this.addChild(this._p1Avatar);
			
			this._p1Name = new Label();
			this._p1Name.text = Data.derp;
			this.addChild(this._p1Name);
			
			this._p1Rank = new Label();
			this._p1Rank.text = "Academy Recruit";
			this.addChild(this._p1Rank);
			
			this._p1Rating = new Label();
			this._p1Rating.text = "ELO Rating: " + "1543";
			this.addChild(this._p1Rating);
			
			this._p1RdyLabel = new Label();
			this._p1RdyLabel.text = "Ready";
			this.addChild(this._p1RdyLabel);
			
			this._p1RdyToggle = new ToggleSwitch();
			this._p1RdyToggle.isSelected = false;
			if(Data.derp != _p1)
				this._p1RdyToggle.isEnabled = false;
			this._p1RdyToggle.onText = "YES";
			this._p1RdyToggle.offText = "NO";
			//this._p1RdyToggle.addEventListener(Event.CHANGE, rdy_triggeredHandler);
			this.addChild(this._p1RdyToggle);
			
			this._p2Avatar = new Button();
			this._p2Avatar.isEnabled = false; //test value
			this._p2Avatar.addEventListener(Event.TRIGGERED, avatar_triggeredHandler);
			this.addChild(this._p2Avatar);
			
			this._p2Name = new Label();
			this._p2Name.text = "Player 2";
			this.addChild(this._p2Name);
			
			this._p2Rank = new Label();
			this._p2Rank.text = "Colonel";
			this.addChild(this._p2Rank);
			
			this._p2Rating = new Label();
			this._p2Rating.text = "ELO Rating: " + "2106";
			this.addChild(this._p2Rating);
			
			this._p2RdyLabel = new Label();
			this._p2RdyLabel.text = "Ready";
			this.addChild(this._p2RdyLabel);
			
			this._p2RdyToggle = new ToggleSwitch();
			this._p2RdyToggle.isSelected = true;
			if(Data.derp != _p2)
				this._p2RdyToggle.isEnabled = false;
			this._p2RdyToggle.onText = "YES";
			this._p2RdyToggle.offText = "NO";
			//this._p2RdyToggle.addEventListener(Event.CHANGE, rdy_triggeredHandler);
			this.addChild(this._p2RdyToggle);
			
			this._leaveButton = new Button();
			this._leaveButton.label = "Leave";
			this._leaveButton.addEventListener(Event.TRIGGERED, left_triggeredHandler);
			this.addChild(this._leaveButton);
			
			this._startGameButton = new Button();
			this._startGameButton.label = "Start Game";
			this._startGameButton.addEventListener(Event.TRIGGERED, start_triggeredHandler);
			
			if(Data.derp == _host)
			{
				//current player is host
				trace(Data.derp + " is host");
				this.addChild(this._startGameButton);
			}
			
			this._header = new Header();
			this._header.title = Data.derp + " versus " + "Player 2"; //+ " on " + "Kelorus";
			this._header.leftItems = new <DisplayObject>
				[
					this._leaveButton
				];
			this._header.rightItems = new <DisplayObject>
				[
					this._startGameButton
				];
			this.addChild(this._header);
		}
		
		override protected function draw():void
		{
			/*this._leaveButton.validate();
			this._leaveButton.x = 700;
			this._leaveButton.y = 560;*/
			
			this._header.validate();
			this._header.width = this.actualWidth;
			
			/**
			 * Player 1 box
			 */
			this._p1Avatar.validate();
			this._p1Avatar.width = 100;
			this._p1Avatar.height = 100;
			this._p1Avatar.x = 30;
			this._p1Avatar.y = this._header.height + 20;
			
			this._p1Name.validate();
			this._p1Name.x = 140;
			this._p1Name.y = this._header.height + 20;
			
			this._p1Rank.validate();
			this._p1Rank.x = 140;
			this._p1Rank.y = this._header.height + 40;
			
			this._p1Rating.validate();
			this._p1Rating.x = 140;
			this._p1Rating.y = this._header.height + 60;
		
			this._p1RdyToggle.validate();
			this._p1RdyToggle.x = this._header.width/2 - this._p1RdyToggle.width;
			this._p1RdyToggle.y = this._header.height + 20 + 50 - this._p1RdyToggle.height/2; //50 = avatar.height/2
			
			this._p1RdyLabel.validate();
			this._p1RdyLabel.x = this._p1RdyToggle.x + this._p1RdyLabel.width/2;
			this._p1RdyLabel.y = this._p1RdyToggle.y - 20;
			
			/**
			 * Player 2 box
			 */	
			this._p2Avatar.validate();
			this._p2Avatar.width = 100;
			this._p2Avatar.height = 100;
			this._p2Avatar.x = 30;
			this._p2Avatar.y = this._header.height + 140;
			
			this._p2Name.validate();
			this._p2Name.x = 140;
			this._p2Name.y = this._header.height + 140;
			
			this._p2Rank.validate();
			this._p2Rank.x = 140;
			this._p2Rank.y = this._header.height + 160;
			
			this._p2Rating.validate();
			this._p2Rating.x = 140;
			this._p2Rating.y = this._header.height + 180;
			
			this._p2RdyToggle.validate();
			this._p2RdyToggle.x = this._header.width/2 - this._p2RdyToggle.width;
			this._p2RdyToggle.y = this._header.height + 140 + 50 - this._p2RdyToggle.height/2;
			
			this._p2RdyLabel.validate();
			this._p2RdyLabel.x = this._p2RdyToggle.x + this._p2RdyLabel.width/2;
			this._p2RdyLabel.y = this._p2RdyToggle.y - 20;
		}
		
		private function left_triggeredHandler():void
		{
			//leave room
			if(Data.derp == _host)
			{
				//destroy room
				trace("Room destroyed");
				this.dispatchEventWith(SHOW_ARENA);
			}
			else
			{
				//only leave
				trace("Player 2 left");
				this.dispatchEventWith(SHOW_ARENA);
			}
		}
		
		/*private function rdy_triggeredHandler():void
		{
			if(Data.derp == _p1)
			{
				trace(Data.derp + " is ready!");
				_p1rdy = true;
			}
			else
			{
				trace("Player 2" + "is ready!");
				_p2rdy = true;
			}
		}*/
		
		private function start_triggeredHandler():void
		{
			/*if(_p1rdy && _p2rdy)
			{
				trace("Both players rdy\n Game Starts!");
				
			}
			else
			{
				if(!_p1rdy)
					trace(Data.derp + " is not ready!");
			}*/
			if(this._p1RdyToggle.isSelected && this._p2RdyToggle.isSelected)
			{
				trace("Both players rdy \n   GAME STARTS!");
				//stage.dispatchEventWith("startGame");
				Data.hitStartGame = true;
				//trace("Base: " + this.base);
				//stage.removeChildren();
				//stage.dispose();
				//var _gs:GameState = new GameState();
				//stage.addChild(_gs);
			}
			else
			{
				trace("Both players must be ready for the game to begin!");
			}
		}
		
		private function avatar_triggeredHandler():void
		{
			//go to player avatar
		}
	}
}