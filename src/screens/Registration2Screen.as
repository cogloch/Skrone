package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	
	import functional.Data;
	
	import starling.events.Event;
	
	[Event(name="showTitle", type="starling.events.Event")]
	[Event(name="showRegistration1", type="starling.events.Event")]
	[Event(name="showRegistration3", type="starling.events.Event")]
	
	
	public class Registration2Screen extends Screen
	{
		public const SHOW_REGISTRATION1:String = "showRegistration1";
		public const SHOW_REGISTRATION3:String = "showRegistration3";
		public const SHOW_TITLE:String = "showTitle";
		
		public function Registration2Screen()
		{
			super();
		}
		
		private var _backButton:Button;
		private var _nextButton:Button;
		private var _TosseFederationFaction:Button;
		private var _KorbisRepublicFaction:Button;
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			this._header = new Header();
			this._header.title = "Choose Your Side";
			this.addChild(this._header);
			
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._nextButton = new Button();
			this._nextButton.label = "Complete";
			this._nextButton.addEventListener(Event.TRIGGERED, next_triggeredHandler);
			this.addChild(this._nextButton);
			
			this._TosseFederationFaction = new Button();
			this._TosseFederationFaction.label = "Tosse Federation";
			this._TosseFederationFaction.isToggle = true;
			this._TosseFederationFaction.addEventListener(Event.TRIGGERED, tosse_triggeredHandler);
			this.addChild(this._TosseFederationFaction);
			
			this._KorbisRepublicFaction = new Button();
			this._KorbisRepublicFaction.label = "The Korbis Republic";
			this._KorbisRepublicFaction.isToggle = true;
			this._KorbisRepublicFaction.addEventListener(Event.TRIGGERED, korbis_triggeredHandler);
			this.addChild(this._KorbisRepublicFaction);
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._backButton.validate();
			this._backButton.x = 40;
			this._backButton.y = 550;
			
			this._nextButton.validate();
			this._nextButton.x = 700;
			this._nextButton.y = 550;
			
			this._TosseFederationFaction.width = 350;
			this._TosseFederationFaction.height = 300
			this._TosseFederationFaction.validate();
			this._TosseFederationFaction.x = 40;
			this._TosseFederationFaction.y = 100;
			
			this._KorbisRepublicFaction.width = 350;
			this._KorbisRepublicFaction.height = 300;
			this._KorbisRepublicFaction.validate();
			this._KorbisRepublicFaction.x = 420;
			this._KorbisRepublicFaction.y = 100;
		}
		
		private function back_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_TITLE);
		}
		
		private function next_triggeredHandler():void
		{
			//keep info/create new account
			//check if account exists
			//data is not final, hence the player may edit it 
			if(Data.regiFaction!="0")
				this.dispatchEventWith(SHOW_REGISTRATION1);
			else
				trace("nothing selected");
		}
		
		private function tosse_triggeredHandler():void
		{
			//chose tosse
			//if korbis is selected, deselect
			trace("Tosse");
			if(this._KorbisRepublicFaction.isSelected)
			{
				trace("Deselected Korbis");
				this._KorbisRepublicFaction.isSelected = false;
			}
			Data.regiFaction = "Tosse";
		}
		
		private function korbis_triggeredHandler():void
		{
			//chose korbis
			//if tosse is selected, deselect
			trace("Korbis");
			if(this._TosseFederationFaction.isSelected)
			{
				trace("Deselected Tosse");
				this._TosseFederationFaction.isSelected = false;	
			}
			Data.regiFaction = "Korbis";
		}
	}
}