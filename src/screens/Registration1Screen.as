package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.TextInput;
	
	import functional.Data;
	
	import playerio.*;
	
	import starling.core.Starling;
	import starling.display.Stage;
	import starling.events.Event;
	
	[Event(name="showRegistration2", type="starling.events.Event")]
	[Event(name="showRegistration3", type="starling.events.Event")]
	
	public class Registration1Screen extends Screen
	{
		public const SHOW_REGISTRATION2:String = "showRegistration2";
		public const SHOW_REGISTRATION3:String = "showRegistration3";
		
		//public var faction:String = Data.regiFaction;
		
		public function Registration1Screen()
		{
			//TODO: implement function
			super();
		}
		
		private var _user:TextInput;
		private var _pass:TextInput;
		private var _email:TextInput;
		private var _name:TextInput;
		
		private var _nextButton:Button;
		private var _backButton:Button;
		
		private var _userLabel:Label;
		private var _passLabel:Label;
		private var _emailLabel:Label;
		private var _nameLabel:Label;
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			this._header = new Header();
			this._header.title = "Account Details";
			this.addChild(this._header);
			
			this._userLabel = new Label();
			this._userLabel.text = "Username";
			this.addChild(this._userLabel);
			
			this._passLabel = new Label();
			this._passLabel.text = "Password";
			this.addChild(this._passLabel);
			
			this._emailLabel = new Label();
			this._emailLabel.text = "Email";
			this.addChild(this._emailLabel);
			
			this._nameLabel = new Label();
			this._nameLabel.text = "In-game Name";
			this.addChild(this._nameLabel);
			
			this._user = new TextInput();
			this.addChild(this._user);
			
			this._pass = new TextInput();
			this.addChild(this._pass);
			
			this._email = new TextInput();
			this.addChild(this._email);
			
			this._name = new TextInput();
			this.addChild(this._name);
			
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._nextButton = new Button();
			this._nextButton.label = "Next";
			this._nextButton.addEventListener(Event.TRIGGERED, next_triggeredHandler);
			this.addChild(this._nextButton);
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._backButton.validate();
			this._backButton.x = 40;
			this._backButton.y = 560;
			
			this._nextButton.validate();
			this._nextButton.x = 700;
			this._nextButton.y = 560;
			
			this._userLabel.validate();
			this._userLabel.x = 340;
			this._userLabel.y = 100;
			
			this._user.validate();
			this._user.x = 340;
			this._user.y = 130;
			
			this._passLabel.validate();
			this._passLabel.x = 340;
			this._passLabel.y = 170;
			
			this._pass.validate();
			this._pass.x = 340;
			this._pass.y = 200;
			
			this._emailLabel.validate();
			this._emailLabel.x = 340;
			this._emailLabel.y = 240;
			
			this._email.validate();
			this._email.x = 340;
			this._email.y = 270;
			
			this._nameLabel.validate();
			this._nameLabel.x = 340;
			this._nameLabel.y = 310;
			
			this._name.validate();
			this._name.x = 340;
			this._name.y = 340;
		}
		
		private function back_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_REGISTRATION2);
		}
		
		private function handleConnect(client:Client):void
		{
			trace("Sucessfully connected to player.io");
			//Set developmentsever (Comment out to connect to your server online)
			client.multiplayer.developmentServer = "127.0.0.1:8184";
		}
		
		private function handleError(error:PlayerIOError):void{
			trace("Got", error)	
		}
		
		private function next_triggeredHandler():void
		{
			//faction set for created account
			//data is final
			trace("Complete");
			/*PlayerIO.connect(
				Starling.current.nativeStage,								//Referance to stage
				"skrone-wiq21wmxkueja2yp64kia",			//Game id (Get your own at playerio.com. 1: Create user, 2:Goto admin pannel, 3:Create game, 4: Copy game id inside the "")
				"public",							//Connection id, default is public
				"123",								//Username
				"",									//User auth. Can be left blank if authentication is disabled on connection
				null,								//Current PartnerPay partner.
				handleConnect,						//Function executed on successful connect
				handleError							//Function executed if we recive an error
			);   */
			//Register a user with QuickConnect for Simple Users
			PlayerIO.quickConnect.simpleRegister(
				Starling.current.nativeStage, 
				"skrone-wiq21wmxkueja2yp64kia", 
				_user.text, 
				_pass.text, 
				_email.text, 
				null, // captcha key, if captcha is used
				null, // captcha value, if captcha is used
				{faction: Data.regiFaction}, // any additional data you want
				null,
				function(client:Client):void{
					trace("Registration complete");
					
				}, function(e:PlayerIORegistrationError):void{
					trace("Error: ", e)
				}
			);
			this.dispatchEventWith(SHOW_REGISTRATION3);
			//do create account stuff
			//initialize stuff
		}
	}
}