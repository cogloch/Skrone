package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	import functional.Data;
	
	[Event(name="showMainMenu", type="starling.events.Event")]
	[Event(name="showLeaderBoard", type="starling.events.Event")]
	public class ProfileScreen extends Screen
	{
		public const SHOW_MAIN_MENU:String = "showMainMenu";
		public const SHOW_LEADERBOARD:String = "showLeaderboard";
		
		public function ProfileScreen()
		{
			super();
		}
		
		private var _header:Header;
		
		private var _backButton:Button;
		private var _leaderBoard:Button;
		private var _avatar:Button;
		
		//private var _comingSoon:Label;
		
		private var _name:Label;
		private var _rank:Label;
		private var _faction:Label;
		private var _elo:Label;
		private var _achiNo:Label;
		private var _credits:Label;
		
		private var _victories:Label;
		private var _losses:Label;
		private var _shipsDestroyed:Label;
		private var _shipsLost:Label;
		private var _buildingsDestroyed:Label;
		private var _buildingsLost:Label;
		
		private var _achievements:List;
		
		override protected function initialize():void
		{
			this._backButton = new Button();
			this._backButton.label = "Back to Main Menu";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._leaderBoard = new Button();
			this._leaderBoard.label = "Leaderboard";
			this._leaderBoard.addEventListener(Event.TRIGGERED, leaderboard_triggeredHandler);
			this.addChild(this._leaderBoard);
			
			this._avatar = new Button();
			//change avatar
			this.addChild(this._avatar);
			
			this._name = new Label();
			this._name.text = Data.derp;
			this.addChild(this._name);
			
			this._rank = new Label();
			this._rank.text = "Rank: " + "Academy Recruit";
			this.addChild(this._rank);
			
			this._faction = new Label();
			this._faction.text = "Faction: " + "Tosse Federation";
			this.addChild(this._faction);
			
			this._elo = new Label();
			this._elo.text = "ELO Rating: " + "1543";
			this.addChild(this._elo);
			
			this._achiNo = new Label();
			this._achiNo.text = "9 " + "Achievements";
			this.addChild(this._achiNo);
			
			this._credits = new Label();
			this._credits.text = "Credits: " + "35713";
			this.addChild(this._credits);
			
			this._victories = new Label();
			this._victories.text = "Battels Won: " + "21";
			this.addChild(this._victories);
			
			this._losses = new Label();
			this._losses.text = "Battles Lost: " + "8";
			this.addChild(this._losses);
			
			this._shipsDestroyed = new Label();
			this._shipsDestroyed.text = "Ships Destroyed: " + "563";
			this.addChild(this._shipsDestroyed);
			
			this._shipsLost = new Label();
			this._shipsLost.text = "Ships Lost: " + "495";
			this.addChild(this._shipsLost);
			
			this._buildingsDestroyed = new Label();
			this._buildingsDestroyed.text = "Structures Destroyed: " + "127";
			this.addChild(this._buildingsDestroyed);
			
			this._buildingsLost = new Label();
			this._buildingsLost.text = "Structures Lost: " + "98";
			this.addChild(this._buildingsLost);
			
			this._achievements = new List;
			this.addChild(this._achievements);
			
			this._header = new Header();
			this._header.title = "Profile";
			this.addChild(this._header);
			this._header.leftItems = new <DisplayObject>
			[
				this._backButton
			];
			this._header.rightItems = new <DisplayObject>
			[
				this._leaderBoard
			];
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._avatar.validate();
			this._avatar.x = 20;
			this._avatar.width = 100;
			this._avatar.height = 100;
			this._avatar.y = this._header.height + 20;
			
			this._name.validate();
			this._name.x = 150;
			this._name.y = this._header.height + 40;
			
			this._rank.validate();
			this._rank.x = 150;
			this._rank.y = this._header.height + 60;
			
			this._faction.validate();
			this._faction.x = 150;
			this._faction.y = this._header.height + 80;
			
			this._elo.validate();
			this._elo.x = 150;
			this._elo.y = this._header.height + 100;
			
			this._achiNo.validate();
			this._achiNo.x = 380;
			this._achiNo.y = 280;
			
			this._credits.validate();
			this._credits.x = 150;
			this._credits.y = this._header.height + 120;
			
			this._victories.validate();
			this._victories.x = 500;
			this._losses.y = this._header.height + 40;
			
			this._losses.validate();
			this._losses.x = 500;
			this._losses.y = this._header.height + 60;
			
			this._shipsDestroyed.validate();
			this._shipsDestroyed.x = 500;
			this._shipsDestroyed.y = this._header.height + 80;
			
			this._shipsLost.validate();
			this._shipsLost.x = 500;
			this._shipsLost.y = this._header.height + 100;
			
			this._buildingsDestroyed.validate();
			this._buildingsDestroyed.x = 500;
			this._buildingsDestroyed.y = this._header.height + 120;
			
			this._buildingsLost.validate();
			this._buildingsLost.x = 500;
			this._buildingsLost.y = this._header.height + 140;
			
			this._achievements.validate();
			this._achievements.width = 600;
			this._achievements.x = 20;
			this._achievements.y = 300;
		}
		
		private function back_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_MAIN_MENU);
		}
		
		private function leaderboard_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_LEADERBOARD);
		}
	}
}





