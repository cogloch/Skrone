package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.TabBar;
	import feathers.controls.Screen;
	
	import functional.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class GameOverScreen extends Screen
	{
		private var _header:Header;
		
		private var _tabs:TabBar;
		
		private var _exit:Button;
		
		public var won:Boolean = false;
		
		private var victoryRibbon:Image;
		
		/**
		 * Players Tab
		 */
		public var p1Avatar:Button;
		public var p2Avatar:Button;	
		public var p1Name:TextField;
		public var p2Name:TextField;
		
		public var p1Elo:TextField;
		public var p2Elo:TextField;
		public var p1EloChange:TextField;
		public var p2EloChange:TextField;
		
		private var achiLabel:TextField;
		public var achiEarned:TextField;
		
		/**
		 * Stats Tab
		 */
		public function GameOverScreen()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			_header = new Header();
			if(won==true)
			{
				_header.title = "VICTORY";
			} else
			{
				_header.title = "YOU LOST";
			}
			_header.width = stage.width;
			addChild(_header);
		}
	}
}