package screens
{
	import feathers.controls.Button;
	import feathers.controls.Screen;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	[Event(name="showArena", type="starling.events.Event")]
	[Event(name="showMissions", type="starling.events.Event")]
	[Event(name="showMainMenu", type="starling.events.Event")]
	[Event(name="showMap", type="starling.events.Event")]
	
	public class PlayScreen extends Screen
	{
		public const SHOW_ARENA:String = "showArena";
		public const SHOW_MISSIONS:String = "showMissions";
		public const SHOW_MAIN_MENU:String = "showMainMenu";
		public const SHOW_MAP:String = "showMap";
		
		public function PlayScreen()
		{
			super();
		}
		
		private var _backButton:Button;
		private var _arenaButton:Button;
		private var _missionsButton:Button;
		private var _mapButton:Button;
		
		override protected function initialize():void
		{
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);
			
			this._arenaButton = new Button();
			this._arenaButton.label = "Arena";
			this._arenaButton.addEventListener(Event.TRIGGERED, arena_triggeredHandler);
			this.addChild(this._arenaButton);
			
			this._missionsButton = new Button();
			this._missionsButton.label = "Challenges";
			this._missionsButton.addEventListener(Event.TRIGGERED, missions_triggeredHandler);
			this.addChild(this._missionsButton);
			
			this._mapButton = new Button();
			this._mapButton.label = "Galaxy Map";
			this._mapButton.addEventListener(Event.TRIGGERED, map_triggeredHandler);
			this.addChild(this._mapButton);
		}
		
		override protected function draw():void
		{
			this._backButton.validate();
			this._backButton.x = 700;
			this._backButton.y = 560;
			
			this._arenaButton.validate();
			this._arenaButton.width = 550;
			this._arenaButton.height = 130;
			this._arenaButton.x = 100;
			this._arenaButton.y = 90;
			
			this._missionsButton.validate();
			this._missionsButton.width = 550;
			this._missionsButton.height = 130;
			this._missionsButton.x = 100;
			this._missionsButton.y = 270;
			
			this._mapButton.validate();
			this._mapButton.width = 550;
			this._mapButton.height = 130;
			this._mapButton.x = 100;
			this._mapButton.y = 450;
		}
		
		private function back_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_MAIN_MENU);
		}
		
		private function arena_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_ARENA);
		}
		
		private function missions_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_MISSIONS);
		}
		
		private function map_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_MAP);
		}
	}
}