package screens
{
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.Callout;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.data.ListCollection;
	
	import flash.text.TextFormat;
	
	import functional.Assets;
	import functional.Data;
	import functional.SkroneTheme;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	[Event(name="showPlay",type="starling.events.Event")]
	[Event(name="showHelp",type="starling.events.Event")]
	[Event(name="showHangar",type="starling.events.Event")]
	[Event(name="showSocial",type="starling.events.Event")]
	[Event(name="showFaction",type="starling.events.Event")]
	//[Event(name="showAlliance",type="starling.events.Event")]
	[Event(name="showProfile",type="starling.events.Event")]
	[Event(name="showTitle",type="starling.events.Event")]
	//[Event(name="fullScreen",type="starling.events.Event")]
	[Event(name="showStore", type="starling.events.Event")]
	[Event(name="showLeaderboard", type="starling.events.Event")]
	
	public class MainMenuScreen extends Screen
	{
		[Embed(source="/../assets/icons/Zoom.png")]
		private static const ZOOM_ICON:Class;
		
		[Embed(source="/../assets/icons/Captain.png")] //pull player avatar from db
		private static const AVATAR_ICON:Class;
		
		public const SHOW_PLAY:String = "showPlay";
		public const SHOW_HELP:String = "showHelp"; //question mark button
		public const SHOW_HANGAR:String = "showHangar";
		public const SHOW_SOCIAL:String = "showSocial";
		public const SHOW_FACTION:String = "showFaction";
		//public const SHOW_ALLIANCE:String = "showAlliance"; 
		public const SHOW_PROFILE:String = "showProfile"; //on avatar
		public const SHOW_TITLE:String = "showTitle"; //log out
		public const SHOW_STORE:String = "showStore";
		public const SHOW_LEADERBOARD:String = "showLeaderboard";
		
		private var mainMenuProfileBarIcons:Image;
		
		public function MainMenuScreen()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		//private var _backButton:Button;
		private var _buttonGroup:ButtonGroup;
		
		//private var _fullScreenButton:Button;
		private var _profileButton:Button; //avatar
		private var _logoutButton:Button;
		
		private var _profileNameButton:Button;
		private var _rankButton:Button;
		private var _factionButton:Button;
		private var _eloRatingButton:Button;
		private var _creditsButton:Button;
		private var _reputationButton:Button;
		private var _premiumPtsButton:Button;
		private var _battleSupplyButton:Button;
		
		//private var _fullScreenIcon:Image;
		private var _avatarIcon:Image;
		
		private var _updates:List;
		
		private var imgsContainer:Sprite;
		
		override protected function initialize():void
		{
			this.imgsContainer = new Sprite();
			this.mainMenuProfileBarIcons = new Image(Assets.getMenuAtlas().getTexture("mainMenuProfileBarIcons"));
			this.imgsContainer.addChild(mainMenuProfileBarIcons);
			this.imgsContainer.y = -40;
			this.addChild(this.imgsContainer);
			
			this._profileNameButton = new Button();
			this._profileNameButton.label = Data.derp; //pull name
			this._profileNameButton.addEventListener(Event.TRIGGERED, profile_triggeredHandler);
			this._profileNameButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._profileNameButton);
			
			this._rankButton = new Button();
			this._rankButton.label = "Rank: " + "Academy Recruit"; //pull rank
			this._rankButton.addEventListener(Event.TRIGGERED, profile_triggeredHandler);
			this._rankButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._rankButton);
			
			this._factionButton = new Button();
			this._factionButton.label = "Faction: " + "Tosse Federation"; //pull faction
			this._factionButton.addEventListener(Event.TRIGGERED, faction_triggeredHandler);
			this._factionButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._factionButton);
			
			this._eloRatingButton = new Button();
			this._eloRatingButton.label = "ELO Rating: " + "1543"; //pull elo rating
			this._eloRatingButton.addEventListener(Event.TRIGGERED, leaderboard_triggeredHandler);
			this._eloRatingButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._eloRatingButton);
			
			this._creditsButton = new Button();
			this._creditsButton.label = "Credits: " + "35713"; //pull credits
			this._creditsButton.addEventListener(Event.TRIGGERED, store_triggeredHandler);
			this._creditsButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._creditsButton);
			
			this._reputationButton = new Button();
			this._reputationButton.label = "Reputation: " + "12" + "/" + "100"; //pull current xp and maximum xp
			this._reputationButton.addEventListener(Event.TRIGGERED, profile_triggeredHandler);
			this._reputationButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._reputationButton);
			
			this._premiumPtsButton = new Button();
			this._premiumPtsButton.label = "Premium Pts: " + "275"; //pull premium pts
			this._premiumPtsButton.addEventListener(Event.TRIGGERED, store_triggeredHandler);
			this._premiumPtsButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			this.addChild(this._premiumPtsButton);
			
			this._battleSupplyButton = new Button();
			this._battleSupplyButton.label = "Battle Supply: " + "3" + "/" + "5"; //pull current bs and maximum bs
			this._battleSupplyButton.addEventListener(Event.TRIGGERED, store_triggeredHandler);
			this._battleSupplyButton.nameList.add(SkroneTheme.NO_BG_BUTTON);
			//this._battleSupplyButton.defaultLabelProperties.textFormat = new TextFormat {"Arial", 18, 0xffffff};
			this.addChild(this._battleSupplyButton);
			
			this._buttonGroup = new ButtonGroup();
			this._buttonGroup.height = 350;
			this._buttonGroup.dataProvider = new ListCollection(
			[
				{label: "Play", triggered:play_triggeredHandler },
				{label: "Hangar", triggered:hangar_triggeredHandler },
				{label: "Faction", triggered:faction_triggeredHandler },
				//{label: "Alliance", triggered:alliance_triggeredHandler },
				{label: "Social", triggered:social_triggeredHandler },
				{label: "Store", triggered:store_triggeredHandler },
				{label: "Help", triggered:help_triggeredHandler },
			]);
			//this._buttonGroup.buttonProperties.textFormat = new TextFormat("Arial", 50, 0xfffff);
			//_buttonGroup.buttonFactory
			this.addChild(this._buttonGroup);		
			
			/*this._fullScreenIcon = new Image(Texture.fromBitmap(new ZOOM_ICON()));
			//this._fullScreenIcon.scaleX = this._fullScreenIcon.scaleY = this.dpiScale;
			
			this._fullScreenButton = new Button;
			this._fullScreenButton.defaultIcon = this._fullScreenIcon;
			this._fullScreenButton.width = 0;
			this._fullScreenButton.height = 0;
			this._fullScreenButton.iconPosition = "center";
			this._fullScreenButton.hoverLabelProperties = "Full Screen";		
			this._fullScreenButton.addEventListener(Event.TRIGGERED, fullScreen_triggeredHandler);
			//Callout.show(DisplayObject(recommended), this._fullScreenButton, Callout.DIRECTION_UP);
			//this.addChild(_fullScreenButton);*/
			
			this._avatarIcon = new Image(Texture.fromBitmap(new AVATAR_ICON()));
			
			this._profileButton = new Button();
			this._profileButton.defaultIcon = this._avatarIcon;
			this._profileButton.hoverLabelProperties = "Your Profile";
			this._profileButton.width = 100;
			this._profileButton.height = 100;
			this._profileButton.iconPosition = "center";
			this._profileButton.addEventListener(Event.TRIGGERED, profile_triggeredHandler);
			this.addChild(_profileButton);
			
			this._logoutButton = new Button();
			this._logoutButton.label = "Log Out";
			this._logoutButton.addEventListener(Event.TRIGGERED, logout_triggeredHandler);
			this.addChild(_logoutButton);
			
			this._updates = new List();
			this._updates.isSelectable = true;
			this._updates.scrollerProperties.hasElasticEdges = false;
			this.addChild(this._updates);
		}
		
		override protected function draw():void
		{
			this._profileNameButton.validate();
			this._profileNameButton.x = 140;
			this._profileNameButton.y = 20;
			
			this._rankButton.validate();
			this._rankButton.x = 140;
			this._rankButton.y = 50;
			
			this._factionButton.validate();
			this._factionButton.x = 140;
			this._factionButton.y = 80;
			
			this._eloRatingButton.validate();
			this._eloRatingButton.x = 140;
			this._eloRatingButton.y = 110;
			
			this._creditsButton.validate();
			this._creditsButton.x = 550;
			this._creditsButton.y = 20;
			
			this._reputationButton.validate();
			this._reputationButton.x = 550;
			this._reputationButton.y = 50;
			
			this._premiumPtsButton.validate();
			this._premiumPtsButton.x = 550;
			this._premiumPtsButton.y = 80;
			
			this._battleSupplyButton.validate();
			this._battleSupplyButton.x = 550;
			this._battleSupplyButton.y = 110;
			
			this._buttonGroup.validate();
			this._buttonGroup.x = 20;
			this._buttonGroup.y = 210;
			
			this._logoutButton.validate();
			this._logoutButton.x = 700;
			this._logoutButton.y = 560;
			
			/*this._fullScreenButton.validate();
			this._fullScreenButton.x = 200;
			this._fullScreenButton.y = 760;*/
			
			this._profileButton.validate();
			this._profileButton.x = 20;
			this._profileButton.y = 20;
			
			this._updates.validate();
			this._updates.x = 500;
			this._updates.y = 210;
		}
		
		private function play_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_PLAY);
			//sound
			trace("Play");
		}
		
		private function hangar_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_HANGAR);
			//sound
		}
		
		private function faction_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_FACTION);
			//sound
		}
		
		/*private function alliance_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_ALLIANCE);
			//sound
		}*/
		
		private function social_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_SOCIAL);
			//sound
		}
		
		private function help_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_HELP);
			//sound
			//send to the Skrone Wiki (skrone.wikia.com)
			//or make a new screen "About"
			//which contains a cool something something that says the amount of time spent on this although nah
			//will leave screen Help for now
		}
		
		private function store_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_STORE);
			//sound
		}
		
		private function fullScreen_triggeredHandler(event:Event):void
		{
			//fullscreen
			trace("fullscreen");
			//sound
		}
		
		private function profile_triggeredHandler(event:Event):void
		{
			//click on avatar -> profile
			this.dispatchEventWith(SHOW_PROFILE);
			//sound
		}
		
		private function logout_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_TITLE);
		}
		
		private function leaderboard_triggeredHandler(event:Event):void
		{
			this.dispatchEventWith(SHOW_LEADERBOARD);
		}
		
	}
}