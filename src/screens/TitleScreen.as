package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.TabBar;
	import feathers.controls.TextInput;
	import feathers.data.ListCollection;
	
	import functional.Assets;
	import functional.Data;
	import functional.Login;
	
	import playerio.*;
	
	import starling.display.Image;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.textures.Texture;
	
	[Event(name="showRegistration2", type="starling.events.Event")]
	[Event(name="showMainMenu", type="starling.events.Event")]
	[Event(name="showLeaderboard", type="starling.events.Event")]
	[Event(name="attemptLogin", type="starling.events.Event")]
	
	public class TitleScreen extends Screen
	{
		[Embed(source="/../assets/interface/titleScreen/loginBg.png")]
		private static const LOGIN_BG:Class;
		
		[Embed(source="/../assets/interface/titleScreen/titleTabsBg.png")]
		private static const TABS_BG:Class;
		
		[Embed(source="/../assets/interface/titleScreen/titleTitle.png")]
		private static const TITLE_IMG:Class;
		
		public const SHOW_REGISTRATION2:String = "showRegistration2";
		public const SHOW_MAIN_MENU:String = "showMainMenu";
		public const SHOW_LEADERBOARD:String = "showLeaderboard";
		public const ATTEMPT_LOGIN:String = "attemptLogin";
		
		public const AUTH_FAIL:String = "Try again";
		
		public var _username:String;
		public var _auth:String;
		
		private static var _client:Client;
		private static var _playerObject:DatabaseObject;
		
		private static var _login:Login;
		
		//[Embed(source="/../assets/images/bigTitle.png")]
		//private static const BIG_TITLE:Class;
		
		public function TitleScreen()
		{
			super();
		}
		
		private var _registrationButton:Button;
		private var _loginButton:Button;
		
		private var _pass:TextInput;
		private var _user:TextInput;
		
		private var _userLabel:Label;
		private var _passLabel:Label;
		private var _fail:Label;
		
		//private var _bigTitle:Image;
		
		private var _titleInfo:TabBar;
		
		private var _header:Header;
		
		private var _loginBg:Image;
		private var _tabsBg:Image;
		private var _titleImg:Image;
		
		//private var _title:Label;
		
		override protected function initialize():void
		{
			this._loginBg = new Image(Assets.getMenuAtlas().getTexture("loginBg"));
			this._loginBg.y = -51;
			this.addChild(_loginBg);
			
			this._tabsBg = new Image(Assets.getMenuAtlas().getTexture("titleTabsBg"));
			this._tabsBg.y = -51;
			this._tabsBg.x = -9;
			this.addChild(_tabsBg);
			
			this._titleImg = new Image(Assets.getMenuAtlas().getTexture("titleTitle"));
			this._titleImg.y = -60;
			this._titleImg.x = 0;
			this.addChild(this._titleImg);
			
			this._fail = new Label;
			this._fail.text = AUTH_FAIL;
			
			this._userLabel = new Label();
			this._userLabel.text = "Username";
			this.addChild(this._userLabel);
			
			this._passLabel = new Label();
			this._userLabel.text = "Password";
			this.addChild(this._passLabel);
			
			this._user = new TextInput();
			this.addChild(this._user);
			
			this._pass = new TextInput();
			this.addChild(this._pass);
			
			this._loginButton = new Button();
			this._loginButton.label = "Login";
			this._loginButton.addEventListener(Event.TRIGGERED, login_triggeredHandler);
			this.addChild(this._loginButton);
			
			this._registrationButton = new Button();
			this._registrationButton.label = "Register";
			this._registrationButton.addEventListener(Event.TRIGGERED, register_triggeredHandler);
			//this._registrationButton.width = 100;
			//this._registrationButton.height = 100;
			this.addChild(this._registrationButton);
			
			this._titleInfo = new TabBar();
			this._titleInfo.dataProvider = new ListCollection(
			[
				{ label: "About" },
				{ label: "News" },
				{ label: "World Status" },
				{ label: "Top Players" },
			]);
			this._titleInfo.addEventListener(Event.CHANGE, titleInfo_changeHandler);
			this.addChild(this._titleInfo);
			
			this._header = new Header();
			this._header.title = "*********    Pre-Alpha Version    *********    Pre-Alpha Version    *********    Pre-Alpha Version    *********";
			this.addChild(this._header);
		}
		
		override protected function draw():void
		{
			/*this._titleImg.x = 100;
			this._titleImg.y = 100;*/
			this._fail.validate();
			this._fail.x = 635;
			this._fail.y = 520;
			
			this._user.validate();
			this._user.text = "Username";
			this._user.height = this._user.height + 3;
			this._user.x = 600;
			this._user.y = 315;
			
			this._pass.validate();
			this._pass.text = "Password";
			this._pass.height = this._pass.height + 3;
			this._pass.x = 600;
			this._pass.y = 365;
			
			this._loginButton.validate();
			this._loginButton.width = this._user.width;
			this._loginButton.x = 600;
			this._loginButton.y = 435;
			
			this._registrationButton.validate();
			this._registrationButton.width = this._user.width;
			this._registrationButton.x = 600;
			this._registrationButton.y = 475;
			
			this._userLabel.validate();
			this._userLabel.x = 400;
			this._passLabel.y = 290;
			
			this._passLabel.x = 600;
			this._passLabel.y = 350;
			this._passLabel.validate();
			
			this._titleInfo.validate();
			this._titleInfo.width = 450;
			this._titleInfo.height = 50;
			this._titleInfo.x = 50;
			this._titleInfo.y = 280;
			//this._titleInfo.
			
			this._header.width = this.actualWidth;
			this._header.validate();
			//this._header.height = 80;
			//this._header.paddingBottom = 1;
			//this._header.paddingTop = 1;
		}
		
		private function login_triggeredHandler():void
		{
			//check if user/pass match
			_username = _user.text;
			_auth = _pass.text;
			if((_username=="test" && _auth == "")||(_username=="Pandam0nium13" && _auth == ""))
			{
				Data.derp = _username;
				trace("Login succ");
				this.dispatchEventWith(SHOW_MAIN_MENU);
			}
			else
			{
				Data._usr = _username; 
				Data._pss = _auth;
				
				Data.hitLogin = true;
				
				//dispatchEventWith("faLogin", true);
				
				/*stage.addChild(_login);
				if(loginData._succ == true)
				{
					trace("Huehueheu");
					Data.derp = _username;
					this.dispatchEventWith(SHOW_MAIN_MENU);
				}
				else
				{
					trace("SCHNITZEL!"+"The username "+_username+" and passoword "+_auth+" are SCHEISE!");
					this._user.text = null;
					this._pass.text = null;
					this.addChild(this._fail);
				}*/
				
			}
			/*if(_username != "")
			{
				//this.dispatchEventWith(ATTEMPT_LOGIN);
				dispatchEvent(new Event("ATTEMPT_LOGIN"));
			}*/
		}
		
		private function register_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_REGISTRATION2);	
		}
		
		private function titleInfo_changeHandler():void
		{
			//tabs
		}
	}
}