package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	
	import starling.events.Event;
	
	[Event(name="showPlay", type="starling.events.Event")]
	[Event(name="showLobby", type="starling.events.Event")]
	[Event(name="showGamesList", type="starling.events.Event")]
	
	public class ArenaScreen extends Screen
	{
		public const SHOW_PLAY:String = "showPlay";
		public const SHOW_LOBBY:String = "showLobby";
		public const SHOW_GAMES_LIST:String = "showGamesList";
		
		public function ArenaScreen()
		{
			super();
		}
		
		private var _backButton:Button;
		private var _quickGameButton:Button;
		private var _gamesListButton:Button;
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			this._header = new Header();
			this._header.title = "Arena";
			this.addChild(this._header);
			
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(Event.TRIGGERED, back_triggeredHandler);
			this.addChild(this._backButton);	
			
			this._quickGameButton = new Button();
			this._quickGameButton.label = "Quick Game";
			this._quickGameButton.addEventListener(Event.TRIGGERED, quickGame_triggeredHandler);
			this.addChild(this._quickGameButton);
			
			this._gamesListButton = new Button();
			this._gamesListButton.label = "Games List";
			this._gamesListButton.addEventListener(Event.TRIGGERED, gamesList_triggeredHandler);
			this.addChild(this._gamesListButton);
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._backButton.validate();
			this._backButton.x = 700;
			this._backButton.y = 560;
			
			this._quickGameButton.validate();
			this._quickGameButton.width = 550;
			this._quickGameButton.height = 130;
			this._quickGameButton.x = 100;
			this._quickGameButton.y = 100;
			
			this._gamesListButton.validate();
			this._gamesListButton.width = 550;
			this._gamesListButton.height = 130;
			this._gamesListButton.x = 100;
			this._gamesListButton.y = 330;
			
		}
		
		private function back_triggeredHandler():void
		{
			
			this.dispatchEventWith(SHOW_PLAY);
		}
		
		private function quickGame_triggeredHandler():void
		{
			//matchmaking
			trace("Enter lobby");
			this.dispatchEventWith(SHOW_LOBBY);
		}
		
		private function gamesList_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_GAMES_LIST);
		}
	}
}