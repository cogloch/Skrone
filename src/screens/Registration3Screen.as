package screens
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	
	import starling.events.Event;
	
	[Event(name="showTitle", type="starling.events.Event")]
	
	public class Registration3Screen extends Screen
	{
		public const SHOW_TITLE:String = "showTitle";
		
		public function Registration3Screen()
		{
			super();
		}
		
		private var _completeButton:Button;
		
		private var _completeLabel:Label;
		
		private var _header:Header;
		
		override protected function initialize():void
		{
			this._header = new Header();
			this._header.title = "Account Creation Complete";
			this.addChild(this._header);
			
			this._completeLabel = new Label();
			this._completeLabel.text = "Check your email for confirmation!";
			this.addChild(this._completeLabel);
			
			this._completeButton = new Button();
			this._completeButton.label = "Return to login screen";
			this._completeButton.addEventListener(Event.TRIGGERED, complete_triggeredHandler);
			this.addChild(this._completeButton);
		}
		
		override protected function draw():void
		{
			this._header.validate();
			this._header.width = this.actualWidth;
			
			this._completeButton.validate();
			this._completeButton.width = 300;
			this._completeButton.height = 100;
			this._completeButton.x = 260;
			this._completeButton.y = 400;
			
			this._completeLabel.validate();
			this._completeLabel.x = 320;
			this._completeLabel.y = 200;
		}
		
		private function complete_triggeredHandler():void
		{
			this.dispatchEventWith(SHOW_TITLE);
		}
	}
}