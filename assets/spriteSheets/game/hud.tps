<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <false/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>hud.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>hud.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>hud.xml</filename>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../interface/hud/actionWheelInfo2.png</filename>
            <filename>../../interface/hud/actionWheelInfo.PNG</filename>
            <filename>../../interface/hud/recycleHover.PNG</filename>
            <filename>../../interface/hud/recycleUp.PNG</filename>
            <filename>../../interface/hud/repairHover.PNG</filename>
            <filename>../../interface/hud/repairUp.PNG</filename>
            <filename>../../interface/hud/buildHover.PNG</filename>
            <filename>../../interface/hud/buildUp.PNG</filename>
            <filename>../../interface/hud/attackHover.png</filename>
            <filename>../../interface/hud/attackUp.PNG</filename>
            <filename>../../interface/hud/inactive.PNG</filename>
            <filename>../../interface/hud/moveHover.PNG</filename>
            <filename>../../interface/hud/moveUp.PNG</filename>
            <filename>../../interface/hud/pointer.PNG</filename>
            <filename>../../interface/hud/hudBarDownBg.PNG</filename>
            <filename>../../interface/hud/tileHidden.png</filename>
            <filename>../../interface/hud/shieldMeter.PNG</filename>
            <filename>../../interface/hud/hpMeter.PNG</filename>
            <filename>../../interface/hud/moraleMeter.PNG</filename>
            <filename>../../interface/hud/tileSelected.png</filename>
            <filename>../../interface/hud/tileActionHover.PNG</filename>
            <filename>../../interface/hud/tileFriendly.png</filename>
            <filename>../../interface/hud/tileEnemy.PNG</filename>
            <filename>../../interface/hud/tile.png</filename>
            <filename>../../interface/hud/miniMap.PNG</filename>
            <filename>../../interface/hud/endTurn.PNG</filename>
            <filename>../../interface/hud/time.png</filename>
            <filename>../../interface/hud/resources.PNG</filename>
            <filename>../../interface/hud/camT2.png</filename>
            <filename>../../interface/hud/camTarget.PNG</filename>
            <filename>../../interface/hud/factoryHover.PNG</filename>
            <filename>../../interface/hud/domeHover.PNG</filename>
            <filename>../../interface/hud/cannonHover.PNG</filename>
            <filename>../../interface/hud/baseHover.PNG</filename>
            <filename>../../interface/hud/fortressHover.png</filename>
            <filename>../../interface/hud/domeUp.PNG</filename>
            <filename>../../interface/hud/fortressUp.PNG</filename>
            <filename>../../interface/hud/wallUp.PNG</filename>
            <filename>../../interface/hud/turretUp.PNG</filename>
            <filename>../../interface/hud/cannonUp.PNG</filename>
            <filename>../../interface/hud/baseUp.PNG</filename>
            <filename>../../interface/hud/monitorUp.png</filename>
            <filename>../../interface/hud/factoryUp.PNG</filename>
            <filename>../../interface/hud/oreMineUp.PNG</filename>
            <filename>../../interface/hud/reactorUp.PNG</filename>
            <filename>../../interface/hud/wallHover.png</filename>
            <filename>../../interface/hud/turretHover.PNG</filename>
            <filename>../../interface/hud/reactorHover.PNG</filename>
            <filename>../../interface/hud/oreMineHover.PNG</filename>
            <filename>../../interface/hud/monitorHover.PNG</filename>
            <filename>../../interface/hud/insufficientFunds.PNG</filename>
            <filename>../../interface/hud/tileRange.PNG</filename>
            <filename>../../interface/hud/unCloakHover.PNG</filename>
            <filename>../../interface/hud/unCloakUp.PNG</filename>
            <filename>../../interface/hud/cloakHover.PNG</filename>
            <filename>../../interface/hud/cloakUp.PNG</filename>
            <filename>../../interface/hud/sniffUp.PNG</filename>
            <filename>../../interface/hud/sniffHover.PNG</filename>
            <filename>../../interface/hud/zombifyHover.PNG</filename>
            <filename>../../interface/hud/zombifyUp.PNG</filename>
            <filename>../../interface/hud/sabotageUp.PNG</filename>
            <filename>../../interface/hud/sabotageHover.PNG</filename>
            <filename>../../interface/hud/healUp.PNG</filename>
            <filename>../../interface/hud/healHover.PNG</filename>
            <filename>../../interface/hud/releaseUp.PNG</filename>
            <filename>../../interface/hud/releaseHover.PNG</filename>
            <filename>../../interface/hud/arrowDown.PNG</filename>
            <filename>../../interface/hud/arrowLeft.png</filename>
            <filename>../../interface/hud/arrowRight.png</filename>
            <filename>../../interface/hud/arrowUp.png</filename>
            <filename>../../interface/hud/outOfRange.PNG</filename>
            <filename>../../interface/hud/moraleBg.PNG</filename>
            <filename>../../interface/hud/shieldBg.PNG</filename>
            <filename>../../interface/hud/hullBg.PNG</filename>
            <filename>../../interface/hud/eAvatarFrame.PNG</filename>
            <filename>../../interface/hud/fAvatarFrame.PNG</filename>
            <filename>../../interface/hud/avatarEngineer.PNG</filename>
            <filename>../../interface/hud/alertBg.PNG</filename>
            <filename>../../interface/hud/noRepair.PNG</filename>
            <filename>../../interface/hud/noBuild.PNG</filename>
            <filename>../../interface/hud/noMove.PNG</filename>
            <filename>../../interface/hud/noRecycle.PNG</filename>
            <filename>../../interface/hud/noEnemies.PNG</filename>
            <filename>../../interface/hud/noSabotage.PNG</filename>
            <filename>../../interface/hud/noZombify.PNG</filename>
            <filename>../../interface/hud/noHeal.PNG</filename>
            <filename>../../interface/hud/yourTurnWarning.PNG</filename>
            <filename>../../interface/hud/enemyTurnWarning.PNG</filename>
            <filename>../../interface/hud/notYourTurn.PNG</filename>
            <filename>../../interface/hud/avatarBase.PNG</filename>
            <filename>../../interface/hud/avatarCannon.PNG</filename>
            <filename>../../interface/hud/avatarDeployment Station.PNG</filename>
            <filename>../../interface/hud/avatarReactor.PNG</filename>
            <filename>../../interface/hud/avatarStealth Dome.PNG</filename>
            <filename>../../interface/hud/avatarTurret.PNG</filename>
            <filename>../../interface/hud/selActionAlert.PNG</filename>
            <filename>../../interface/hud/statIconsTurns.PNG</filename>
            <filename>../../interface/hud/statIconsFire.png</filename>
            <filename>../../interface/hud/statIconsMoves1.png</filename>
            <filename>../../interface/hud/statIconsMoves2.png</filename>
            <filename>../../interface/hud/statIconsMoves3.png</filename>
            <filename>../../interface/hud/statIconsMoves4.png</filename>
            <filename>../../interface/hud/statIconsMoves5.png</filename>
            <filename>../../interface/hud/statIconsMoves6.png</filename>
            <filename>../../interface/hud/statIconsMoves7.png</filename>
            <filename>../../interface/hud/statIconsMoves8.png</filename>
            <filename>../../interface/hud/statIconsMoves9.png</filename>
            <filename>../../interface/hud/statIconsShield.png</filename>
            <filename>../../interface/hud/statIconsMoves0.PNG</filename>
            <filename>../../interface/hud/noMovesIcon.PNG</filename>
            <filename>../../interface/hud/noMovesRemaining.PNG</filename>
            <filename>../../interface/hud/spawnEngiHover.PNG</filename>
            <filename>../../interface/hud/spawnEngiUp.PNG</filename>
            <filename>../../interface/hud/deploymentZoneOccupied.png</filename>
            <filename>../../interface/hud/moraleDmgFloater.png</filename>
            <filename>../../interface/hud/repairFloater.png</filename>
            <filename>../../interface/hud/shieldDmgFloater.png</filename>
            <filename>../../interface/hud/healFloater.png</filename>
            <filename>../../interface/hud/hpDmgFloater.png</filename>
            <filename>../../interface/hud/strikerUp.PNG</filename>
            <filename>../../interface/hud/carrierHover.PNG</filename>
            <filename>../../interface/hud/carrierUp.PNG</filename>
            <filename>../../interface/hud/corvetteHover.PNG</filename>
            <filename>../../interface/hud/corvetteUp.PNG</filename>
            <filename>../../interface/hud/destroyerHover.PNG</filename>
            <filename>../../interface/hud/destroyerUp.PNG</filename>
            <filename>../../interface/hud/droneHover.PNG</filename>
            <filename>../../interface/hud/droneUp.PNG</filename>
            <filename>../../interface/hud/engineerHover.PNG</filename>
            <filename>../../interface/hud/engineerUp.PNG</filename>
            <filename>../../interface/hud/frigateHover.PNG</filename>
            <filename>../../interface/hud/frigateUp.PNG</filename>
            <filename>../../interface/hud/infiltratorHover.PNG</filename>
            <filename>../../interface/hud/infiltratorUp.PNG</filename>
            <filename>../../interface/hud/kelvinHover.PNG</filename>
            <filename>../../interface/hud/kelvinUp.PNG</filename>
            <filename>../../interface/hud/scoutHover.PNG</filename>
            <filename>../../interface/hud/scoutUp.PNG</filename>
            <filename>../../interface/hud/strikerHover.PNG</filename>
            <filename>../../interface/hud/avatarFrigate.PNG</filename>
            <filename>../../interface/hud/avatarDestroyer.PNG</filename>
            <filename>../../interface/hud/avatarN-DAR.PNG</filename>
            <filename>../../interface/hud/avatarOre Mine.PNG</filename>
            <filename>../../interface/hud/avatarEnergy Field.png</filename>
            <filename>../../interface/hud/secondaryRepair.PNG</filename>
            <filename>../../interface/hud/secondaryBuild.PNG</filename>
            <filename>../../interface/hud/secondaryAttack.PNG</filename>
            <filename>../../interface/hud/secondaryMove.PNG</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
